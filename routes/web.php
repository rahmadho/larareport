<?php

use Illuminate\Support\Facades\Hash;

Route::get('/', function () {
    return view('vue');
});

Route::get('qr', function () {
    return QrCode::size(500)->generate('Welcome to kerneldev.com!');
});

// SIGN IN OUT
Route::get('login', 'Monep\SignCtrl@login')->name('login');
Route::post('login', 'Monep\SignCtrl@signin')->name('signin');
Route::get('logout', 'Monep\SignCtrl@signout')->name('signout');
Route::get('info', 'Monep\SignCtrl@info')->name('info');
Route::get('reset', 'Monep\SignCtrl@reset')->name('reset');
Route::group(['prefix' => 'monep',  'middleware' => 'sign'], function () {
    Route::post('filter-paket-pokja', 'Monep\InfoCtrl@dorpdown_paket_pokja')->name('dorpdown.paket.pokja');
    // MONEP
    Route::get('dashboard', 'Monep\MonepCtrl@dashboard')->name('monep.dashboard');
    Route::get('surat', 'Monep\MonepCtrl@surat')->name('monep.surat.index');
    Route::get('paket-spse', 'Monep\MonepCtrl@paket_spse')->name('monep.paket.spse');
    Route::get('paket-rup', 'Monep\MonepCtrl@paket_rup')->name('monep.paket.rup');
    Route::get('paket', 'Monep\MonepCtrl@paket')->name('monep.paket.index');
    Route::get('panitia', 'Monep\MonepCtrl@panitia')->name('monep.panitia.index');
    Route::get('report', 'Monep\MonepCtrl@report')->name('monep.report.index');
    Route::get('progres', 'Monep\MonepCtrl@report_progres_paket')->name('monep.report.progres');
    Route::get('history', 'Monep\MonepCtrl@history')->name('monep.history');

    // USER
    Route::get('users', 'Monep\MonepCtrl@users')->name('monep.users.index');
    Route::get('user/create', 'Monep\MonepCtrl@user_create')->name('monep.user.create');
    Route::get('user/{id}/edit', 'Monep\MonepCtrl@user_edit')->name('monep.user.edit');
    Route::delete('user/destroy', 'Monep\MonepCtrl@user_delete')->name('monep.user.destroy');

    // INFO
    Route::get('paket/{id}/history', 'Monep\InfoCtrl@paket_history')->name('monep.paket.history');
    Route::get('lelang/{id}/info', 'Monep\InfoCtrl@lelang_info')->name('monep.lelang.info');
    Route::get('panitia/{id}/info', 'Monep\InfoCtrl@panitia_info')->name('panitia.info');

    // SUBMIT
    Route::post('paket/masuk', 'Monep\SubmitCtrl@paket_masuk')->name('monep.paket.masuk');
    Route::match(['post', 'put'], 'dokumen/store', 'Monep\SubmitCtrl@dokumen_store')->name('monep.dokumen.store');
    Route::post('spt/store', 'Monep\SubmitCtrl@spt_store')->name('monep.spt.store');
    Route::post('persiapan/store', 'Monep\SubmitCtrl@persiapan_store')->name('monep.persiapan.store');

    // Route::match(['post', 'put'], '/monep/{url}', 'Monep\SubmitCtrl@run')->name('monep.submit');

    // PRINT
    Route::get('print/info/{id}', 'Monep\PrintCtrl@info')->name('monep.print.info');
    Route::get('print/spt/{id}', 'Monep\PrintCtrl@spt')->name('monep.print.spt');
    Route::get('print/dokumen/{id}', 'Monep\PrintCtrl@dokumen')->name('monep.print.dokumen');
    Route::get('print/persiapan/{id}', 'Monep\PrintCtrl@persiapan')->name('monep.print.persiapan');
    Route::get('print/hasil/{id}', 'Monep\PrintCtrl@hasil')->name('monep.print.hasil');

    // SURAT 
    Route::get('surat/tambah', 'Monep\PaketCtrl@surat_add')->name('monep.surat.add');
    Route::post('surat/tambah', 'Monep\PaketCtrl@surat_add')->name('monep.surat.filter');
    Route::post('surat/store', 'Monep\SubmitCtrl@surat_store')->name('monep.surat.store');
    Route::get('surat/{id}/edit', 'Monep\PaketCtrl@surat_info')->name('monep.surat.edit');

    // REPORT
    Route::get('report/paket', 'Monep\PaketCtrl@report_paket')->name('report.paket');
    Route::get('report/paket/progres', 'Monep\MonepCtrl@report_progres_paket')->name('report.paket.gagal');

    Route::get('profile', 'Monep\MonepCtrl@profile')->name('profile');
    Route::put('profile', 'Monep\MonepCtrl@profile')->name('user.update');

    // MENU
    Route::get('menu', 'Monep\MenuCtrl@index')->name('monep.menu.index');
    Route::get('menu/create', 'Monep\MenuCtrl@create')->name('monep.menu.add');
    Route::get('menu/{id}/edit', 'Monep\MenuCtrl@edit')->name('monep.menu.edit');
    Route::post('menu/submit', 'Monep\MenuCtrl@store')->name('monep.menu.store');
    Route::put('menu/{id}/update', 'Monep\MenuCtrl@update')->name('monep.menu.update');
    Route::delete('menu/{id}/delete', 'Monep\MenuCtrl@destroy')->name('monep.menu.delete');

    // PEGAWAI
    Route::get('pegawai/{id}/edit', 'Monep\PaketCtrl@edit_pegawai')->name('monep.pegawai.edit');
    Route::put('pegawai/{id}/update', 'Monep\SubmitCtrl@pegawai_update')->name('monep.pegawai.update');
});
