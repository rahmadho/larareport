<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

// syncronize
Route::get('sync/{table}/{fisrtTime?}', 'Epns\SyncController@sync')->name('api.db.sync');
Route::get('sync-all', 'Epns\SyncController@sync_all')->name('api.db.sync.all');

Route::prefix('reporting')->group(function () {
	Route::get('stats/paket/{tahun?}', 'Api\ReportingCtrl@stats')->name('api.report.paket');
	Route::get('stats/persatker/{tahun?}', 'Api\ReportingCtrl@stats_per_satker')->name('api.report.persatker');
	Route::match(['get', 'post'], 'table/lelang/{tahun?}', 'Api\ReportingCtrl@table_lelang')->name('api.report.table.lelang');
	Route::get('stats/rekanan/{tahun?}', 'Api\ReportingCtrl@rekanan')->name('api.report.rekanan');
});

Route::prefix('monep')->group(function () {
	Route::get('table/surat', 'Api\MonepCtrl@table_surat')->name('api.monep.table.surat');
	Route::get('table/paket-rup', 'Api\MonepCtrl@table_paket_rup')->name('api.monep.table.paket_rup');
	Route::get('table/paket', 'Api\MonepCtrl@table_paket')->name('api.monep.table.paket');
	Route::get('table/pilih-paket', 'Api\MonepCtrl@table_pilih_paket')->name('api.monep.table.pilih_paket');
	Route::get('table/report-query-builder', 'Api\MonepCtrl@table_report_query_builder')->name('api.monep.table.report');
});

Route::get('ajax/get/{url}', 'Api\MonepCtrl@get')->name('ajax.get');
Route::post('ajax/post/{url}', 'Api\MonepCtrl@post')->name('ajax.post');
Route::put('ajax/post/{url}', 'Api\MonepCtrl@put')->name('ajax.put');
Route::delete('ajax/delete/{url}', 'Api\MonepCtrl@delete')->name('ajax.delete');
