const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

const public_js = 'public/js/';
const public_css = 'public/css/';
const resource_sass = 'resources/assets/sass/';
const fa_home = 'node_modules/@fortawesome/fontawesome-free';

mix.
   copy(fa_home + '/css/all.min.css', public_css + 'fontawesome.css');

// mix.js('resources/js/surat.js', 'public/js')