let state = {
	api_url: 'http://localhost/laravel/larareport/public/api/reporting',
	monep_url: 'http://localhost/laravel/larareport/public/',
	loading: false,
	tahun: new Date().getFullYear(),
	monep_stats: {},
	monep_stats_pertahun: {}
}

export default state;