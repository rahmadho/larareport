let mutations = {
	SET_LOADING(state, payload) {
		state.loading = payload;
	},
	SET_TAHUN(state, payload) {
		state.tahun = payload;
	},
	SET_STATS(state, payload) {
		state.monep_stats = payload;
	},
	SET_STATS_PERTAHUN(state, payload) {
		state.monep_stats_pertahun = payload;
	},
}

export default mutations;