var api_url = 'http://localhost/laravel/larareport/public/api/reporting';

let actions = {
	setLoading({ commit }, payload) {
		commit('SET_LOADING', payload);
	},
	setTahun({ commit }, tahun = "") {
		commit('SET_TAHUN', tahun);
	},
	setStats({ commit }, tahun = "") {
		axios.get(`${api_url}/stats/paket/${tahun}`)
			.then(res => {
				commit('SET_STATS', res.data);
			}).catch(err => console.log(err));
	},
	setStatsPertahun({ commit }, tahun = "") {
		axios.get(`${api_url}/stats/paket/${tahun}`)
			.then(res => {
				commit('SET_STATS_PERTAHUN', res.data);
			}).catch(err => console.log(err));
	},
}

export default actions;