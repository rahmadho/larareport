let getters = {
	loading: state => {
		return state.loading;
	},
	tahun: state => {
		return state.tahun;
	},
	monep_stats: state => {
		return state.monep_stats;
	},
	monep_stats_pertahun: state => {
		return state.monep_stats_pertahun;
	}
}

export default getters;