const routes = [
	{
		path: '/',
		component: require('./pages/home.vue').default
	},
	{
		path: '/detail',
		component: require('./pages/ringkasan.vue').default
	},
	{
		path: '/satker',
		component: require('./pages/satker.vue').default
	},
	{
		path: '/paket',
		component: require('./pages/paket.vue').default
	},
	{
		path: '/ulang',
		component: require('./pages/ulang.vue').default
	},
	{
		path: '/gagal',
		component: require('./pages/gagal.vue').default
	},
	{
		path: '/rekanan',
		component: require('./pages/rekanan.vue').default
	}
]
export default routes;