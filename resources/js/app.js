require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import routes from './routes';
import store from './store/index';
Vue.use(VueRouter);
const router = new VueRouter({
	routes: routes
});
import { VueTables, ClientTable, ServerTable } from 'vue-tables-2';
// Vue.use(ClientTable, [options = {}], [useVuex = true], [theme = 'bootstrap4'], [template = 'default']);
Vue.use(ClientTable);
Vue.use(ServerTable);
Vue.component('spinner', require('./components/Spinner.vue').default);
Vue.component('modal-box', require('./components/Modal.vue').default);

Vue.component('aside-sidebar', require('./components/AsideSidebar.vue').default);
Vue.component('header-navbar', require('./components/HeaderNavbar.vue').default);

Vue.component('knob', require('./components/Knob.vue').default);
Vue.component('box', require('./components/Box.vue').default);
Vue.component('box-header', require('./components/BoxHeader.vue').default);
Vue.component('box-body', require('./components/BoxBody.vue').default);
Vue.component('box-footer', require('./components/BoxFooter.vue').default);
Vue.component('info-box', require('./components/InfoBox.vue').default);
Vue.component('small-box', require('./components/SmallBox.vue').default);
Vue.component('progress-group', require('./components/ProgressGroup.vue').default);
Vue.component('alert-box', require('./components/AlertBox.vue').default);
Vue.component('description-block', require('./components/DescriptionBlock.vue').default);

Vue.component('control-skin', require('./components/ControlSkin.vue').default);

// chart
Vue.component('highchart-bar', require('./components/HighChartBar.vue').default);
// filter
Vue.component('filter-box', require('./components/FilterBox.vue').default);
Vue.component('filter-tahun', require('./components/FilterTahun.vue').default);

// table
Vue.component('vue-table-2', require('./components/VueTable.vue').default);
Vue.component('vue-table-rekanans', require('./components/VueTableRekanan.vue').default);
Vue.component('table-paket-satker-pilih', require('./components/TablePaketSatkerPilih.vue').default);

Vue.mixin({
	methods: {
		formatPrice(value) {
			let val = (value / 1).toFixed(2).replace(".", ",");
			return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		}
	}
})

window.app = new Vue({
	el: '#app',
	store,
	router
});