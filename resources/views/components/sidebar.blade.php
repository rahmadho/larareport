<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{asset('./sumbar.png')}}" alt="SUMBAR">
      </div>
      <div class="pull-left info">
        <p>UKPBJ Sumatera Barat</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    {{-- @if (Auth::user()->id_level == 4)
    <form action="{{route('dorpdown.paket.pokja')}}" method="post" class="sidebar-form" id="as_pokja">
      @method('POST')
      @csrf
      <div class="costom-group">
        @php 
          $pegawai_panitia_login = App\Models\Monep\Pegawai::with('panitia')
          ->whereHas('panitia', function($q){
            return $q->where('is_active', '-1')->where('pnt_tahun', date('Y'));
          })->where('peg_nip', Auth::user()->username)->first();
          $pilihan_pokja = $pegawai_panitia_login->panitia->where('pnt_tahun', 2019)->where('is_active', -1);
        @endphp
        <select name="as_pokja" class="form-control" placeholder="Login Sebagai?" onchange="document.getElementById('as_pokja').submit()">
          @foreach ($pilihan_pokja as $item)
            <option @if(Session::has('as_pokja') && Session::get('as_pokja') == $item->id) selected @endif value="{{$item->id}}">{{$item->pnt_nama}}</option>
          @endforeach
        </select>
        </span>
      </div>
    </form>
    @endif --}}

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      @if ($auth)
        @php
        $menus = \App\Models\Monep\RouteController::where('is_menu', true)
        ->where('active', true)
        ->whereRaw($auth->id_level.' = any(role)')->orderBy('urut', 'asc')->get();
        @endphp
        @foreach ($menus as $menu)
        <li><a href="{{route($menu->route)}}"><i class="{{$menu->icon}}"></i> {{$menu->label}}</a></li>
        @endforeach
        <li><a href="{{route('profile')}}"><i class="fa fa-unlock-alt"></i> Ubah Password</a></li>
        <li><a href="{{route('signout')}}"><i class="fa fa-power-off"></i> Sign Out</a></li>
      @else
        <li><a href="{{route('profile')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('profile')}}"><i class="fa fa-bar-chart-o"></i> Ringkasan Paket</a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Data Lelang</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-cube"></i> Paket Lelang</a></li>
            <li><a href=""><i class="fa fa-building-o"></i> Satuan Kerja</a></li>
            <li><a href=""><i class="fa fa-repeat"></i> Tender Ulang</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Tender Gagal</a></li>
          </ul>
        </li>
        <li><a href="{{route('profile')}}"><i class="fa fa-building"></i> Rekanan</a></li>
      @endif
    </ul>
  </section>
</aside>