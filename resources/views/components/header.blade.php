<header class="main-header">
  <a href="index2.html" class="logo hidden-xs"></a>
  <nav class="navbar navbar-static-top">
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li><a href="#" id="syncronize" style="background:rgba(0, 0, 0, 0.1);"><i class="fa fa-refresh"></i></a></li>
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{asset('./sumbar.png')}}" class="user-image" alt="LOGO">
            <span class="hidden-xs">
              @if ($auth)
                @auth
                {{ Auth::user()->nama }}
                @endauth
              @else
                Sign In
              @endif
            </span>
          </a>
          <ul class="dropdown-menu">
            <li class="user-header">
              <img src="{{asset('./sumbar.png')}}" alt="LOGO">
              <p>
                UKPBJ Sumatera Barat
                <small>From 2019</small>
              </p>
            </li>
            <li class="user-footer">
              <div class="">
                @if ($auth)
                <a href="{{route('signout')}}" class="btn col-md-12 btn-default btn-flat">Sign out</a>
                @else
                <a href="{{route('login')}}" class="btn col-md-12 btn-default btn-flat">Sign In</a>
                @endif
              </div>
            </li>
          </ul>
        </li>
        <li><a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a></li>
      </ul>
    </div>
  </nav>
</header>