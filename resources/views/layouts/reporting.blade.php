
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>LPSE | Reporting</title>
	<link rel="stylesheet" href="./css/app.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	{{-- <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet"> --}}
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="{{asset('./css/loading.css')}}">
	<style>
		.main-header .logo {
			background: url(./lpse.png);
			background-size: 52%;
			background-position: center;
			background-repeat: no-repeat;
			background-color: #fff!important;
		}
		.main-header .logo:hover {
			background-color: #fff;
		}
		.navbar-nav>.user-menu .user-image{
			border-radius: 0;
		}
	</style>
	@section('css')
	@show
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
	<div class="wrapper" id="app">

		<header class="main-header">
			<a href="index2.html" class="logo">
				{{-- <span class="logo-mini"><b>e</b>REPORTING</span>
				<span class="logo-lg"><b>e</b>REPORTING</span> --}}
			</a>
			<header-navbar logo-src="./sumbar.png"></header-navbar>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<aside-sidebar logo-src="./sumbar.png"></aside-sidebar>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">			
			@yield('content')
		</div>
		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0.0
			</div>
			<strong>Copyright &copy; {{date('Y')}} <a href="http://www.sumbarprov.go.id">UKPBJ Sumatera Barat</a>.</strong> All rights
			reserved.
		</footer>

		<control-skin></control-skin>

		<spinner></spinner>

	</div>

	<script src="{{asset('./js/app.js')}}"></script>
	<script src="{{asset('./js/demo.js')}}"></script>
	@section('js')

	@show
</body>
</html>