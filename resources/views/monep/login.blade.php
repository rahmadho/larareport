
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>| SIGN IN UKPBJ - ULP - POKJA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
  <link href="//stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.10/css/AdminLTE.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.10/css/skins/_all-skins.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.10/css/skins/skin-blue-light.css" rel="stylesheet">
  <link href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{route('login')}}"><b>UKPBJ</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="{{route('signin')}}" method="post">
      @method('POST')
      @csrf
      <div class="form-group has-feedback @if($errors->first('username')) has-error @endif">
        <input name="username" type="text" class="form-control" placeholder="Username" value="{{old('username')}}">
        <span class="fa fa-user form-control-feedback"></span>
        @if($errors->first('username'))
        <span class="help-block">{{$errors->first('username')}}</span>
        @endif
      </div>
      <div class="form-group has-feedback @if($errors->first('password')) has-error @endif">
        <input name="password" type="password" class="form-control" placeholder="Password">
        <span class="fa fa-lock form-control-feedback"></span>
        @if($errors->first('password'))
        <span class="help-block">{{$errors->first('password')}}</span>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-8"></div>
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script src="//code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</body>
</html>
