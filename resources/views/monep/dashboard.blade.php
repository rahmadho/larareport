@extends('layout')
@section('css')
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1>Dashboard</h1>
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header clearfix">
            <h5>MONITORING</h5>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="info-box bg-red">
                  <span class="info-box-icon"><i class="fa fa-grav"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">TEXT</span>
                    <span class="info-box-number">200</span>
                    <div v-if="dataProgress">
                      <div class="progress" v-if="dataPercent">
                        <div class="progress-bar"></div>
                      </div>
                      <span class="progress-description" v-if="dataDesc">
                        Deskripsi Info
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
@endsection