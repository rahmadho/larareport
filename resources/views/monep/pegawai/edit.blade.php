@extends('layout')
@section('css')
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1 class="uppercase">Edit Pegawai</h1>
  </div>
  <div class="content">
    <form class="paketmasuk" method="post" action="{{ route('monep.pegawai.update', $pegawai->peg_id) }}">
      @csrf
      @method('PUT')
      <div class="box box-primary paket-template">
        <div class="box-header">
          {{$pegawai->peg_nama}}
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('nama')) has-error @endif">
                <label>Nama</label>
                <input name="nama" type="text" class="form-control form-control-sm" value="{{$pegawai->peg_nama}}" />
                @if($errors->first('nama'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('golongan')) has-error @endif">
                <label>Golongan</label>
                <input name="golongan" type="text" class="form-control form-control-sm" value="{{$pegawai->peg_golongan}}" />
                @if($errors->first('golongan'))
                <span class="help-block">{{$errors->first('golongan')}}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('pangkat')) has-error @endif">
                <label>Pangkat</label>
                <input name="pangkat" type="text" class="form-control form-control-sm" value="{{$pegawai->peg_pangkat}}" />
                @if($errors->first('pangkat'))
                <span class="help-block">{{$errors->first('pangkat')}}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('jabatan')) has-error @endif">
                <label>Jabatan</label>
                <input name="jabatan" type="text" class="form-control form-control-sm" value="{{$pegawai->peg_jabatan}}" />
                @if($errors->first('jabatan'))
                <span class="help-block">{{$errors->first('jabatan')}}</span>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
      <a class="btn btn-warning no-radius" href="{{route('monep.panitia.index')}}">
        <i class="fa fa-arrow-left space-right"></i> KEMBALI 
      </a>
      <button type="submit" class="btn btn-primary no-radius">
        SIMPAN <i class="fa fa-arrow-right space-left"></i>
      </button>
    </form>
  </div>
</div>
@endsection
@section('js')
@endsection