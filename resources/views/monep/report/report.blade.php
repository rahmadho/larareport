@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.css')}}">
<style>
  .content-header h1 { margin: 0; }
  #datatable-paket tbody tr:hover { cursor: pointer; }
  #datatable-paket_filter { visibility: hidden; display: none; }
  .space-right { margin-right: 8px; }
</style>
@endsection
@section('content')
<div class="page-template">
  <div class="content">
    <section class="">
      <div class="row">
        <div class="col-md-3">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-check-circle"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">TENDER TAYANG</span>
              <span class="info-box-number">{{$result['count_all']}}</span>
              <div v-if="dataProgress">
                <div class="progress">
                  <div class="progress-bar"></div>
                </div>
                <span class="progress-description">
                  Paket tayang
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-check-circle"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">PROSES TENDER</span>
              <span class="info-box-number">{{$result['count_jalan']}}</span>
              <div v-if="dataProgress">
                <div class="progress">
                  <div class="progress-bar"></div>
                </div>
                <span class="progress-description">
                  Paket proses tender
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-check-circle"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">TENDER SELESAI</span>
              <span class="info-box-number">{{$result['count_selesai']}}</span>
              <div v-if="dataProgress">
                <div class="progress">
                  <div class="progress-bar"></div>
                </div>
                <span class="progress-description">
                  Paket selesai
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-check-circle"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">TENDER GAGAL</span>
              <span class="info-box-number">{{$result['count_gagal']}}</span>
              <div v-if="dataProgress">
                <div class="progress">
                  <div class="progress-bar"></div>
                </div>
                <span class="progress-description">
                  Paket gagal
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="box">
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <div class="filterable pull-left">
              @if (Auth::user()->id_level != 4)
              <div action="" method="post" class="form-">
                @csrf
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-4">
                      <input type="text" class="form-control" placeholder="Pencarian..." id="filterable">
                    </div>
                    <div class="col-md-4">
                      <select name="satker" id="filter-satker" class="form-control">
                        <option value="0">SATUAN KERJA</option>
                        @foreach ($result['satker'] as $item)
                          <option value="{{$item->stk_id}}">{{$item->stk_nama}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select name="as_pokja" class="form-control uppercase" id="pilihPanitia">
                        <option value="">Pilih Pokja</option>
                        @foreach ($paket_pokja as $item)
                          <option value="{{$item->pnt_id}}">{{$item->pnt_nama}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-2">
                      <div class="input-group">
                        <select name="tahun" id="filter-tahun" class="form-control">
                          @php 
                            $yNow = date('Y');
                            $yMin = $yNow-5;  // 2008; 
                          @endphp
                          @for ($i = $yNow; $i >= $yMin; $i--)
                            <option value="{{$i}}">{{$i}}</option>
                          @endfor
                        </select>
                        <div class="input-group-btn">
                          <button class="btn btn-primary" type="submit" id="btn-filter">
                            <i class="glyphicon glyphicon-search"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <span>MASUK ULP</span>
                  <div class="form-group" style="display:inline-block">
                    <label class="switch" style="margin-top:2px">
                      <input name="in_ulp" type="checkbox">
                      <span class="slider round"></span>
                    </label>
                  </div>
                </div>
              </div>
              @else
                <div action="" method="post" class="form-inline">
                  @csrf
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Pencarian..." id="filterable">
                  </div>
                  <div class="form-group">
                    <select name="satker" id="filter-satker" class="form-control" style="max-width: 400px">
                      <option value="0">SATUAN KERJA</option>
                      @foreach ($result['satker'] as $item)
                        <option value="{{$item->stk_id}}">{{$item->stk_nama}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <select name="as_pokja" class="form-control uppercase" id="pilihPanitia">
                      <option value="">Pilih Pokja</option>
                      @foreach ($paket_pokja as $item)
                        <option value="{{$item->pnt_id}}">{{$item->pnt_nama}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <select name="tahun" id="filter-tahun" class="form-control">
                        @php 
                          $yNow = date('Y');
                          $yMin = $yNow-5;  // 2008; 
                        @endphp
                        @for ($i = $yNow; $i >= $yMin; $i--)
                          <option value="{{$i}}">{{$i}}</option>
                        @endfor
                      </select>
                      <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit" id="btn-filter">
                          <i class="glyphicon glyphicon-search"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box box-primary">
      <div class="box-body">
        <section class="">
          <div class="row">
            <div class="col-md-12">
                <table class="table table-striped" id="datatable-paket" style="width:100%; font-size: 12px">
                  <thead>
                    <th style="min-width: 305px">Paket</th>
                    <th>KPA / PPK</th>
                    <th style="min-width: 95px;">Pagu / HPS (Rp)</th>
                    <th style="min-width: 80px;">Tanggal</th>
                    <th>Dokumen</th>
                    <th>SPT</th>
                    <th>Progres</th>
                    <th>Pemenang</th>
                  </thead>
                </table>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('/lib/datatables/datatables.min.js')}}"></script>
<script src="{{asset('/lib/datatables/1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function () {
    var url = "{{ url('/api/ajax/get/query_table_paket_monep') }}";
    var table = $('#datatable-paket').DataTable({
      "searching": false,
      "ordering": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        "url": url,
        "dataType": "json",
        "type": "get",
        "data": function(data){
          let filterTahun = $("#filter-tahun").val();
          let filterable = $("#filterable").val();
          let pilihPanitia = $("#pilihPanitia").val();
          data.panitia = pilihPanitia;
          let filterSatker = $("#filter-satker").val();
          let in_ulp = $('input[name="in_ulp"]').prop('checked');
          data.filter_satker = filterSatker;
          data.in_ulp = in_ulp;
          data._token = "{{csrf_token()}}";
          data.tahun = filterTahun;
          data.filter = filterable
        }
      },
      "columns": [
        { 
          "data": "pkt_nama",
          "render": function(data, type, row) {
            let ulang = "";
            let tayang = "";
            let on_sibaja = "";
            let evaluasi_ulang = "";
            if (row.ulang) {
              ulang = `<span class="label label-warning space-right">${row.ulang}</span>`
            }
            if (row.lelang.eva_versi > 1) {
              evaluasi_ulang = `<span class="label label-danger space-right">Evaluasi Ulang</span>`
            }
            if(row.lls_status == 1){
              tayang = `<i class="fa fa-star space-right" style="color: orange !important"></i>`
            }
            if(row.on_sibaja === true){
              on_sibaja = `<i class="fa fa-check space-right" style="color: green !important"></i>`
            }
            return `
            <div>
              ${on_sibaja}
              ${tayang}
              <span class="label label-info space-right">${row.lls_id}</span>
              <span class="label label-success space-right">${row.spse_versi}</span>
              ${ulang}
              ${evaluasi_ulang}
            </div>
            <div><a>${data}</a></div>
            <div><small><i class="fa fa-building-o space-right"></i> ${row.stk_nama}</small></div>
            <div><small><i class="fa fa-flag-o space-right"></i><b>${row.kategori_lelang.nama}/${row.metode.metode_kualifikasi} - ${row.sbd_id} - ${row.ang_tahun}</b></small></div>
            `
          }
        },
        { 
          "data": "ppk",
          "render": function(data, type, row) {
            if(row.ppk_nip != null){
              return `<div><span class="label label-success">${row.ppk_nip}</span></div><div class="status_lelang"><small>${row.ppk_nama}</small></div>`
            } else {
              return `<div class="status_lelang"><small>Tidak ada KPA / PPK </small></div>`
            }
          }
        },
        { 
          "data": "pkt_pagu",
          "render": function(data, type, row) {
            return `<div style='font-size: 11px;'>
            <div style='font-size: 8px;border-bottom: 1px solid #3c8dbc;font-weight: 700'>PAGU</div>
            <div class='pkt_pagu' style='margin-bottom: 8px;font-weight: 700'>${row.rp_pagu}</div>
            <div style='font-size: 8px;border-bottom: 1px solid #00a65a;font-weight: 700'>HPS</div>
            <div class='pkt_hps'>${row.rp_hps}</div>
            </div>
            `
          }
        },
        { 
          "data": "tanggal",
          "render": function(data, type, row) {
            let last_persiapan = (row.persiapan.length > 0) ? tgl_indo(row.persiapan[row.persiapan.length-1].tgl_rapat) : null;
            return `<div style='font-size: 11px;'>
            <div style='font-size: 8px;border-bottom: 1px solid #3c8dbc;font-weight: 700'>Rapat Persiapan</div>
            <div class='pkt_hps' style='margin-bottom: 8px;'><b>${row.persiapan.length} kali</b></div>
            <div style='font-size: 8px;border-bottom: 1px solid #00a65a;font-weight: 700'>Tanggal Tayang</div>
            <div class='pkt_hps'>${tgl_indo(row.lls_tgl_setuju)}</div></div>`
          }
        },
        { 
          "data": "dokumen",
          "render": function(data, type, row) {
            if (row.surat) {
              return `<div style='font-size: 11px;'>
              <div style='font-size: 8px;border-bottom: 1px solid #3c8dbc;font-weight: 700;'>Tanggal Diterima</div>
              <div class='pkt_pagu' style='margin-bottom: 8px;font-weight: 700'><b>${tgl_indo(row.surat.tgl_input)}</b></div>
              <div style='font-size: 8px;border-bottom: 1px solid #00a65a;font-weight: 700'>Tanggal Surat</div>
              <div class='pkt_hps'>${tgl_indo(row.surat.tgl_surat)}</div>
              </div>`
            } else {
              return '-';
            }
          }
        },
        {
          "data": "spt",
          "render": function(data, type, row){
            let pokja = "";
            if(row.pnt_id) {
              pokja = `<span style='text-transform: capitalize'>${row.pnt_nama}</span>`
            }
            if (row.spt.length > 0) {
              return `<div style='font-size: 11px;'>
                <span class='label label-primary'>${pokja} - [ ${row.spt[row.spt.length - 1].no_spt} ]</span>
                <div>${tgl_indo(row.spt[row.spt.length - 1].tgl_input)}</div>
              </div>`;
            }
            return '-';
          }
        },
        {
          "data": "progres",
          "render": function(data, type, row){
            return `<div style='font-size: 11px;'>
              <div>${row.tahap_now}</div>
            </div>`;
          }
        },
        {
          "data": "pemenang",
          "render": function(data, type, row){
            let harga = 0;
            let rekanan = "-";
            if (row.pemenang){
              if (row.pemenang.nev_harga_negosiasi > 0) { harga = row.pemenang.nev_harga_negosiasi; }
              else if (row.pemenang.nev_harga_terkoreksi > 0) { harga = row.pemenang.nev_harga_terkoreksi; }
              else if (row.pemenang.nev_harga > 0) { harga = row.pemenang.nev_harga }
              else if (row.pemenang.psr_harga_terkoreksi > 0) { harga = row.pemenang.psr_harga_terkoreksi; }
              else { harga = row.pemenang.psr_harga; }
              harga = formatPrice(harga);
              rekanan = row.pemenang.rekanan.rkn_nama;
            } else {
              harga = '-';
            }
            return `<div style='font-size: 11px;'>
              <div style='border-bottom: 1px solid #3c8dbc;font-weight: 700;'>${rekanan}</div>
              <div style=''>${harga}</div>
            </div>`;
          }
        }
      ]
    });
    table.on( 'click', 'tr', function ( e, dt, type, indexes ) {
      var pilih = table.row(this).data();
      window.open('{{url('/')}}/monep/lelang/'+pilih.lelang.lls_id+'/info', '_blank');
    });
    $('#filterable').unbind();
    $('#filterable').bind('keyup', function(e) {
      if(e.keyCode == 13) {
        table.draw();
      }
    });

    $('#btn-filter').bind('click', function(e) {
      table.draw();
    });
    $('input[name="has_pokja"]').bind('change', function(e) {
      table.draw();
    });
  });

  function tgl_indo(tgl){
    let bulan = ["Januari", "Februari", "Maret","April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    let tanggal = new Date(tgl);
    if (tgl !== null) {
      return `${tanggal.getDate()} ${bulan[tanggal.getMonth()]} ${tanggal.getFullYear()}`;
    }
    return '-';
  }
  function formatPrice(value) {
    let val = (value / 1).toFixed(2).replace(".", ",");
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }
</script>
@endsection