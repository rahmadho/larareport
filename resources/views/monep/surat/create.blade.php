@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datepicker/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.css')}}">
<style>
  .modal-dialog {display: table;width: auto;max-width: 1000px;}
  #datatable-paket tbody tr:hover { cursor: pointer; }
  button.ceklis, button.ceklis:hover, button.ceklis:visited, button.ceklis:active, button.ceklis:focus{border: none; background: transparent;}
</style>
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1>Surat Masuk</h1>
  </div>
  <div class="content">
    <div class="box box-primary">
      <div class="box-header clearfix uppercase">
        <div class="pull-left">Kelengkapan Dokumen</div>
        <div class="filterable pull-right">
          <form action="{{route('monep.surat.filter')}}" method="post" class="form-inline">
            @method('post')
            @csrf
            <div class="form-group @if($errors->first('kgr_id')) has-error @endif">
              <select name="kgr_id" class="form-control">
                <option @if($request->input('kgr_id') == '999') selected @endif value="999">Pilih Jenis Pengadaan</option>
                <option @if($request->input('kgr_id') == '0') selected @endif value="0">Pengadaan Barang</option>
                <option @if($request->input('kgr_id') == '1') selected @endif value="1">Jasa Konsultansi</option>
                <option @if($request->input('kgr_id') == '2') selected @endif value="2">Jasa Konstruksi</option>
                <option @if($request->input('kgr_id') == '3') selected @endif value="3">Jasa Lainnya</option>
              </select>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Filter</button>
            </div>
          </form>
        </div>
      </div>
      <div class="box-body">
        <form action="{{route('monep.paket.masuk')}}" method="POST" id="submited-form">
          @csrf
          @method('post')
          <div class="row">
            <div class="col-md-12">
              @if ($kategori_lelang)
                <div class="kategori">
                  <h6>{{$kategori_lelang['nama']}}</h6>
                </div>
              @endif
              @if($dokumen->isNotEmpty())
              <div class="table-responsive">
                <table class="table dataTable display responsive no-wrap" width="100%" id="dokumen">
                  <thead class="silver">
                    <th>No</th>
                    <th>Dokumen</th>
                    <th style="text-align:center">Ketersediaan</th>
                  </thead>
                  <tbody>
                    @php $no = 1; @endphp
                    @foreach($dokumen as $doc)
                      <tr>
                        <td>{{$no++}}</td>
                        <td>{{$doc->nama}}</td>
                        <td align="center">
                          <label class="switch">
                            <input name="is_ada[{{$doc->id}}]" type="checkbox" @if ($doc->id == 7) class='pilih-paket' @endif>
                            <span class="slider round"></span>
                          </label>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              @endif
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <section class="">
                <div class="row">
                  <hr>
                  <div class="col-md-11">
                    <input type="hidden" name="pkt_id">
                    <input type="hidden" name="lls_id">
                    <div id="paket-info"></div>
                  </div>
                  <div class="col-md-1">
                    <div>
                      <button type="button" class="ceklis" style="font-size:63px;color:orange">
                      </button>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>

          @if($dokumen->isNotEmpty())
          <div class="row">
            <hr>
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Nomor Surat</label>
                    <input name="no_surat" value="{{old('no_surat')}}" type="text" class="form-control form-control-sm" />
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Tanggal Surat</label>
                    <input name="tgl_surat" value="{{old('tgl_surat')}}" data-date-format="yyyy-mm-dd" data-provide="datepicker-inline" type="text" class="form-control form-control-sm date-pick" autocomplete="off" />
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Tanggal Input</label>
                    <input name="tgl_input" value="{{old('tgl_input')}}" data-date-format="yyyy-mm-dd" data-provide="datepicker-inline" type="text" class="form-control form-control-sm date-pick" autocomplete="off" />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>Ringkasan</label>
                <textarea name="perihal" rows="4" class="form-control form-control-sm">{{old('perihal')}}</textarea>
              </div>
              <div class="form-group">
                <label>Surat usulan tender?</label>
                <div>
                  <label class="switch">
                    <input name="is_tender" checked type="checkbox" readonly>
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
            </div>
          </div>
          @endif

        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('template')
<div class="modal-template">
  <div class="modal fade" id="create-paket">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title uppercase">Pilih Paket Pengadaan</h4>
          </div>
          <div class="box-body">
            <div class="clearfix">
              <div class="filterable pull-left">
                <div class="form-inline" style="font-size: 12px">
                  @csrf
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Pencarian..." id="filterable">
                  </div>
                  <div class="form-group">
                    <select name="tahun" id="filter-tahun" class="form-control">
                      @php 
                        $yNow = date('Y');
                        $yMin = $yNow-5;  // 2008; 
                      @endphp
                      @for ($i = $yNow; $i >= $yMin; $i--)
                        <option value="{{$i}}">{{$i}}</option>
                      @endfor
                    </select>
                  </div>
                  <div class="form-group">
                    <select name="satker" id="filter-satker" class="form-control" style="font-size:11px;">
                      <option value="0">SATUAN KERJA</option>
                      @foreach ($satker as $item)
                        <option value="{{$item->stk_id}}">{{$item->stk_nama}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                  </div>
                </div>
              </div>
            </div>
            <table class="table with-border table-bordered table-striped" id="datatable-paket" style="width:100%">
              <thead>
                <th>Paket</th>
                <th style="width: 250px">Pagu / HPS</th>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script src="{{asset('/lib/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('/lib/datatables/datatables.min.js')}}"></script>
<script src="{{asset('/lib/datatables/1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.js')}}"></script>
@if ($dokumen)
<script>
  $(document).ready(function(){
    $('.pilih-paket').change(function(){
      if(this.checked){
        $("#create-paket").modal('show');
      } else {
        $("input[name='pkt_id']").val("");
        $("input[name='lls_id']").val("");
        $("#paket-info").html("");
        $(".ceklis").html("");
      }
    });
    $(".ceklis").click(function(){
      let cek = $('#dokumen input[type="checkbox"]');
      let no_ceklis = cek.not(':checked').length;
      // if (no_ceklis === 0){
      //   $("#submited-form").submit();
      // } else { 
      //   alert(no_ceklis+" dokumen tidak lengkap!");
      // }
      $("#submited-form").submit();
    });

    $('#create-paket').on('hidden.bs.modal', function () {
      let pkt = $("input[name='pkt_id']").val();
      if (!pkt) {
        $('.pilih-paket').prop("checked", false);
      }
    });
    var url = "{{ url('/api/ajax/get/query_table_paket_monep') }}";
    //var url = "{{ route('api.monep.table.pilih_paket') }}";
    var table = $('#datatable-paket').DataTable({
      "bLengthChange": false,
      "searching": false,
      "ordering": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        "url": url,
        "dataType": "json",
        "type": "get",
        "data": function(data){
          let filterTahun = $("#filter-tahun").val();
          let filterable = $("#filterable").val();
          let filterSatker = $("#filter-satker").val();
          let filterKgr = $("select[name='kgr_id']").val();
          data._token = "{{csrf_token()}}";
          data.tahun = filterTahun;
          data.filter = filterable;
          data.filter_satker = filterSatker;
          data.kgr_id = filterKgr;
          data.tayang = 0;
          data.filter_progres = 1;
        }
      },
      "columns": [
        { 
          "data": "pkt_nama",
          "render": function(data, type, row) {
            let ulang = "";
            let tayang = "";
            let on_sibaja = "";
            let evaluasi_ulang = "";
            let pokja = "";
            if (row.ulang) {
              ulang = `<span class="label label-warning">${row.ulang}</span>`
            }
            if (row.eva_versi > 1) {
              evaluasi_ulang = `<span class="label label-danger">Evaluasi Ulang</span>`
            }
            if(row.lls_status == 0){
              tayang = `<i class="fa fa-star-o space-right" style="color: orange !important"></i>`
            } else if(row.lls_status == 1){
              tayang = `<i class="fa fa-star space-right" style="color: orange !important"></i>`
            } else if(row.lls_status == 2){
              tayang = `<i class="fa fa-star space-right" style="color: red !important"></i>`
            }
            if(row.on_sibaja === true){
              on_sibaja = `<i class="fa fa-check space-right" style="color: green !important"></i>`
            }
            if(row.pnt_nama) {
              pokja = `<span class="label label-primary space-right" style='text-transform: capitalize'>${row.pnt_nama}</span>`
            }
            return `
            <div>
              ${on_sibaja}
              ${tayang}
              <span class="label label-primary">RUP : ${row.rup_id}</span>
              <span class="label label-info">KODE : ${row.lls_id}</span>
              <span class="label label-info">PKT : ${row.pkt_id}</span>
              <span class="label label-success">${row.spse_versi}</span>
              ${ulang}
              ${evaluasi_ulang}
              ${pokja}
            </div>
            <div class="uppercase">${data}</div>
            <div class="uppercase"><b><small><i class="fa fa-building-o space-right"></i> ${row.stk_nama}</small></b></div>
            <div><small><i class="fa fa-flag-o space-right"></i><b>${row.kategori_lelang.nama} - ${row.sbd_id} - ${row.ang_tahun}</b></small></div>
            `
          }
        },
        { 
          "data": "pkt_pagu",
          "render": function(data, type, row) {
            return `<div>
            <div style='font-size: 8px;border-bottom: 1px solid #3c8dbc;font-weight: 700'>PAGU</div>
            <div class='pkt_pagu' style='margin-bottom: 8px;font-weight: 700'>Rp ${row.rp_pagu}</div>
            <div style='font-size: 8px;border-bottom: 1px solid #00a65a;font-weight: 700'>HPS</div>
            <div class='pkt_hps' style='font-size: 11px;'>Rp ${row.rp_hps}</div>
            </div>
            `
          }
        }
      ]
    });
    table.on( 'click', 'tr', function ( e, dt, type, indexes ) {
      var pilih = table.row(this).data();
      let ulang = ""
      let eva = ""
      if (pilih.ulang) ulang = `<span class="label label-warning">${pilih.ulang}</span>`
      if (pilih.eva_versi > 1) eva = `<span class="label label-danger">Evaluasi Ulang</span>`
      $("#paket-info").html(`
      <div style="margin-bottom: 5px;">
        <span class="label label-primary">RUP : ${pilih.rup_id}</span>
        <span class="label label-success" id="info-lls-id">KODE : ${pilih.lls_id}</span>
        <span class="label label-warning" id="inf-spse-versi">${pilih.spse_versi}</span>
        ${ulang}
        ${eva}
      </div>
      <div class="uppercase" style="font-weight:700">${pilih.pkt_nama}</div>
      <div class="uppercase" style="color: #00a65a;"> <i class="fa fa-building-o space-right"></i> ${pilih.stk_nama}</div>
      <div><i class="fa fa-diamond space-right"></i>${pilih.sbd_id} ${pilih.ang_tahun}</div>
      <div><i class="fa fa-money space-right"></i>PAGU : <b>${pilih.rp_pagu}</b> HPS : <b>${pilih.rp_hps}</b></div>
      `);
      $(".ceklis").html(`<i class="fa fa-check-circle-o"></i>`);
      $("input[name='pkt_id']").val(pilih.pkt_id);
      $("input[name='lls_id']").val(pilih.lls_id);
      $("textarea[name='perihal']").val(pilih.pkt_nama);
      $("#create-paket").modal('hide');
    });
    $('#filterable').unbind();
    $('#filterable').bind('keyup', function(e) {
      if(e.keyCode == 13) {
        table.draw();
      }
    });
    $('#btn-filter').bind('click', function(e) {
      table.draw();
    });
  });
</script>
@endif
@endsection