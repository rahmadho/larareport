@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datepicker/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.css')}}">
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1>Surat Masuk</h1>
  </div>
  <div class="content">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab1" data-toggle="tab">SURAT UKPBJ</a></li>
          <li><a href="#tab2" data-toggle="tab">PAKET</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab1">
            <form class="suratmasuk" method="post" action="{{ route('monep.surat.edit', $result->id) }}">
              @csrf
              @method('PUT')
              <div class="box box-primary">
                <h3 class="box-header">
                </h3>
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Nomor Surat</label>
                        <input name="no_surat" value="{{$result->no_surat}}" type="text" class="form-control form-control-sm" />
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Tanggal Surat</label>
                        <input name="tgl_surat" value="{{$result->tgl_surat}}" data-date-format="yyyy-mm-dd" data-provide="datepicker-inline" type="text" class="form-control form-control-sm date-pick" />
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Ringkasan</label>
                    <textarea name="isi" rows="4" class="form-control form-control-sm">{{$result->perihal}}</textarea>
                  </div>
                  <div class="form-group">
                    <label>Surat usulan tender?</label>
                    <div>
                      <label class="switch">
                        <input name="is_tender" checked type="checkbox">
                        <span class="slider round"></span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="box-footer text-align-right">
                  <a href="{{url('monep/surat')}}" class="btn btn-warning">
                    <i class="fa fa-arrow-left space-right"></i>
                    KEMBALI
                  </a>
                  <button type="submit" class="btn btn-primary no-radius">
                    SIMPAN
                    <i class="fa fa-arrow-right space-left"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
          <div class="tab-pane" id="tab2">
            <div id="is-tender">
              <div class="box box-primary">
                <h3 class="box-header"></h3>
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="table-responsive">
                        <table class="table dataTable display " style="width:100%; font-size: 12px">
                          <thead class="silver">
                            <th style="max-width: 500px;width: 620px">Paket</th>
                            <th style="max-width: 200px">KPA/PPK</th>
                            <th>Pagu / HPS</th>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                @php
                                if ($result->paket->pkt_flag == 3) {
                                    $spse_versi = "SPSE 4.3";
                                } else if ($result->paket->pkt_flag == 2) {
                                    $spse_versi = "SPSE 4.2";
                                } else if ($result->paket->pkt_flag < 2) {
                                    $spse_versi = "SPSE 3";
                                }
                                @endphp
                                <div style="margin-bottom: 5px;">
                                  <span class="label label-success" id="info-lls-id">{{ $result->paket->lelang->lls_id }}</span>
                                  <span class="label label-warning" id="inf-spse-versi">{{$spse_versi}}</span>
                                  @if ($result->paket->ulang)
                                    <span class="label label-warning">{{$result->paket->ulang}}</span>
                                  @endif
                                  @if ($result->paket->lelang->eva_versi > 1)
                                    <span class="label label-danger">Evaluasi Ulang</span>
                                  @endif
                                </div>
                                <div class="uppercase" style="font-weight:700">
                                  <a href="{{route('monep.lelang.info', $result->paket->lelang->lls_id)}}">{{ $result->paket->pkt_nama }}</a>
                                </div>
                                <div class="uppercase" style="color: #00a65a;"> <i class="fa fa-building-o space-right"></i> {{$result->paket->satker->stk_nama}}</div>
                                <div><i class="fa fa-diamond space-right"></i><b>{{ $result->paket->sbd_id }}</b> {{ $result->paket->ang_tahun }}</div>
                              </td>
                              <td>
                                @if($result->paket->ppk)
                                <div><span class="label label-success">{{ $result->paket->ppk->peg_nip }}</span></div>
                                <div>{{ $result->paket->ppk->peg_nama }}</div>
                                @endif
                              </td>
                              <td>
                                <div>
                                  <div style='font-size: 8px;border-bottom: 1px solid #3c8dbc;font-weight: 700'>PAGU</div>
                                  <div class='pkt_pagu' style='margin-bottom: 8px;font-weight: 700'>Rp. {{ number_format($result->paket->pkt_pagu, 2) }}</div>
                                  <div style='font-size: 8px;border-bottom: 1px solid #00a65a;font-weight: 700'>HPS</div>
                                  <div class='pkt_hps' style='font-size: 11px;'>Rp. {{ number_format($result->paket->pkt_hps, 2) }}</div>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('/lib/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
@endsection