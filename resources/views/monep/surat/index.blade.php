@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.css')}}">
<style>
  /* .dataTable tbody tr:hover { cursor: pointer; } */
</style>
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1>Surat Masuk</h1>
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header clearfix">
            <div class="filterable pull-left">
              <div action="" method="post" class="form-inline">
                @csrf
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Pencarian..." id="filterable">
                </div>
                <div class="form-group">
                  <select name="tahun" id="filter-tahun" class="form-control">
                    @php 
                      $yNow = date('Y');
                      $yMin = $yNow-5;  // 2008; 
                    @endphp
                    @for ($i = $yNow; $i >= $yMin; $i--)
                      <option value="{{$i}}">{{$i}}</option>
                    @endfor
                  </select>
                </div>
                <div class="form-group">
                  <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                </div>
              </div>
            </div>
            <div class="pull-right">
              @if (\App\Helpers\Access::route('monep.surat.store'))
              <a href="{{ route('monep.surat.add') }}" class="btn btn-primary">
                <i class="fa fa-plus"></i> SURAT MASUK
              </a>
              @endif
            </div>
          </div>
          <div class="box-body">
            <table class="table dataTable table-stripped display responsive" width="100%">
              <thead class="">
                <th>Pengirim</th>
                <th>Nomor Surat</th>
                <th width="395">Perihal</th>
                <th>Tanggal Masuk</th>
                <th></th>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('/lib/datatables/datatables.min.js')}}"></script>
<script src="{{asset('/lib/datatables/1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function () {
    var url = "{{ route('api.monep.table.surat') }}";

    var table = $('.dataTable').DataTable({
      "searching": false,
      "ordering": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        "url": url,
        "dataType": "json",
        "type": "get",
        "data": function(data){
          let filterTahun = $("#filter-tahun").val();
          let filterable = $("#filterable").val();
          data._token = "{{csrf_token()}}";
          data.tahun = filterTahun;
          data.filter = filterable;
        }
      },
      "columns": [
        { 
          "data": "paket.satker.stk_nama",
          "render": function(data, type, row) {
            return `<div><i class="fa fa-building-o space-right"></i> ${row.paket.satker.stk_nama}</div>`
          }
        },
        { 
          "data": "no_surat",
        },
        { 
          "data": "perihal",
          "render": function(data, type, row) {
            return `<small>${row.perihal}</small>`
          }
        },
        { 
          "data": "tgl_input",
          "render": function(data, type, row) {
            let bulan = ["Januari", "Februari", "Maret","April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
            let tanggal = new Date(row.tgl_input);
            return `<div><i class="fa fa-calendar-check-o space-right"></i> ${tanggal.getDate()} ${bulan[tanggal.getMonth()]} ${tanggal.getFullYear()}</div>`
          }
        },
        {
          "data": "action",
          "render": function(data, type, row) {
            return `
            <a class="btn btn-sm btn-primary" data-btnaction="appdetail"><i class="fa fa-search-plus icon-btn"></i></a>
            <a class="btn btn-sm btn-danger" data-btnaction="appdelete"><i class="fa fa-trash icon-btn"></i></a>
            `
          }
        }
      ],
      fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
        $('td > a', nRow).on('click', function() {
          // if you have the property "data" in your columns, access via aData.property_name
          // if not, access data array from parameter aData, aData[0] = data for field 0, and so on...
          var btnAction = $(this).data('btnaction');
          if (btnAction === 'appdetail'){
            window.open("{{url('/')}}/monep/surat/"+aData.id+"/edit", '_blank');
          } else if (btnAction === 'appdelete'){
            let tanya = confirm("Ini akan mengakibatkan semua transaksi berkaitan dengan data ini akan dihapus! Apakah yakin akan menghapus data ini?");
            if (tanya){
              $.ajax({
                url: '{{route('ajax.delete','delete_surat')}}',
                data: {id: aData.id},
                type: 'delete',
                success:(dt,status) => {
                  console.log(status)
                }
              });
              table.draw();
            }
          }
        });
      }
    });
    table.on( 'click', 'tr td:eq(0)', function ( e, dt, type, indexes ) {
      var pilih = table.row(this).data();
      //window.open("{{url('/')}}/surat/"+pilih.id+"/edit", '_blank');
    });
    $('#filterable').unbind();
    $('#filterable').bind('keyup', function(e) {
      if(e.keyCode == 13) {
        table.draw();
      }
    });
    $('#btn-filter').bind('click', function(e) {
      table.draw();
    });
  });
</script>
@endsection