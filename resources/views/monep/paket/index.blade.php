@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datepicker/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/timepicker/css/bootstrap-timepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.css')}}">
<style>
  .content-header h1 { margin: 0; }
  #datatable-paket tbody tr:hover { cursor: pointer; }
  #datatable-paket_filter { visibility: hidden; display: none; }
  .space-right { margin-right: 8px; }
</style>
@endsection
@section('content')
<div class="page-template">
  @if (\App\Helpers\Access::route('paket.edit'))
  <div class="content-header">
    <div class="row">
      <div class="col-md-12">
        <h1 class="pull-left">Paket</h1>
      </div>
    </div>
  </div>
  @endif
  
  <div class="content">
    <div class="box">
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <div class="filterable pull-left">
              <div action="" method="post" class="form-inline">
                @csrf
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Pencarian..." id="filterable">
                </div>
                <div class="form-group">
                  <select name="tahun" id="filter-tahun" class="form-control">
                    @for ($i = date('Y'); $i >= 2011; $i--)
                      <option value="{{$i}}">{{$i}}</option>
                    @endfor
                  </select>
                </div>
                <div class="form-group">
                  <select name="pokjaPemilihan" id="filter-pokja" class="form-control">
                    <option value="1">Filter Pokja Pemilihan</option>
                    <option value="2">Pokja Sudah Dipilih</option>
                    <option value="3">Pokja Belum Dipilih</option>
                  </select>
                </div>
                <div class="form-group">
                  <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                </div>
              </div>
            </div>
            <div class="pull-right">
              <div class="form-inline">
                <span>POKJA PEMILIHAN</span>
                <div class="form-group">
                  <label class="switch" style="margin-top:2px">
                    <input name="has_pokja" type="checkbox">
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box box-primary">
      <div class="box-body">
        <section class="">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-striped" id="datatable-paket" style="width:100%">
                <thead>
                  <th style="max-width: 650px">Paket</th>
                  <th style="max-width: 200px">KPA/PPK</th>
                  <th>Pagu / HPS</th>
                </thead>
              </table>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('/lib/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('/lib/timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script src="{{asset('/lib/datatables/datatables.min.js')}}"></script>
<script src="{{asset('/lib/datatables/1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#datatable-paket').DataTable({
      "searching": false,
      "ordering": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        "url": "{{ route('api.monep.table.paket') }}",
        "dataType": "json",
        "type": "get",
        "data": function(data){
          let filterTahun = $("#filter-tahun").val();
          let filterable = $("#filterable").val();
          let has_pokja = $('input[name="has_pokja"]').prop('checked');
          let filterPokja = $("#filter-pokja").val();
          data._token = "{{csrf_token()}}";
          data.tahun = filterTahun;
          data.filter = filterable;
          data.has_pnt = has_pokja;
          data.filter_pnt = filterPokja;
        }
      },
      "columns": [
        { 
          "data": "pkt_nama",
          "render": function(data, type, row) {
            let ulang = "";
            let tayang = "";
            let on_sibaja = "";
            let evaluasi_ulang = "";
            let pokja = "";
            if (row.ulang) {
              ulang = `<span class="label label-warning space-right">${row.ulang}</span>`
            }
            if (row.lelang.eva_versi > 1) {
              evaluasi_ulang = `<span class="label label-danger space-right">Evaluasi Ulang</span>`
            }
            if(row.lelang.lls_status == 0){
              tayang = `<i class="fa fa-star-o space-right" style="color: orange !important"></i>`
            } else if(row.lelang.lls_status == 1){
              tayang = `<i class="fa fa-star space-right" style="color: orange !important"></i>`
            } else if(row.lelang.lls_status == 2){
              tayang = `<i class="fa fa-star space-right" style="color: red !important"></i>`
            }
            if(row.on_sibaja === true){
              on_sibaja = `<i class="fa fa-check space-right" style="color: green !important"></i>`
            }
            if(row.pnt_nama) {
              pokja = `<span class="label label-primary space-right" style='text-transform: capitalize'>${row.pnt_nama}</span>`
            }
            return `
            <div>
              ${on_sibaja}
              ${tayang}
              <span class="label label-info space-right">${row.lelang.lls_id}</span>
              <span class="label label-success space-right">${row.spse_versi}</span>
              ${ulang}
              ${evaluasi_ulang}
              ${pokja}
            </div>
            <div><a><b>${data}</b></a></div>
            <div><small><i class="fa fa-building-o space-right"></i> ${row.satker.stk_nama}</small></div>
            <div><small><i class="fa fa-flag-o space-right"></i><b>${row.kategori_lelang.nama} - ${row.sbd_id} - ${row.ang_tahun}</b></small></div>
            `
          }
        },
        { 
          "data": "ppk",
          "render": function(data, type, row) {
            if(row.ppk != null){
              return `<div><span class="label label-success">${row.ppk.peg_nip}</span></div><div class="status_lelang"><small>${row.ppk.peg_nama}</small></div>`
            } else {
              return `<div class="status_lelang"><small>Tidak ada KPA / PPK </small></div>`
            }
          }
        },
        { 
          "data": "pkt_pagu",
          "render": function(data, type, row) {
            return `<div>
            <div style='font-size: 8px;border-bottom: 1px solid #3c8dbc;font-weight: 700'>PAGU</div>
            <div class='pkt_pagu' style='margin-bottom: 8px;font-weight: 700'>Rp ${row.rp_pagu}</div>
            <div style='font-size: 8px;border-bottom: 1px solid #00a65a;font-weight: 700'>HPS</div>
            <div class='pkt_hps' style='font-size: 11px;'>Rp ${row.rp_hps}</div>
            </div>
            `
          }
        }
      ]
    });
    table.on( 'click', 'tr', function ( e, dt, type, indexes ) {
      var pilih = table.row(this).data();
      window.open('{{url('/')}}/monep/lelang/'+pilih.lelang.lls_id+'/info', '_blank');
    });
    $('#filterable').unbind();
    $('#filterable').bind('keyup', function(e) {
      if(e.keyCode == 13) {
        table.draw();
      }
    });

    $('#btn-filter').bind('click', function(e) {
      console.log('filter tahun '+$("#filter-tahun").val())
      table.draw();
    });
    $('input[name="has_pokja"]').bind('change', function(e) {
      table.draw();
    });
  });
</script>
@endsection