@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.css')}}">
<style>
  .content-header h1 { margin: 0; }
  #datatable-paket tbody tr:hover { cursor: pointer; }
  #datatable-paket_filter { visibility: hidden; display: none; }
  .space-right { margin-right: 8px; }
</style>
@endsection
@section('content')
<div class="page-template">
  @if (\App\Helpers\Access::route('paket.edit'))
  <div class="content-header">
    <div class="row">
      <div class="col-md-12">
        <h1 class="pull-left">Paket</h1>
      </div>
    </div>
  </div>
  @endif
  
  <div class="content">
    <div class="box">
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <div class="filterable pull-left">
              <div action="" method="post" class="form-inline">
                @csrf
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Pencarian..." id="filterable">
                </div>
                <div class="form-group">
                  <select name="filter_stk" id="filter_stk" class="form-control" style="max-width:320px;">
                    <option value="">Satuan Kerja</option>
                    @foreach ($stk_nama as $item)
                      <option value="{{$item}}">{{$item}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <select name="filter_metode" id="filter_metode" class="form-control">
                    <option value="">Metode Pemilihan</option>
                    @foreach ($mtd_pemilihan as $item)
                      <option value="{{$item}}">{{$item}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <input type="number" class="form-control" placeholder="Minimal Pagu" id="filter_pagu">
                </div>
                <div class="form-group">
                  <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                </div>
              </div>
            </div>
            <div class="pull-right"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="box box-primary">
      <div class="box-body">
        <section class="">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-striped" id="datatable-paket" style="width:100%">
                <thead>
                  <th>RUP ID</th>
                  <th>Satuan Kerja</th>
                  <th>Paket</th>
                  <th>Pagu</th>
                  <th>Metode Pemilihan</th>
                  <th>Sumber</th>
                  <th>Waktu</th>
                </thead>
                <tbody style="font-size:12px;"></tbody>
              </table>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('/lib/datatables/datatables.min.js')}}"></script>
<script src="{{asset('/lib/datatables/1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#datatable-paket').DataTable({
      "searching": false,
      "ordering": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        "url": "{{ route('api.monep.table.paket_rup') }}",
        "dataType": "json",
        "type": "get",
        "data": function(data){
          let filterable = $("#filterable").val();
          let filter_stk = $("#filter_stk").val();
          let filter_mtd = $("#filter_metode").val();
          let filter_pgu = $("#filter_pagu").val();
          data._token = "{{csrf_token()}}";
          data.filter = filterable;
          data.filter_stk = filter_stk;
          data.filter_metode = filter_mtd;
          data.filter_pagu = filter_pgu;
        }
      },
      "columns": [
        { 
          "data": "rup_id"
        },
        { 
          "data": "stk_nama"
        },
        { 
          "data": "pkt_nama"
        },
        { 
          "data": "pkt_pagu",
          "render": function(data, type, row) {
            return `<span>${formatPrice(row.pkt_pagu)}</span>`
          }
        },
        { 
          "data": "mtd_pemilihan"
        },
        { 
          "data": "sbd_id"
        },
        { 
          "data": "waktu_pemilihan"
        }
      ]
    });
    $('#filterable').unbind();
    $('#filterable').bind('keyup', function(e) {
      if(e.keyCode == 13) {
        table.draw();
      }
    });
    $('#btn-filter').bind('click', function(e) {
      table.draw();
    });
  });
  function formatPrice(value) {
    let val = (value / 1).toFixed(2).replace(".", ",");
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }
</script>
@endsection