@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datepicker/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/timepicker/css/bootstrap-timepicker.min.css')}}">
<style>
  .content-header h1 { margin: 0; }
  #datatable-rekanan tbody tr:hover { cursor: pointer; }
  #paket-pemenang-add input, #paket-penawar-add input { text-transform: uppercase; }
  #table-tab-info-paket tbody td:nth-child(1) { font-weight: 700; }
</style>
@endsection
@section('content')
<div class="page-template">
  <div class="content">
    {{-- <div class="box">
      <div class="box-body">
        <div class="pull-leftx">
          <a href="{{url()->previous()}}" class="btn btn-info"><i class="fa fa-history space-right"></i> History Paket</a>
        </div>
      </div>
    </div> --}}
    <div class="box box-primary">
      <div class="box-body">
        <section>
          <div class="row">
            <div class="col-xs-12">
              <div class="flex align-item-center">
                <div class="paket-info">
                  <div style="margin-bottom: 5px;">
                    <span class="label label-warning">{{$result->spse_versi}}</span> <span class="label label-info">{{$result->kategori_lelang['nama']}}</span>
                  </div>
                  <div class="uppercase"><i class="fa fa-bank space-right"></i><b>{{$result->satker->instansi->nama}}</b></div>
                  <div class="uppercase" style="color: #00a65a;"> <i class="fa fa-building-o space-right"></i> {{$result->satker->stk_nama}}</div>
                  <div><i class="fa fa-diamond space-right"></i>{{$result->sbd_id}} {{$result->ang_tahun}}</div>
                  <div>
                    <span class="space-right"><i class="fa fa-money space-right"></i>PAGU : <b>{{$result->rp_pagu}}</b> </span>
                    <span class="space-right"><i class="fa fa-money space-right"></i> HPS : <b>{{$result->rp_hps}}</b></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          @foreach ($result->lelangs as $item)
          <div class="row">
            <div class="col-xs-12">
              <div class="flex align-item-center">
                <div class="qrcode">
                  {!! QrCode::size(180)->generate($item->lls_id) !!}
                </div>
                <div class="paket-info">
                  <div style="margin-bottom: 5px;">
                    <span class="label label-primary">{{$item->lls_id}}</span>
                    <span class="label label-info">{{$item->metode_pemilihan['nama']}}</span>
                    <span class="label label-success">{{$item->status_lelang['nama']}}</span>
                    @if ($item->lls_versi_lelang > 1)
                      <span class="label label-warning">Tender Ulang</span>
                    @endif
                  </div>
                  <div class="paket_nama uppercase" style="margin-bottom: 5px; line-height: 2">
                    {{$result->pkt_nama}}
                  </div>
                  <div>
                    <span class="space-right"><i class="fa fa-calendar space-right"></i> <small><b>Dibuat :</b>{{App\Helpers\Tanggal::dmy($item->lls_dibuat_tanggal)}}</small></span>
                    <span class="space-right"><i class="fa fa-calendar space-right"></i> <small><b>Disetujui :</b>{{App\Helpers\Tanggal::dmy($item->lls_tgl_setuju)}}</small></span>
                  </div>
                  <div>
                    <i class="fa fa-flag space-right"></i>
                    <small><b>Diulang Karena :</b>{{$item->lls_diulang_karena}}</small>
                  </div>
                  <div>
                    <i class="fa fa-flag space-right"></i>
                    <small><b>Ditutup Karena :</b>{{$item->lls_ditutup_karena}}</small>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </section>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@endsection