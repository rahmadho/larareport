@extends('layout')
@section('css')
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1 class="uppercase">{{$panitia->pnt_nama}}</h1>
  </div>
  <div class="content">
    <div class="box box-primary">
      <div class="box-header clearfix">
        <div class="pull-left uppercase">anggota panitia</div>
        <div class="pull-right">
        </div>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <table class="table dataTable display responsive no-wrap" width="100%">
            <thead class="silver">
              <th>No</th>
              <th>Pegawai</th>
              <th>Golongan</th>
              <th>Jabatan</th>
            </thead>
            <tbody>
              <?php $no = 1; ?>
              @foreach($panitia->pegawai as $peg)
              <tr>
                <td>{{$no++}}</td>
                <td>
                  <span class="label label-info">{{$peg->peg_nip}}</span>
                  {{$peg->peg_nama}}
                </td>
                <td class="uppercase">
                  {{$peg->peg_pangkat}} / {{$peg->peg_golongan}}
                </td>
                <td>
                  {{$peg->pivot->agp_jabatan}}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('template')
@endsection
@section('js')
@endsection