@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.css')}}">
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1>Panitia / Pokja</h1>
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header clearfix">
            <div class="pull-left">PANITIA / POKJA</div>
            <div class="pull-right"></div>
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table class="table dataTable display responsive no-wrap" width="100%">
                <thead class="silver">
                  <th style="max-width: 45px">No</th>
                  <th>Panitia</th>
                  <th>Status</th>
                </thead>
                <tbody>
                  <?php $no = 1; ?>
                  @foreach($panitia as $pokja)
                  <tr>
                    <td>{{$no++}}</td>
                    <td>
                      <a href="{{route('panitia.info', $pokja->pnt_id)}}" class="uppercase">{{$pokja->pnt_nama}}</a>
                      <span class="label label-primary">{{count($pokja->pegawai)}} anggota</span>
                    </td>
                    <td>
                      @if($pokja->is_active)
                      <span class="label label-success">Aktif</span>
                      @else
                      <span class="label label-danger">Tidak Aktif</span>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="box box-info">
          <div class="box-header clearfix">
            <div class="pull-left">PEGAWAI UKPBJ</div>
            <div class="pull-right"></div>
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table class="table dataTable display responsive no-wrap" width="100%">
                <thead class="silver">
                  <th>No</th>
                  <th>Pegawai</th>
                  <th>Golongan</th>
                  <th>Pangkat</th>
                  <th>Jabatan</th>
                </thead>
                <tbody>
                  <?php $no = 1; ?>
                  @foreach($pegawai as $peg)
                  <tr>
                    <td>{{$no++}}</td>
                    <td class="uppercase">
                      <a href="{{route('monep.pegawai.edit', $peg->peg_id)}}">
                        <span class="label label-info">{{$peg->pegawai->peg_nip}}</span>
                      </a>
                      {{$peg->pegawai->peg_nama}}
                    </td>
                    <td class="uppercase">
                      {{$peg->pegawai->peg_golongan}}
                    </td>
                    <td>
                      {{$peg->pegawai->peg_pangkat}}
                    </td>
                    <td>
                      {{$peg->pegawai->peg_jabatan}}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('/lib/datatables/datatables.min.js')}}"></script>
<script src="{{asset('/lib/datatables/1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.js')}}"></script>
<script>
  $(document).ready( function () {
    $('.dataTable').DataTable({
      responsive: true,
      "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
    });
  } );
</script>
@endsection