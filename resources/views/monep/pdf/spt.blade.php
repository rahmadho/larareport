<html>
<head>
	<meta charset="UTF-8">
	<title>SPT | {{$paket->panitia->pnt_nama}}</title>
	<link rel="stylesheet" href="{{asset('/css/print.css')}}">
	<style>
		.no-2 { display: block; text-align: center; }
		span.surat-number { font-weight: 700; }
	</style>
</head>
<body>
	<div class="heading">
		<div class="logo">
			<img src="{{asset('sumbar.png')}}" alt="logo">
		</div>
		<div class="text-align-center">
			<div class="title-1">PEMERINTAH PROVINSI SUMATERA BARAT</div>
			<div class="title-1">SEKRETARIAT DAERAH</div>
			<div class="title-2 bold">UNIT KERJA PENGADAAN BARANG/JASA</div>
			<div class="title-3">Jalan Jenderal Sudirman No 51 Padang 25112, Telepon (0751)31401 - 31402 - 34425</div>
			<div class="title-3">Fax (0751)34671, www.sumbarprov.go.id, e-mail: ulp_sumbar@sumbarprov.go.id</div>
		</div>
	</div>
	<hr>
	<div class="no-surat text-align-center">
		<div class="no-1 bold">SURAT TUGAS</div>
		<div class="no-2">
			Nomor: 020/ <span class="surat-number">@if($paket->spt->isNotEmpty()){{$paket->spt->last()->no_spt}} @else &nbsp; &nbsp; &nbsp; &nbsp; @endif</span> /BAP2BMD-UKPBJ/{{$paket->ang_tahun}}
		</div>
	</div>

	<p class="indent">Berdasarkan ketentuan Pasal 13 Peraturan Presiden Nomor 16 Tahun 2018 tentang Pengadaan Barang/Jasa Pemerintah serta Pasal 16 Peraturan Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah Nomor 14 Tahun 2018, maka dengan ini kami tugaskan kepada :</p>
	<div class="pokja-pilihan bold">Kelompok Kerja (POKJA) - {{$paket->panitia->pnt_nama}}</div>
	<table class="table table-striped border">
		<thead>
			<tr>
				<th class="text-align-center" width="20">No</th>
				<th class="text-align-center">Nama</th>
				<th class="text-align-center">NIP/Pangkat/Gol</th>
				<th class="text-align-center">Kedudukan dalm Pokja</th>
			</tr>
		</thead>
		@if($paket->panitia)
		<tbody>
			<?php $no = 1; ?>
			@foreach($paket->panitia->pegawai as $peg)
			<tr>
				<td class="text-align-center">{{$no++}}</td>
				<td class="bold uppercase">{{$peg->peg_nama}}</td>
				<td class="uppercase">
					<div>Nip. {{$peg->peg_nip}}</div>
					<div>{{$peg->peg_pangkat}}, ({{$peg->peg_golongan}})</div>
				</td>
				<td>Pokja Pemilihan</td>
			</tr>
			@endforeach
		</tbody>
		@endif
	</table>
	<p>untuk melaksanakan pemilihan Penyedia pada :</p>
	<div class="paket">
		<table>
			<tr>
				<td width="100">OPD</td>
				<td width="5">:</td>
				<td>{{$paket->satker->stk_nama}}</td>
			</tr>
			<tr>
				<td>KEGIATAN/PAKET</td>
				<td>:</td>
				<td>{{$paket->pkt_nama}}</td>
			</tr>
			<tr>
				<td>PAGU DANA</td>
				<td>:</td>
				<td>Rp. {{number_format($paket->pkt_pagu,2,',','.')}}</td>
			</tr>
			<tr>
				<td>HPS</td>
				<td>:</td>
				<td>Rp. {{number_format($paket->pkt_hps,2,',','.')}}</td>
			</tr>
			<tr>
				<td>SUMBER DANA</td>
				<td>:</td>
				<td>{{$paket->sbd_id}} Tahun {{$paket->ang_tahun}}</td>
			</tr>
		</table>
	</div>
	<p>Selanjutnya Saudara dapat memproses pengadaan ini sesuai tugas pokok dan kewenangan Kelompok Kerja Unit Kerja Pengadaan Barang/Jasa, yaitu :</p>
	<ol>
		<li>Melaksanakan persiapan dan pelaksanaan pemilihan Penyedia;</li>
		<li>Melaksanakan persiapan dan pelaksanaan pemilihan Penyedia untuk katalog elektronik;</li>
		<li>
			Menetapkan pemenang pemilihan / Penyedia untuk metode pemilihan;
			<ul class="abjad">
				<li>Tender/Penunjukan Langsung untuk paket Pengadaan Barang/Pekerjaan Konstruksi/Jasa Lainnya dengan Pagu Anggaran paling banyak Rp. 100.000.000.000,- (seratus miliar rupiah); dan</li>
				<li>Seleksi/Penunjukan Langsung untuk paket Pengadaan Jasa Konsultasi dengan nilai Pagu Anggaran paling banyak Rp. 10.000.000.000,- (sepuluh miliar rupiah).</li>
			</ul>
		</li>
		<li>Membuat laporan mengenai proses pengadaan kepada Kepala Bagian Pengadaan Barang/Jasa;</li>
	</ol>
	<p class="indent">Demikian surat tugas ini dibuat, untuk dapat dipergunakan dan dilaksanakan sesuai dengan ketentuan dan peraturan yang berlaku. Terima kasih.</p>
	
	<div class="ttd-wrapper text-align-center">
		<div class="ttd">
			@php 
				$tanggal = ($paket->spt->isNotEmpty()) ? \App\Helpers\Tanggal::dmy($paket->spt->last()->tgl_input) : $tanggal = \App\Helpers\Tanggal::dmy(date('Y-m-d'));
			@endphp
			<div class="ttd-tgl">Padang, {{ $tanggal }}</div>
			<div class="ttd-by">KEPALA BAGIAN<br>PENGADAAN BARANG/JASA</div>
			<div class="ttd-space"></div>
			<div class="ttd-nama bold">ZULKARNAINI, S.T</div>
			<div class="nip">NIP. 197204031992021001</div>
			</div>
	</div>

	<div class="tebusan">
		Tebusan disampaikan kepada Yth. :
		<ol>
			<li>Kepala {{$paket->satker->stk_nama}}</li>
			<li>Arsip</li>
		</ol>
	</div>
</body>
</html>