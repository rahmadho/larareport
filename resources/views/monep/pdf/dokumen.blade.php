<html>
<head>
	<meta charset="UTF-8">
	<title>SPT | Surat Perintah Tugas</title>
	<link rel="stylesheet" href="{{asset('/css/print.css')}}">
	<style>
		.catatan { margin-top: 45px; }
	</style>
</head>
<body>
	<div class="heading">
		<div class="logo">
			<img src="{{asset('sumbar.png')}}" alt="logo">
		</div>
		<div class="text-align-center">
			<div class="title-1">PEMERINTAH PROVINSI SUMATERA BARAT</div>
			<div class="title-1">SEKRETARIAT DAERAH</div>
			<div class="title-2 bold">UNIT KERJA PENGADAAN BARANG/JASA</div>
			<div class="title-3">Jalan Jenderal Sudirman No 51 Padang 25112, Telepon (0751)31401 - 31402 - 34425</div>
			<div class="title-3">Fax (0751)34671, www.sumbarprov.go.id, e-mail: ulp_sumbar@sumbarprov.go.id</div>
		</div>
	</div>
	<hr>
	<div class="no-surat text-align-center">
		<div class="no-1 bold">CEK LIST KELENGKAPAN DOKUMEN LELANG</div>
		<div class="no-1 bold">YANG DISAMPAIKAN KEPADA PENGADAAN BARANG/JASA</div>
	</div>

	<table class="table border bold">
		<tbody>
			<tr>
				<td>OPD</td>
				<td align="center">:</td>
				<td colspan="2">{{$paket->satker->stk_nama}}</td>
			</tr>
			<tr>
				<td>Nama Kegiatan</td>
				<td align="center">:</td>
				<td colspan="2">
					@if ($paket->surat)
						{{$paket->surat->perihal}}
					@else
						{{$paket->pkt_nama}}
					@endif
				</td>
			</tr>
			<tr>
				<td>Nama Paket</td>
				<td align="center">:</td>
				<td colspan="2">
					@if ($paket->surat)
						{{$paket->surat->perihal}}
					@else
						{{$paket->pkt_nama}}
					@endif
				</td>
			</tr>
			<tr>
				<td>Pagu Dana</td>
				<td align="center">:</td>
				<td>Rp. {{number_format($paket->pkt_pagu, 2, ',', '.')}}</td>
				<td>Sumber Dana : {{$paket->sbd_id}}</td>
			</tr>
			<tr>
				<td>HPS</td>
				<td align="center">:</td>
				<td colspan="2">Rp. {{number_format($paket->pkt_hps, 2, ',', '.')}}</td>
			</tr>
			<tr>
				<td>Nama KPA/PPK</td>
				<td align="center">:</td>
				<td colspan="2">{{ $paket->ppk->peg_nama }} / NIP. {{ $paket->ppk->peg_nip }}</td>
			</tr>
			<tr>
				<td>Tgl Usulan & Terima Dokumen</td>
				<td align="center">:</td>
				<td colspan="2">
					@if ($paket->surat)
						{{\Carbon\Carbon::parse($paket->surat->tgl_surat)->formatLocalized('%d %B %Y')}} / {{\App\Helpers\Tanggal::dmy($paket->surat->tgl_input)}}
					@else 
						-
					@endif
				</td>
			</tr>
		</tbody>
	</table>

	<div class="dokumen">
		<h4>Dokumen {{$paket->kategori['nama']}}</h4>
		<table class="border">
			<thead>
				<tr>
					<th class="text-align-center" width="20">No</th>
					<th class="text-align-center">Dokumen</th>
					<th class="text-align-center" width="80">Kelengkapan</th>
				</tr>
			</thead>
			<tbody>
				@php $no = 1; @endphp
				@foreach ($paket->dokumen as $doc)
					<tr>
						<td class="text-align-center">{{$no++}}</td>
						<td>{{$doc->nama}}</td>
						<td class="text-align-center">@if($doc->ada == true) Ada @else Tidak @endif</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<div class="catatan">
		<div>Catatan :</div>
		<div class="flex-align-center">
			<input type="checkbox" class="inline-block"> 
			<span class="inline-block">Dikembalikan</span>
		</div>
		<div class="flex-align-center">
			<input type="checkbox" class="inline-block">
			<span class="inline-block">
				@if($paket->panitia)
					Diteruskan ke {{$paket->panitia->pnt_nama}}
				@else
					Diteruskan ke ............
				@endif
			</span>
		</div>
	</div>
</body>
</html>