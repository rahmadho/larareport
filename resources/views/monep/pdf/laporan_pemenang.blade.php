<html>
<head>
	<meta charset="UTF-8">
	<title>SPT | POKJA {{$paket->panitia->last()->pnt_nama}}</title>
	{{-- <link href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> --}}
	<link rel="stylesheet" href="{{asset('/css/print.css')}}">
	<style>
		body { padding: 0px 35px 0px 35px; }
		.catatan { margin-top: 45px; }
		ol { padding-left: 20px; }
		.fsize-14, .fsize-14 * { font-size: 15px; }
		table.v-top th, table.v-top td{ vertical-align: top; }
		table.panitia-pnt td{ padding-bottom: 35px; }
		table.panitia-pnt td.ttd_right{ padding-left: 150px; }
		.tembusan {position: absolute; bottom: 15px;}
		.tembusan small { position: relative }
	</style>
</head>
<body>
	<div class="heading">
		<div class="logo">
			<img src="{{asset('sumbar.png')}}" alt="logo">
		</div>
		<div class="text-align-center">
			<div class="title-1" style="font-size: 20px">PEMERINTAH PROVINSI SUMATERA BARAT</div>
			<div class="title-1" style="font-size: 20px">SEKRETARIAT DAERAH</div>
			<div class="title-2 bold" style="font-size: 22px">UNIT KERJA PENGADAAN BARANG/JASA</div>
			<div class="title-3">Jalan Jenderal Sudirman No 51 Padang 25112, Telepon (0751)31401 - 31402 - 34425</div>
			<div class="title-3">Fax (0751)34671, www.sumbarprov.go.id, e-mail: ulp_sumbar@sumbarprov.go.id</div>
		</div>
	</div>
	<hr>
	@php
		$no_surat = "07/ST-{$paket->panitia->last()->pivot->no_surat}/Pokja-{$paket->panitia->last()->pnt_nama}/UKPBJ/".Carbon\Carbon::parse($paket->persiapan->last()->tgl_rapat)->formatLocalized('%Y');
		$pemenang = $paket->pemenang->where('pivot.urutan', 1)->first();
		$cadangan1 = $paket->pemenang->where('pivot.urutan', 2)->first();
		$cadangan2 = $paket->pemenang->where('pivot.urutan', 3)->first();
		$ketua_pokja = $paket->panitia->last()->anggota->where('pivot.jabatan', 'Ketua')->first();
		// $metode_dokumen = "Pasca Kualifikasi";
		$metode_dokumen = ($paket->metode_kualifikasi) ? $paket->metode_kualifikasi->nama : '';
	@endphp
	<table class="table text-align-justify fsize-14 v-top">
		<tbody>
			<tr>
				<td width="52">Nomor</td>
				<td width="5">:</td>
				<td width="170">{{$no_surat}}</td>
				<td class="text-align-right">Padang, {{\App\Helpers\Tanggal::dmy(date('Y-m-d'))}}</td>
			</tr>
			<tr>
				<td>Lampiran</td>
				<td>:</td>
				<td>1 (satu) berkas</td>
				<td></td>
			</tr>
			<tr>
				<td>Perihal</td>
				<td>:</td>
				<td>Laporan Hasil Pemilihan</td>
				<td>Kepada Yth : <br>KPA Paket Pengadaan {{$paket->pkt_nama}} <br><div class="capitalize">{{strtolower($paket->satker->stk_nama)}}</div>di <br><div style="margin-left:18px">Tempat</div></td>
			</tr>
		</tbody>
	</table>
	<p class="text-align-justify fsize-14">
		Berkenaan dengan Paket Pengadaan Barang dan Jasa :
	</p>
	<table class="table text-align-justify fsize-14 v-top">
		<tbody>
			<tr>
				<td width="145"><b>Kode</b></td>
				<td width="10">:</td>
				<td class="uppercase"><b>{{$paket->pkt_id}}</b></td>
			</tr>
			<tr>
				<td><b>Nama Tender</b></td>
				<td>:</td>
				<td>{{$paket->pkt_nama}}</td>
			</tr>
			<tr>
				<td><b>Instansi</b></td>
				<td>:</td>
				<td class="capitalize">Pemerintah Provinsi Sumatera Barat</td>
			</tr>
			<tr>
				<td><b>Satuan Kerja</b></td>
				<td>:</td>
				<td class="capitalize">{{$paket->satker->stk_nama}}</td>
			</tr>
			<tr>
				<td><b>Kategori</b></td>
				<td>:</td>
				<td class="capitalize">{{$paket->jenis->nama}}</td>
			</tr>
			<tr>
				<td><b>Metode Pengadaan</b></td>
				<td>:</td>
				<td class="capitalize">{{$paket->metode->nama}}</td>
			</tr>
			<tr>
				<td><b>Metode Dokumen</b></td>
				<td>:</td>
				<td class="capitalize">{{$metode_dokumen}}</td>
			</tr>
			<tr>
				<td><b>Nilai Pagu Paket</b></td>
				<td>:</td>
				<td><b>Rp {{number_format($paket->pkt_pagu, 2, '.', ',')}} ,-</b></td>
			</tr>
			<tr>
				<td><b>Nilai HPS</b></td>
				<td>:</td>
				<td><b>Rp {{number_format($paket->pkt_hps, 2, '.', ',')}} ,-</b></td>
			</tr>
		</tbody>
	</table>
	<p class="text-align-justify fsize-14">
		Telah dilaksanakan proses pengadaannya oleh Kelompok Kerja (POKJA) {{$paket->panitia->last()->pnt_nama}} 
		Unit Kerja Pengadaan Barang dan Jasa Provinsi Sumatera Barat dan Berdasarkan Berita Acara Hasil Pemilihan Nomor 
		06/ST-{{$paket->panitia->last()->pivot->no_surat}}/Pokja {{$paket->panitia->last()->pnt_nama}}/UKPBJ/{{date('Y')}},
		pemenang paket Pekerjaan / Pengadaan tersebut diatas adalah sebagai berikut :
	</p>
	{{-- <div class="fsize-14"><b><u>Pemenang</u></b></div><br> --}}
	<table class="table text-align-left fsize-14 v-top">
		<tbody>
			<tr>
				<td width="175"><b>Nama Pemenang</b></td>
				<td width="10">:</td>
				<td class="uppercase"><b>{{$pemenang->rkn_nama}}</b></td>
			</tr>
			<tr>
				<td><b>NPWP</b></td>
				<td>:</td>
				<td>{{$pemenang->rkn_npwp}}</td>
			</tr>
			<tr>
				<td><b>Alamat</b></td>
				<td>:</td>
				<td class="capital">{{$pemenang->rkn_alamat}}</td>
			</tr>
			<tr>
				<td><b>Nilai Penawaran / Negosiasi</b></td>
				<td>:</td>
				<td><b>Rp {{number_format($pemenang->pivot->harga, 2, '.', ',')}} ,-</b><br></td>
			</tr>
		</tbody>
	</table>
	<br>
	<br>
	{{-- <div class="fsize-14"><b><u>Pemenang Cadangan 1</u></b></div> --}}
	<br>
	@if ($cadangan1)
		<table class="table text-align-left fsize-14 v-top">
			<tbody>
				<tr>
					<td width="145"><b>Nama Pemenang Cadangan</b></td>
					<td width="10">:</td>
					<td class="uppercase"><b>{{$cadangan1->rkn_nama}}</b></td>
				</tr>
				<tr>
					<td><b>NPWP</b></td>
					<td>:</td>
					<td>{{$cadangan1->rkn_npwp}}</td>
				</tr>
				<tr>
					<td><b>Alamat</b></td>
					<td>:</td>
					<td class="capital">{{$cadangan1->rkn_alamat}}</td>
				</tr>
				<tr>
					<td><b>Nilai Penawaran / Reverse Action</b></td>
					<td>:</td>
					<td><b>Rp {{number_format($cadangan1->pivot->harga, 2, '.', ',')}} ,-</b><br></td>
				</tr>
			</tbody>
		</table>
	@else 
		<br>
		<table class="table text-align-justify fsize-14 v-top">
			<tbody>
				<tr>
					<td width="145"><b>Nama Pemenang</b></td>
					<td width="10">:</td>
					<td class="uppercase"><b>-</b></td>
				</tr>
				<tr>
					<td><b>NPWP</b></td>
					<td>:</td>
					<td>-</td>
				</tr>
				<tr>
					<td><b>Alamat</b></td>
					<td>:</td>
					<td class="capital">-</td>
				</tr>
				<tr>
					<td><b>Nilai Penawaran / Reverse Action</b></td>
					<td>:</td>
					<td><b>-</b><br></td>
				</tr>
			</tbody>
		</table>
	@endif
	<div class="page-break"></div>
	{{-- <div class="fsize-14"><b><u>Pemenang Cadangan 2</u></b></div> --}}
	<br>
	@if ($cadangan2)
		<table class="table text-align-left fsize-14 v-top">
			<tbody>
				<tr>
					<td width="180"><b>Nama Pemenang Cadangan</b></td>
					<td width="10">:</td>
					<td class="uppercase"><b>{{$cadangan2->rkn_nama}}</b></td>
				</tr>
				<tr>
					<td><b>NPWP</b></td>
					<td>:</td>
					<td>{{$cadangan2->rkn_npwp}}</td>
				</tr>
				<tr>
					<td><b>Alamat</b></td>
					<td>:</td>
					<td class="capital">{{$cadangan2->rkn_alamat}}</td>
				</tr>
				<tr>
					<td><b>Nilai Penawaran / Reverse Action</b></td>
					<td>:</td>
					<td><b>Rp {{number_format($cadangan2->pivot->harga, 2, '.', ',')}} ,-</b><br></td>
				</tr>
			</tbody>
		</table>
	@else 
		<br>
		<table class="table text-align-justify fsize-14 v-top">
			<tbody>
				<tr>
					<td width="145"><b>Nama Pemenang</b></td>
					<td width="10">:</td>
					<td class="uppercase"><b>-</b></td>
				</tr>
				<tr>
					<td><b>NPWP</b></td>
					<td>:</td>
					<td>-</td>
				</tr>
				<tr>
					<td><b>Alamat</b></td>
					<td>:</td>
					<td class="capital">-</td>
				</tr>
				<tr>
					<td><b>Nilai Penawaran / Reverse Action</b></td>
					<td>:</td>
					<td><b>-</b><br></td>
				</tr>
			</tbody>
		</table>
	@endif
	<p class="fsize-14">Demikian laporan ini disampaikan sebagai bahan proses selanjutnya, terima kasih.</p>

	<br>
	<table class="table fsize-14 panitia-pnt">
		<tbody>
			@php $num = 1 @endphp
			@foreach($paket->panitia->last()->anggota as $panitia)
			<tr>
				<td class="text-align-center">{{ $num }}. </td>
				<td width="200">{{strtoupper($panitia->peg_nama)}}</td>
				<td @if($num%2 == 0) class="ttd_right" @endif>{{ $num }}. ..............................</td>
			</tr>
			@php $num++ @endphp
			@endforeach
		</tbody>
	</table>
	{{-- <div class="ttd-wrapper text-align-center">
		<div class="ttd fsize-14">
			<div class="ttd-tgl uppercase">Kelompok Kerja (Pokja) {{$paket->panitia->last()->pnt_nama}}</div>
			<div class="ttd-by">UKPBJ PROVINSI SUMATERA BARAT <br>Ketua</div>
			<div class="ttd-space"></div>
			<div class="ttd-nama bold uppercase">{{$ketua_pokja->peg_nama}}</div>
			<div class="nip">NIP. {{$ketua_pokja->peg_nip}}</div>
		</div>
	</div> --}}
	<br><br>
	<div class="tembusan">
		<div>
			<small>
				<div>Tembusan disampaikan kepada Yth :</div>
				<p>1. Kepala UKPBJ Provinsi Sumatera Barat</p>
			</small>
		</div>
	</div>
</body>
</html>