<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Cetak | Info Paket</title>
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
  <link href="//stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.10/css/AdminLTE.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.10/css/skins/_all-skins.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.10/css/skins/skin-blue-light.css" rel="stylesheet">
  <link href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('/css/sibaja.css?'.time()) }}">
  <style>
    .content-header h1 { margin: 0; }
    #datatable-rekanan tbody tr:hover { cursor: pointer; }
    #paket-pemenang-add input, #paket-penawar-add input { text-transform: uppercase; }
    #table-tab-info-paket tbody td:nth-child(1) { font-weight: 700; }
  </style>
</head>
<body>
<div class="page-template">
  <div class="content">
    <div class="box box-primary">
      <div class="box-body">
        <section>
          <div class="row">
            <div class="col-xs-12">
              <div class="flex align-item-center">
                <div class="qrcode">
                  {!! QrCode::size(180)->generate($result->lls_id) !!}
                </div>
                <div class="paket-info">
                  <div style="margin-bottom: 5px;">
                    <span class="label label-info">KODE LELANG {{$result->lls_id}}</span>
                    <span class="label label-warning">{{$result->spse_versi}}</span>
                    <span class="label label-danger">{{$result->kategori_lelang['nama']}}</span>
                    <span class="label label-primary">{{$result->metode_pemilihan['nama']}}</span>
                  </div>
                  <div class="paket_nama uppercase" style="margin-bottom: 5px; line-height: 2">
                    {{$result->paket->pkt_nama}}
                  </div>
                  <div class="uppercase"><i class="fa fa-bank space-right"></i><b>{{$result->paket->satker->instansi->nama}}</b></div>
                  <div class="uppercase" style="color: #00a65a;"> <i class="fa fa-building-o space-right"></i> {{$result->paket->satker->stk_nama}}</div>
                  <div><i class="fa fa-diamond space-right"></i>{{$result->paket->sbd_id}} {{$result->paket->ang_tahun}}</div>
                  <div><i class="fa fa-money space-right"></i>PAGU : <b>{{$result->rp_pagu}}</b> HPS : <b>{{$result->rp_hps}}</b></div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <section>
      <div class="box box-success">
        <div class="box-header uppercase">
          <div class="pull-left">Informasi Paket Pengadaan</div>
          <div class="pull-right">
            @if (!$result->paket->surat)
              <span class="label label-warning">Paket belum masuk ke ULP</span>
            @else
              <span class="label label-primary">Paket sudah masuk ke ULP</span>
            @endif
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped" id="table-tab-info-paket">
                <tbody>
                  <tr>
                    <td>RUP ID</td>
                    <td class="text-align-right">
                      {{$result->paket->rup_id}}
                    </td>
                  </tr>
                  <tr>
                    <td>ID Paket</td>
                    <td class="text-align-right">
                      {{$result->pkt_id}}
                    </td>
                  </tr>
                  <tr>
                    <td>Nama KPA / PPK</td>
                    <td class="text-align-right">
                      @if ($result->paket->ppk)
                        {{$result->paket->ppk->peg_nama}} <span class="label label-info">{{$result->paket->ppk->peg_nip}}</span>
                      @else
                        -
                      @endif
                    </td>
                  </tr>
                  <tr>
                    <td>Tanggal Paket Dibuat</td>
                    <td class="text-align-right">
                      {{\App\Helpers\Tanggal::dmy($result->paket->pkt_tgl_buat)}}
                    </td>
                  </tr>
                  <tr>
                    <td>Metode Kualifikasi</td>
                    <td class="text-align-right">
                      @if ($result->metode)
                        {{$result->metode['metode_kualifikasi']}}
                        {{$result->metode['metode_dokumen']}}
                      @else
                        -
                      @endif
                    </td>
                  </tr>
                  <tr>
                    <td>Progres</td>
                    <td class="text-align-right">
                      {{$result->tahap_now}}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="box box-success">
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Dokumen</th>
                    <th class="text-align-center">
                      @if ($result->paket->surat)
                        <span class="label label-warning">Ada ?</span>
                      @endif
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; ?>
                  @if (isset($result->dokumen))
                    @foreach($result->dokumen as $dok)
                      <tr>
                        <td>{{$no++}}</td>
                        <td>
                          {{$dok->nama}}
                        </td>
                        @if (Auth::user()->id_level != 2)
                          <td align="center">
                            <label class="switch">
                              <input name="is_ada[{{$dok->id}}]" type="checkbox" @if($dok->ada) checked="" @endif>
                              <span class="slider round"></span>
                            </label>
                          </td>
                        @else
                          <td align="center">
                            @if($dok->ada)
                              <span class="badge label-success"><i class="fa fa-check"></i></span>
                            @else
                              <span class="badge label-danger"><i class="fa fa-close"></i></span>
                            @endif
                          </td>
                        @endif
                      </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
</body>
</html>