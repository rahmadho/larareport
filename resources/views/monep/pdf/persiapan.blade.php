<html>
<head>
	<meta charset="UTF-8">
	<title>RAPAT PERSIAPAN | {{$persiapan->paket->panitia->pnt_nama}}</title>
	{{-- <link href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> --}}
	<link rel="stylesheet" href="{{asset('/css/print.css')}}">
	<style>
		body { padding: 0px 35px 0px 35px; }
		.catatan { margin-top: 45px; }
		ol { padding-left: 20px; }
		.fsize-14, .fsize-14 * { font-size: 16px; }
		table.absen { width: 100%; border-collapse: collapse; border: 1px solid; }
		table.absen th { border: 1px solid; vertical-align: top; padding: 4px 8px; }
		table.absen td { line-height: 3; border-left: 1px solid; border-right: 1px solid; }
	</style>
</head>
<body>
	@php
		$spt = $persiapan->paket->spt->last();
	@endphp
	<div class="heading">
		<div class="text-align-center">
			<div class="title-1 bold">BERITA ACARA</div>
			<div class="title-1 bold">REVIU DOKUMEN PERSIAPAN PENGADAAN</div>
			<div class="title-1 bold">Nomor : 02/ST-{{$spt->no_spt}}/PP.{{$persiapan->paket->panitia->pnt_nama}}/{{ Carbon\Carbon::parse($persiapan->tgl_rapat)->formatLocalized('%Y') }}</div>
		</div>
	</div>

	<table class="table text-align-justify fsize-14">
		<tbody>
			<tr>
				<td width="100">Pekerjaan</td>
				<td width="10">:</td>
				<td>{{$persiapan->paket->pkt_nama}}</td>
			</tr>
			<tr>
				<td>Pagu Anggaran</td>
				<td>:</td>
				<td>Rp. {{number_format($persiapan->paket->pkt_pagu, 2, ',', '.')}}</td>
			</tr>
			<tr>
				<td>OPD</td>
				<td>:</td>
				<td>{{$persiapan->paket->satker->stk_nama}}</td>
			</tr>
			<tr>
				<td>Tahun Anggaran</td>
				<td>:</td>
				<td>{{ $persiapan->paket->ang_tahun }}</td>
			</tr>
		</tbody>
	</table>
	
	<p class="text-align-justify fsize-14">
		Pada hari ini <b>{{ \App\Helpers\Tanggal::hari($persiapan->tgl_rapat) }}</b>, 
		tanggal <b>{{ Carbon\Carbon::parse($persiapan->tgl_rapat)->formatLocalized('%d') }} 
		<i>({{\App\Helpers\Angka::terbilang(Carbon\Carbon::parse($persiapan->tgl_rapat)->formatLocalized('%d'))}})</i></b>
		Bulan <b>{{ Carbon\Carbon::parse($persiapan->tgl_rapat)->formatLocalized('%B') }}</b>
		Tahun <b class="capitalize">{{ \App\Helpers\Angka::terbilang(Carbon\Carbon::parse($persiapan->tgl_rapat)->formatLocalized('%Y')) }}</b>,
		kami Kelompok Kerja (Pokja) Pemilihan-{{$persiapan->paket->panitia->pnt_nama}} UKPBJ Provinsi Sumatera Barat 
		yang ditugaskan berdasarkan Surat Tugas Kepala UKPBJ <b>No.020/{{$spt->no_spt}}/BAP2BMD-UKPBJ/{{ Carbon\Carbon::parse($persiapan->tgl_rapat)->formatLocalized('%Y') }}</b> 
		tanggal {{\App\Helpers\Tanggal::dmy($spt->tgl_input)}} mengadakan reviu dokumen
		persiapan pengadaan tersebut diatas bersama Kuasa Pengguna Anggaran (KPA) / Pejabat Pembuat Komitmen (PPK)
		beserta tim teknis untuk pekerjaan tersebut diatas.
	</p>
	<p class="text-align-justify fsize-14">
		Reviu Dokumen Persiapan Pengadaan ini membahas tentang hal-hal yang kurang jelas bagi Pokja Pemilihan 
		dan kelengkapan dokumen persiapan yang disampaikan kepada UKPBJ yang menghasilkan saran-saran dari Pokja Pemilihan
		kepada Kuasa Pengguna Anggaran (KPA) / Pejabat Pembuat Komitmen (PPK) sebagai berikut:
	</p>
	<ol class="fsize-14">
		<li class="bold">Spesifikasi Teknis / KAK dan Gambar</li>
		<p class="text-align-justify">Memastikan bahwa spesifikasi teknis/KAK telah dituangkan secara lengkap.</p>
		<table class="table border">
			<thead>
				<tr>
					<th>Tanggapan Pokja Pemilihan</th>
					<th>Tanggapan KPA/PPK</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="height: 400px"></td>
					<td></td>
				</tr>
			</tbody>
		</table>
		
		<div class="page-break"></div>

		<li class="bold">Harga Perkiraan Sendiri (HPS)</li>
		<p class="text-align-justify">Memastikan bahwa nilai HPS telah cukup dan sesuai dengan spesifikasi teknis/KAK dan ruang lingkup pekerjaan.</p>
		<table class="table border">
			<thead>
				<tr>
					<th>Tanggapan Pokja Pemilihan</th>
					<th>Tanggapan KPA/PPK</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="height: 230px"></td>
					<td></td>
				</tr>
			</tbody>
		</table>
		<br>
		<li class="bold">Rancangan Kontrak</li>
		<p class="text-align-justify">Memastikan bahwa draft kontrak telah sesuai dengan ruang lingkup pekerjaan.</p>
		<table class="table border">
			<thead>
				<tr>
					<th>Tanggapan Pokja Pemilihan</th>
					<th>Tanggapan KPA/PPK</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="height: 190px"></td>
					<td></td>
				</tr>
			</tbody>
		</table>
		<br>
		<li class="bold">Dokumen Anggaran Belanja (DIPA/DPA atau RKA-KL/RKA-PD yang telah ditetapkan)</li>
		<p class="text-align-justify">Memastikan bahwa anggaran untuk pekerjaan yang akan dilaksanakan telah tersedia dan jumlahnya cukup.</p>
		<table class="table border">
			<thead>
				<tr>
					<th>Tanggapan Pokja Pemilihan</th>
					<th>Tanggapan KPA/PPK</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="height: 140px"></td>
					<td></td>
				</tr>
			</tbody>
		</table>

		<div class="page-break"></div>

		<li class="bold">ID Paket RUP</li>
		<p class="text-align-justify">Memastikan bahwa paket yang akan dilaksanakan telah terdaftar dan diumumkan dalam SIRUP.</p>
		<table class="table border">
			<thead>
				<tr>
					<th>Tanggapan Pokja Pemilihan</th>
					<th>Tanggapan KPA/PPK</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="height: 190px"></td>
					<td></td>
				</tr>
			</tbody>
		</table>
		<br>
		<li class="bold">Waktu Penggunaan Barang/Jasa</li>
		<p class="text-align-justify">Memastikan bahwa pelaksanaan Pengadaan Barang/Jasa dan Pelaksanaan Kontrak dapat selesai sesuai rencana penggunaan/pemanfaatan barang/jasa.</p>
		<table class="table border">
			<thead>
				<tr>
					<th>Tanggapan Pokja Pemilihan</th>
					<th>Tanggapan KPA/PPK</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<ul>
							<li style="margin-bottom: 25px;">Perkiraan tanggal akan dilaksanakan proses pengadaan:</li>
							<li style="margin-bottom: 25px;">Perkiraan tanggal selesai proses pengadaan:</li>
							<li style="margin-bottom: 35px;">Lain-lain:</li>
							<br><br><br>
						</ul>
					</td>
					<td></td>
				</tr>
			</tbody>
		</table>
		<br>
		<li class="bold">Analisis Pasar</li>
		<p class="text-align-justify">Mengetahui kemungkinan ketersediaan barang/jasa dan pelaku usaha dalam negeri yang mampu dan memenuhi persyaratan untuk melaksanakan pekerjaan.</p>
		<table class="table border">
			<thead>
				<tr>
					<th>Tanggapan Pokja Pemilihan</th>
					<th>Tanggapan KPA/PPK</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="height: 160px"></td>
					<td></td>
				</tr>
			</tbody>
		</table>

		<div class="page-break"></div>

		<li class="bold">Lain-lain</li>
		<p>Hal-hal lainnya.</p>
		<table class="table border">
			<thead>
				<tr>
					<th>Tanggapan Pokja Pemilihan</th>
					<th>Tanggapan KPA/PPK</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="height: 380px">Mohon dicantumkan alamat email KPA/PPK</td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</ol>

	<p class="fsize-14 text-align-justify">
		Proses pemilihan penyedia dilaksanakan oleh Pokja Pemilihan setelah perubahan / perbaikan 
		atas saran-saran tersebut diatas disampaikan / diupload kembali pada Aplikasi SPSE melalui akun PPK.
	</p>
	<p class="fsize-14 text-align-justify">
		Hasil pemilihan yang dilaksanakan oleh Pokja Pemilihan dapat didownload oleh KPA/PPK melalui akun PPK 
		pada aplikasi SPSE.
	</p>
	<p class="fsize-14 text-align-justify">Demikian Berita Acara ini dibuat untuk dipergunakan sebagaimana mestinya.</p>
	<p class="text-align-right fsize-14">Padang, tanggal tersebut diatas</p>

	<div class="page-break"></div>

	<table class="table fsize-14 width-100">
		<thead>
			<tr style=line-height: 4">
				<th class="width-50">KPA / PPK / PPTK / Tim Teknis</th>
				<th class="width-50 text-align-center">Pokja Pemilihan</th>
			</tr>
		</thead>
		<tbody>
			@for ($i = 1; $i <= 5; $i++)
			<tr style="line-height: 3">
				<td><span class="inline-block" style="width: 15px">{{$i}}</span>...............................   ....................</td>
				<td><span class="inline-block" style="width: 15px">{{$i}}</span>...............................   ....................</td>
			</tr>	
			@endfor
		</tbody>
	</table>

	<div class="page-break"></div>

	<div class="no-surat text-align-center">
		<div class="fsize-14 bold">DAFTAR HADIR</div>
		<div class="fsize-14 bold">REVIU DOKUMEN PERSIAPAN PENGADAAN</div>
	</div>
	<br>
	<table class="table text-align-justify fsize-14">
		<tbody>
			<tr>
				<td width="100">Hari / Tanggal</td>
				<td width="10">:</td>
				<td>{{ Carbon\Carbon::parse($persiapan->tgl_rapat)->formatLocalized('%A / %d %B %Y') }}</td>
			</tr>
			<tr>
				<td>Tempat </td>
				<td>:</td>
				<td>Ruang Rapat UKPBJ Provinsi Sumatera Barat</td>
			</tr>
		</tbody>
	</table>
	<br>
	<table class="table fsize-14 absen">
		<thead>
			<tr>
				<th width="15" class="text-align-center">No</th>
				<th>Nama</th>
				<th>Tanda Tangan</th>
			</tr>
		</thead>
		<tbody>
			@for ($i = 1; $i <= 14; $i++)
			<tr>
				<td class="text-align-center">{{ $i }}</td>
				<td></td>
				<td @if($i%2 == 0) class="text-align-right" @endif>{{ $i }}. ..............................</td>
			</tr>
			@endfor
		</tbody>
	</table>
</body>
</html>