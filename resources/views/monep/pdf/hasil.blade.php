<html>
<head>
	<meta charset="UTF-8">
	<title>HASIL | PEMENANG</title>
	{{-- <link href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> --}}
	<link rel="stylesheet" href="{{asset('/css/print.css')}}">
	<style>
		body { padding: 0px 35px 0px 35px; }
		.catatan { margin-top: 45px; }
		ol { padding-left: 20px; }
		.fsize-14, .fsize-14 * { font-size: 14px; }
		table.v-top th, table.v-top td{ vertical-align: top; }
		table.panitia-pnt td{ padding-bottom: 35px; }
		table.panitia-pnt td.ttd_right{ padding-left: 150px; }
	</style>
</head>
<body>
	<div class="heading">
		<div class="logo">
			<img src="{{asset('sumbar.png')}}" alt="logo">
		</div>
		<div class="text-align-center">
			<div class="title-1">PEMERINTAH PROVINSI SUMATERA BARAT</div>
			<div class="title-1">SEKRETARIAT DAERAH</div>
			<div class="title-2 bold">UNIT KERJA PENGADAAN BARANG/JASA</div>
			<div class="title-3">Jalan Jenderal Sudirman No 51 Padang 25112, Telepon (0751)31401 - 31402 - 34425</div>
			<div class="title-3">Fax (0751)34671, www.sumbarprov.go.id, e-mail: ulp_sumbar@sumbarprov.go.id</div>
		</div>
	</div>
	<hr>
	@php
		$metode = $paket->metode['metode_kualifikasi'];
		$tanggal1 = (isset($paket->persiapan->tgl_rapat)) ? $paket->persiapan->last()->tgl_rapat : date('Y-m-d');
		$no_surat = "15/ST-{$paket->spt->last()->no_spt}/Pokja-{$paket->panitia->pnt_nama}/".Carbon\Carbon::parse($tanggal1)->formatLocalized('%Y');
		$pemenang = $paket->pemenang;
		//$ketua_pokja = $paket->panitia->last()->anggota->where('pivot.jabatan', 'Ketua')->first();
	@endphp
	<table class="table text-align-justify fsize-14 v-top">
		<tbody>
			<tr>
				<td width="52">Nomor</td>
				<td width="5">:</td>
				<td width="170">{{$no_surat}}</td>
				<td class="text-align-right">Padang, {{\App\Helpers\Tanggal::dmy(date('Y-m-d'))}}</td>
			</tr>
			<tr>
				<td>Lampiran</td>
				<td>:</td>
				<td>1 (satu) berkas</td>
				<td></td>
			</tr>
			<tr>
				<td>Perihal</td>
				<td>:</td>
				<td>Penyampaian Pemenang <br>Metode Tender, {{$metode}}</td>
				<td>Kepada Yth : <br>Kuasa Pengguna Anggaran <br>{{$paket->pkt_nama}} <br><br><br>di <br><div style="margin-left:18px">Padang</div></td>
			</tr>
		</tbody>
	</table>
	<br><br><br>
	<p class="text-align-justify fsize-14">
		Berdasarkan berita acara hasil pelelangan nomor: {{$no_surat}} tanggal {{\App\Helpers\Tanggal::dmy($paket->spt->last()->tgl_input)}},
		{{$paket->pkt_nama}} terlampir. telah ditetapkan pemenang e-{{$paket->metode_pemilihan['nama']}}, {{$metode}} sebagaimana tercantum dibawah ini :
	</p>
	<p><strong><u>Pemenang Tender {{$metode}}</u></strong></p>
	<table class="table text-align-justify fsize-14 v-top">
		<tbody>
			<tr>
				<td width="145">Nama Perusahaan</td>
				<td width="10">:</td>
				<td class="uppercase"><b>{{$pemenang->rekanan->rkn_nama}}</b></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td>{{$pemenang->rekanan->rkn_alamat}}</td>
			</tr>
			<tr>
				<td>No. NPWP</td>
				<td>:</td>
				<td class="uppercase">{{$pemenang->rekanan->rkn_npwp}}</td>
			</tr>
			<tr>
				<td>Harga Penawaran Terkoreksi</td>
				<td>:</td>
				<td>
					<b>
						@if ($pemenang->nev_harga_negosiasi > 0)
							@php $harga = $pemenang->nev_harga_negosiasi; @endphp
						@elseif ($pemenang->nev_harga_terkoreksi > 0)
							@php $harga = $pemenang->nev_harga_terkoreksi; @endphp
						@elseif ($pemenang->nev_harga > 0)
							@php $harga = $pemenang->nev_harga; @endphp
						@elseif ($pemenang->psr_harga_terkoreksi > 0)
							@php $harga = $pemenang->psr_harga_terkoreksi; @endphp
						@else 
							@php $harga = $pemenang->psr_harga; @endphp
						@endif
						Rp. {{ number_format($harga, 0) }},-
					</b><br>
					<b style="text-transform:capitalize">( {{\App\Helpers\Angka::terbilang($harga)}} ) termasuk PPN 10%.</b>
				</td>
			</tr>
		</tbody>
	</table>
	<p class="text-align-justify fsize-14">
		Demikian disampaikan dan selanjutnya agar diproses sesuai ketentuan yang berlaku.
	</p>
	<br><br>
	<div class="">Kelompok Kerja (Pokja) {{$paket->panitia->pnt_nama}}</div>
	<br>
	<table class="table fsize-14 panitia-pnt">
		<tbody>
			@php $num = 1 @endphp
			@foreach($paket->panitia->pegawai as $panitia)
			<tr>
				<td class="text-align-center">{{ $num }}. </td>
				<td width="200">{{strtoupper($panitia->peg_nama)}}</td>
				<td @if($num%2 == 0) class="ttd_right" @endif>{{ $num }}. ..............................</td>
			</tr>
			@php $num++ @endphp
			@endforeach
		</tbody>
	</table>
	{{-- <div class="ttd-wrapper text-align-center">
		<div class="ttd fsize-14">
			<div class="ttd-tgl">Kelompok Kerja (Pokja) {{$paket->panitia->last()->pnt_nama}}</div>
			<div class="ttd-by">LAYANAN PENGADAAN PROVINSI SUMATERA BARAT <br>Ketua</div>
			<div class="ttd-space"></div>
			<div class="ttd-nama bold">{{$ketua_pokja->peg_nama}}</div>
			<div class="nip">NIP. {{$ketua_pokja->peg_nip}}</div>
		</div>
	</div> --}}
</body>
</html>