@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.css')}}">
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1>User Sistem</h1>
  </div>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header clearfix">
            <a href="{{ route('monep.user.create') }}" class="btn btn-primary no-radius">
              <i class="fa fa-plus space-right"></i>
              Tambah
            </a>
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table class="table dataTable display responsive no-wrap" width="100%">
                <thead class="silver">
                  <th>No</th>
                  <th>Nama</th>
                  <th>Username</th>
                  <th>Level</th>
                  <th></th>
                </thead>
                <tbody>
                  <?php $no = 1; ?>
                  @foreach($users as $user)
                  <tr>
                    <td>{{$no++}}</td>
                    <td>
                      <a href="{{route('monep.user.edit', $user->id)}}">{{$user->nama}}</a>
                    </td>
                    <td>{{$user->username}}</td>
                    <td>
                      <span class="label label-success">
                        {{$user->level->nama}}
                      </span>
                    </td>
                    <td>
                      <form action="{{route('monep.user.destroy')}}" method="post">
                        @method('delete')
                        @csrf
                        <input type="hidden" name="id" value="$user->id">
                        <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('/lib/datatables/datatables.min.js')}}"></script>
<script src="{{asset('/lib/datatables/1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.js')}}"></script>
<script>
  $(document).ready( function () {
    $('.dataTable').DataTable({
      responsive: true
    });
  } );
</script>
@endsection