@extends('layout')
@section('css')
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1 class="uppercase">@if($user) {{$user->nama}} @endif</h1>
  </div>
  <div class="content">
    <div class="box box-primary">
      <div class="box-header clearfix">
        <div class="pull-left uppercase"></div>
        <div class="pull-right">
        </div>
      </div>
      <div class="box-body">
        
      </div>
    </div>
  </div>
</div>
@endsection

@section('template')
@endsection
@section('js')
<script>
  $(document).ready( function () {
    $('.style-select2').select2();
  });
</script>
@endsection