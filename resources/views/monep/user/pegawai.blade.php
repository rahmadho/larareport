@extends('layout')
@section('css')
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1>Pegawai</h1>
  </div>
  <div class="content">
    <form class="" method="post" action="{{ route('panitia.pegawai') }}">
      @csrf
      @method('POST')
      <div class="box box-primary paket-template">
        <h3 class="box-header">TAMBAH PEGAWAI</h3>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('nip')) has-error @endif">
                <label>NIP</label>
                <input name="nip" type="text" class="form-control form-control-sm" value="{{old('nip')}}" />
                @if($errors->first('nip'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('nama')) has-error @endif">
                <label>Nama</label>
                <input name="nama" type="text" class="form-control form-control-sm" value="{{old('nama')}}" />
                @if($errors->first('nama'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('golongan')) has-error @endif"">
                <label>Golongan</label>
                <input name="golongan" type="text" class="form-control form-control-sm" value="{{old('golongan')}}" />
                @if($errors->first('golongan'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('pangkat')) has-error @endif"">
                <label>Pangkat</label>
                <input name="pangkat" type="text" class="form-control form-control-sm" value="{{old('pangkat')}}" />
                @if($errors->first('pangkat'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('jabatan')) has-error @endif"">
                <label>Jabatan</label>
                <input name="jabatan" type="text" class="form-control form-control-sm" value="{{old('jabatan')}}" />
                @if($errors->first('jabatan'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
      <a href="{{route('panitia.index')}}" class="btn btn-warning no-radius">
        <i class="fa fa-arrow-left space-right"></i> KEMBALI
      </a>
      <button type="submit" class="btn btn-primary no-radius">
        SIMPAN <i class="fa fa-arrow-right space-left"></i>
      </button>
    </form>
  </div>
</div>
@endsection
@section('template')
@endsection
@section('js')
@endsection