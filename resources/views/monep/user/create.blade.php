@extends('layout')
@section('css')
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1>User</h1>
  </div>
  <div class="content">
    <form class="paketmasuk" method="post" action="{{ route('user.store') }}">
      @csrf
      @method('POST')
      <div class="box box-primary paket-template">
        <div class="box-header">TAMBAH USER</div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('nama')) has-error @endif">
                <label>Nama</label>
                <input name="nama" type="text" class="form-control form-control-sm" value="{{old('nama')}}" />
                @if($errors->first('nama'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('username')) has-error @endif">
                <label>Username</label>
                <input name="username" type="text" class="form-control form-control-sm" value="{{old('username')}}" />
                @if($errors->first('username'))
                <span class="help-block">{{$errors->first('username')}}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('level')) has-error @endif"">
                <label>Level</label>
                <select name="level" class="form-control form-control-sm">
                  <option value="1">Administrator</option>
                  <option value="2">Layanan</option>
                  <option value="3">Kepala UKPBJ</option>
                </select>
                @if($errors->first('level'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('password')) has-error @endif"">
                <label>Password</label>
                <input name="password" type="password" class="form-control form-control-sm" value="{{old('password')}}" />
                @if($errors->first('password'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
      <a href="{{route('user.index')}}" class="btn btn-warning no-radius">
        <i class="fa fa-arrow-left space-right"></i> KEMBALI
      </a>
      <button type="submit" class="btn btn-primary no-radius">
        SIMPAN <i class="fa fa-arrow-right space-left"></i>
      </button>
    </form>
  </div>
</div>
@endsection
@section('template')
@endsection
@section('js')
@endsection