@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datepicker/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/timepicker/css/bootstrap-timepicker.min.css')}}">
<style>
  .content-header h1 { margin: 0; }
  #datatable-rekanan tbody tr:hover { cursor: pointer; }
  #paket-pemenang-add input, #paket-penawar-add input { text-transform: uppercase; }
  #table-tab-info-paket tbody td:nth-child(1) { font-weight: 700; }
</style>
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <div class="row">
      <div class="col-md-12">
        <div style="margin-bottom: 8px;">
          @if ($result->lls_status == 1)
            <span class="label label-info"><i class="fa fa-bell"></i> Paket Sudah Tayang </span>
          @elseif ($result->lls_status == 0)
            <span class="label label-warning"><i class="fa fa-bell-slash"></i> Paket Belum Tayang</span>
          @elseif ($result->lls_status == 2)
            <span class="label label-danger"><i class="fa fa-bell-slash"></i> Tender Gagal</span>
          @endif
        </div>
        <div class="paket_nama uppercase" style="margin-bottom: 5px; line-height: 2">
            {{$result->paket->pkt_nama}}
          <b></b> 
        </div>
        <div>
          <span class="label label-success">{{$result->kategori_lelang['nama']}}</span>
          <span class="label label-primary">{{$result->metode_pemilihan['nama']}}</span>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="box">
      <div class="box-body">
        <div class="pull-left">
          <a href="{{route('monep.paket.history', $result->pkt_id)}}" class="btn btn-info"><i class="fa fa-history space-right"></i> History Paket</a>
          <a href="{{route('monep.print.info', $result->lls_id)}}" class="btn btn-primary" target="_blank"><i class="fa fa-print space-right"></i> Cetak Paket</a>
        </div>
        <div class="pull-right">
          @if ($result->lls_status == 1)
            <div style="margin-top: 5px; color: #646464;">
              <small class="uppercase"><i class="fa fa-calendar"></i> Paket tayang pada {{\App\Helpers\Tanggal::dmy($result->lls_tgl_setuju)}}</small>
            </div>
          @endif
        </div>
      </div>
    </div>
    <div class="box box-primary">
      <div class="box-body">
        <section>
          <div class="row">
            <div class="col-xs-12">
              <div class="flex align-item-center">
                <div class="qrcode">
                  {!! QrCode::size(180)->generate($result->lls_id) !!}
                </div>
                <div class="paket-info">
                  <div style="margin-bottom: 5px;">
                    <span class="label label-success">{{$result->lls_id}}</span>
                    <span class="label label-warning">{{$result->spse_versi}}</span>
                    @if ($result->ulang)
                      <span class="label label-warning">{{$result->ulang}}</span>
                    @endif
                    @if ($result->eva_versi > 1)
                      <span class="label label-danger">Evaluasi Ulang</span>
                    @endif
                  </div>
                  <div class="uppercase"><i class="fa fa-bank space-right"></i><b>{{$result->paket->satker->instansi->nama}}</b></div>
                  <div class="uppercase" style="color: #00a65a;"> <i class="fa fa-building-o space-right"></i> {{$result->paket->satker->stk_nama}}</div>
                  <div><i class="fa fa-diamond space-right"></i>{{$result->paket->sbd_id}} {{$result->paket->ang_tahun}}</div>
                  <div><i class="fa fa-money space-right"></i>PAGU : <b>{{$result->rp_pagu}}</b> HPS : <b>{{$result->rp_hps}}</b></div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab0" data-toggle="tab">PAKET</a></li>
        <li><a href="#tab1" data-toggle="tab">DOKUMEN</a></li>
        <li><a href="#tab2" data-toggle="tab">SURAT TUGAS</a></li>
        <li><a href="#tab3" data-toggle="tab">RAPAT PERSIAPAN</a></li>
        <li><a href="#tab4" data-toggle="tab">JADWAL</a></li>
        <li><a href="#tab5" data-toggle="tab">PENAWAR</a></li>
        <li><a href="#tab6" data-toggle="tab">HASIL EVALUASI</a></li>
        <li><a href="#tab7" data-toggle="tab">PEMENANG</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab0">
          <div class="box box-success">
            <div class="box-header uppercase">
              <div class="pull-left">Informasi Paket Pengadaan</div>
              <div class="pull-right">
                @if (!$result->paket->surat)
                  <span class="label label-warning">Paket belum masuk ke ULP</span>
                @else
                  <span class="label label-primary">Paket sudah masuk ke ULP</span>
                @endif
                @if ($result->dokumen->where('ada', false)->isNotEmpty())
                  <span class="label label-danger">Dokumen lelang belum lengkap</span>
                @endif
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-12 table-responsive">
                  <table class="table table-striped" id="table-tab-info-paket">
                    <tbody>
                      <tr>
                        <td>RUP ID</td>
                        <td class="text-align-right">
                          {{$result->paket->rup_id}}
                        </td>
                      </tr>
                      <tr>
                        <td>ID Paket</td>
                        <td class="text-align-right">
                          {{$result->pkt_id}}
                        </td>
                      </tr>
                      <tr>
                        <td>Nama KPA / PPK</td>
                        <td class="text-align-right">
                          @if ($result->paket->ppk)
                            {{$result->paket->ppk->peg_nama}} <span class="label label-info">{{$result->paket->ppk->peg_nip}}</span>
                          @else
                            -
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Tanggal Paket Dibuat</td>
                        <td class="text-align-right">
                          {{\App\Helpers\Tanggal::dmy($result->paket->pkt_tgl_buat)}}
                        </td>
                      </tr>
                      <tr>
                        <td>Tanggal Penetapan Pemenang</td>
                        <td class="text-align-right">
                          @if ($result->tgl_penetapan_pemenang)
                            {{\App\Helpers\Tanggal::dmy($result->tgl_penetapan_pemenang->pst_tgl_setuju)}}
                          @else
                            -
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Tanggal Selesai Lelang</td>
                        <td class="text-align-right">
                          @if ($result->selesai)
                            {{\App\Helpers\Tanggal::dmy($result->selesai->dtj_tglakhir)}}
                          @else
                            -
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Metode Kualifikasi</td>
                        <td class="text-align-right">
                          @if ($result->metode)
                            {{$result->metode['metode_kualifikasi']}}
                            {{$result->metode['metode_dokumen']}}
                          @else
                            -
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Surat Tugas</td>
                        <td class="text-align-right">
                          @if ($result->paket->spt->isNotEmpty())
                            {{$result->paket->spt->last()->panitia->pnt_nama}}
                            / <b>{{$result->paket->spt->last()->no_spt}}</b> 
                            / {{\App\Helpers\Tanggal::dmy($result->paket->spt->last()->tgl_input)}}
                          @else
                            -
                          @endif
                        </td>
                      </tr>
                      <tr>
                        <td>Progres</td>
                        <td class="text-align-right">
                          {{$result->tahap_now}}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  {{-- @if (Auth::user()->id_level != 4) --}}
                  <div class="box-footer text-align-right">
                    @if($result->paket->panitia)
                      <a href="{{route('monep.print.spt', $result->pkt_id)}}" class="btn btn-info btn-sm no-radius uppercase" target="_blank">
                        <i class="fa fa-file-pdf-o space-right"></i>{{$result->paket->panitia->pnt_nama}}
                      </a>
                    @endif
                    @if (\App\Helpers\Access::route('monep.spt.store') AND !$result->paket->surat)
                      <button class="btn btn-primary btn-sm no-radius" data-toggle="modal" data-target="#create-paket-modal">
                        <i class="fa fa-mouse-pointer space-right"></i> INPUT SURAT
                      </button>
                    @endif
                    @if (\App\Helpers\Access::route('monep.spt.store') AND $result->paket->surat AND $result->dokumen->where('ada', false)->isEmpty())
                      <button class="btn btn-primary btn-sm no-radius" data-toggle="modal" data-target="#create-paket-spt">
                        <i class="fa fa-mouse-pointer space-right"></i> BUAT SPT
                      </button>
                    @endif
                  </div>
                  {{-- @endif --}}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab1">
          <form action="{{route('monep.dokumen.store')}}" method="post" id="form_verify_document">
            @csrf
            @method('PUT')
            <input type="hidden" name="lls_id" value="{{$result->lls_id}}">
            <input type="hidden" name="pkt_id" value="{{$result->pkt_id}}">
            <input type="hidden" name="kgr_id" value="{{$result->paket->kgr_id}}">
            <div class="box box-success">
              <div class="box-header uppercase">Dokumen Pengadaan</div>
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Dokumen</th>
                          <th class="text-align-center">
                            @if (Auth::user()->id_level == 2 || Auth::user()->id_level == 1 && $result->paket->surat)
                              <button class="btn btn-primary btn-sm no-radius" id="btn-verify-document" type="submit">
                                <i class="fa fa-check space-right"></i> SIMPAN
                              </button>
                            @else
                              <span class="label label-warning">Paket belum masuk ke ULP</span>
                            @endif
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        @if (isset($result->dokumen))
                          @foreach($result->dokumen as $dok)
                            <tr>
                              <td>{{$no++}}</td>
                              <td>
                                {{$dok->nama}}
                              </td>
                              @if (Auth::user()->id_level != 2)
                                <td align="center">
                                  <label class="switch">
                                    <input name="is_ada[{{$dok->id}}]" type="checkbox" @if($dok->ada) checked="" @endif>
                                    <span class="slider round"></span>
                                  </label>
                                </td>
                              @else
                                <td align="center">
                                  @if($dok->ada)
                                    <span class="badge label-success"><i class="fa fa-check"></i></span>
                                  @else
                                    <span class="badge label-danger"><i class="fa fa-close"></i></span>
                                  @endif
                                </td>
                              @endif
                            </tr>
                          @endforeach
                        @endif
                        @if ($result->paket->surat)
                          <tr>
                            <td colspan="2"></td>
                            <td class="text-align-center">
                              <a class="btn btn-info btn-sm no-radius" href="{{route('monep.print.dokumen', $result->pkt_id)}}" target="_blank">
                                <i class="fa fa-print space-right"></i> PRINTING
                              </a>
                            </td>
                          </tr>                              
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="tab-pane" id="tab2">
          <div class="box box-info">
            <div class="box-header clearfix">
              <div class="pull-left">
                SURAT PERINTAH TUGAS 
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Anggota</th>
                        <th>Golongan</th>
                        <th class="text-align-center">Jabatan</th>
                      </tr>
                    </thead>
                    @if($result->paket->panitia)
                    <tbody>
                      <?php $no = 1; ?>
                      @foreach($result->paket->panitia->pegawai as $peg)
                      <tr>
                        <td>{{$no++}}</td>
                        <td>
                          {{$peg->peg_nama}}
                        </td>
                        <td class="uppercase">
                          @if($peg->peg_pangkat) {{$peg->peg_pangkat}} @else - @endif
                          / 
                          @if($peg->peg_golongan) {{$peg->peg_golongan}} @else - @endif
                        </td>
                        <td>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                    @else 
                    <tbody>
                      <tr>
                        <td class="text-align-center" colspan="4">Panitia belum dipilih</td>
                      </tr>
                    </tbody>
                    @endif
                  </table>
                </div>
              </div>
            </div>
            <div class="box-footer clearfix">
              <div class="pull-left">
                @if($result->paket->spt->isEmpty())
                  <span class="label label-warning no-radius">Nomor SPT belum di input</span>
                @else
                  Dibuat tanggal <span class="label label-success no-radius">{{\App\Helpers\Tanggal::dmy($result->paket->spt->last()->tgl_input)}}</span>
                @endif
              </div>
              <div class="pull-right">
                @if ($result->paket->panitia)
                <a href="{{route('monep.print.spt', $result->pkt_id)}}" class="btn btn-info btn-sm no-radius uppercase" target="_blank">
                  <i class="fa fa-file-pdf-o space-right"></i>{{$result->paket->panitia->pnt_nama}}
                </a>
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab3">
          <div class="box box-warning">
            <div class="box-header clearfix">
              <div class="pull-left">RAPAT PERSIAPAN</div>
              <div class="pull-right">
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th width="100">Cetak</th>
                        <th>Agenda</th>
                        <th width="200">Tanggal</th>
                        <th width="100">Jam</th>
                      </tr>
                    </thead>
                    @if($result->paket->persiapan->isNotEmpty())
                    <tbody>
                      <?php $no = 1; ?>
                      @foreach($result->paket->persiapan as $rapat)
                      <tr>
                        <td>
                          <a href="{{route('monep.print.persiapan', $rapat->id)}}" class="btn btn-success btn-sm no-radius" target="_blank">
                            <i class="fa fa-file-pdf-o"></i>
                          </a>
                        </td>
                        <td>
                          {{$rapat->agenda}}
                        </td>
                        <td>
                          {{\Carbon\Carbon::parse($rapat->tgl_rapat)->format('d M Y')}}
                        </td>
                        <td>
                          {{\Carbon\Carbon::parse($rapat->tgl_rapat)->format('H:i:s')}}
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                    @else 
                    <tbody>
                      <tr>
                        <td class="text-align-center" colspan="4">Tidak ada jadwal</td>
                      </tr>
                    </tbody>
                    @endif
                  </table>
                </div>
              </div>
            </div>
            <div class="box-footer text-align-right">
              @if (\App\Helpers\Access::route('monep.persiapan.store') AND $result->paket->spt->isNotEmpty() AND $result->paket->spt->last()->panitia)
              <button class="btn btn-primary btn-sm no-radius" data-toggle="modal" data-target="#create-paket-persiapan">
                <i class="fa fa-calendar space-right"></i> RAPAT PERSIAPAN
              </button>
              @endif
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab4">
          <div class="box box-warning">
            <div class="box-header clearfix">
              <div class="pull-left">JADWAL</div>
              <div class="pull-right">
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Tahap</th>
                    <th>Mulai</th>
                    <th>Selesai</th>
                  </tr>
                </thead>
                <tbody>
                  @if ($result->aktivitas)
                    @foreach ($result->aktivitas as $jadwal)
                      <tr>
                        @if (Carbon\Carbon::now()->lessThan(Carbon\Carbon::parse($jadwal->pivot->dtj_tglawal)))
                        <td><i style="color:tomato" class="fa fa-calendar-minus-o space-right"></i> {{$jadwal->keterangan}}</td>
                        @else
                        <td><i style="color:yellowgreen" class="fa fa-calendar-check-o space-right"></i> {{$jadwal->keterangan}}</td>
                        @endif
                        <td>{{\App\Helpers\Tanggal::dmy($jadwal->pivot->dtj_tglawal)}}</td>
                        <td>{{\App\Helpers\Tanggal::dmy($jadwal->pivot->dtj_tglakhir)}}</td>
                      </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab5">
          <div class="box-header uppercase">PESERTA</div>
          <table class="table table-striped">
            <thead>
              <th>Rekanan</th>
              <th>Harga</th>
            </thead>
            <tbody>
              @if (isset($peserta))
                @foreach ($peserta as $psr)
                  <tr>
                    <td class="uppercase">
                      <div><span class="label label-info">{{ $psr->rkn_npwp }}</span></div>
                      {{ $psr->rkn_nama }}
                    </td>
                    <td>Rp. {{ number_format($psr->psr_harga, 2) }}</td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
        <div class="tab-pane" id="tab6">
          <div class="box box-warning">
            <div class="box-header clearfix">
            </div>
            <div class="box-body">
              <table class="table table-striped">
                <thead>
                  <th></th>
                  <th>Rekanan</th>
                  <th>Harga</th>
                </thead>
                <tbody>
                  @if (isset($hasil_evaluasi))
                    @foreach ($hasil_evaluasi as $eva)
                      <tr>
                        <td>
                          <div>
                            @if($eva->pemenang)
                              <i class="fa fa-star-half-empty space-right" style="color: green"></i>
                            @endif
                            @if($eva->pemenang_verif)
                              <i class="fa fa-star space-right" style="color: orange"></i>
                            @endif
                          </div>
                        </td>
                        <td class="uppercase">
                          <div>{{ $eva->rkn_nama }}</div>
                          <div>{{ $eva->rkn_npwp }}</div>
                        </td>
                        <td>
                          @if ($eva->nev_harga_terkoreksi)
                            Rp. {{ number_format($eva->nev_harga_terkoreksi, 2) }}
                          @else
                            Rp. {{ number_format($eva->nev_harga, 2) }}
                          @endif
                        </td>
                      </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab7">
          <div class="box box-warning">
            <div class="box-header clearfix">
              @if($result->pemenang)
                @if (sizeOf($result->paket->spt) == 0)
                  <span class="label label-warning">Tidak ada SPT yang dibuat!</span>
                @else
                  <a href="{{route('monep.print.hasil', $result->pkt_id)}}" target="_blank" class="btn btn-info"><i class="fa fa-file-pdf-o space-right"></i>Laporan Hasil</a>
                  <a href="{{route('monep.print.hasil', $result->pkt_id)}}" target="_blank" class="btn btn-info"><i class="fa fa-file-pdf-o space-right"></i>Surat KPA</a>
                @endif
              @endif
            </div>
            <div class="box-body">
              <table class="table table-striped">
                <thead>
                  <th>Rank</th>
                  <th>Rekanan</th>
                  <th>Harga</th>
                </thead>
                <tbody>
                  @if($result->pemenang)
                    <tr>
                      <td><span class="label @if($result->pemenang->nev_urutan == 1) label-success @else label-warning @endif">{{ $result->pemenang->nev_urutan }}</span></td>
                      <td class="uppercase">{{ $result->pemenang->rekanan->rkn_nama }}</td>
                      <td>
                        @if ($result->pemenang->nev_harga_negosiasi > 0)
                          Rp. {{ number_format($result->pemenang->nev_harga_negosiasi, 2) }}
                        @elseif ($result->pemenang->nev_harga_terkoreksi > 0)
                          Rp. {{ number_format($result->pemenang->nev_harga_terkoreksi, 2) }}
                        @elseif ($result->pemenang->nev_harga > 0)
                          Rp. {{ number_format($result->pemenang->nev_harga, 2) }}
                        @elseif ($result->pemenang->psr_harga_terkoreksi > 0)
                          Rp. {{ number_format($result->pemenang->psr_harga_terkoreksi, 2) }}
                        @else 
                          Rp. {{ number_format($result->pemenang->psr_harga, 2) }}
                        @endif
                      </td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('template')
<div class="modal-template">
  @if ($result->paket->surat)
    @if (\App\Helpers\Access::route('monep.spt.store'))
    <form action="{{route('monep.spt.store')}}" method="post">
      @method('POST')
      @csrf
      <div class="modal fade" id="create-paket-spt">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <input type="hidden" name="pkt_id" value="{{$result->pkt_id}}">
              <input type="hidden" name="lls_id" value="{{$result->lls_id}}">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title uppercase">Nomor Surat - <b>@if($result->paket->surat){{$result->paket->surat->no_surat}}@endif</b></h4>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group @if($errors->first('tgl_input')) has-error @endif">
                      <label>Tanggal Pilih</label>
                      <input name="tgl_input" type="text" class="form-control form-control-sm" value="{{date('Y-m-d')}}" data-date-format="yyyy-mm-dd" data-provide="datepicker-inline" autocomplete="off" />
                      @if($errors->first('tgl_input'))
                      <span class="help-block">Isian tidak boleh kosong!</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group @if($errors->first('no_spt')) has-error @endif"">
                      <label>Nomor SPT</label>
                      <input name="no_spt" type="text" class="form-control form-control-sm" value="{{old('no_spt')}}" />
                      @if($errors->first('no_spt'))
                      <span class="help-block">Isian tidak boleh kosong!</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group @if($errors->first('pnt_id')) has-error @endif"">
                      <label>Panitia</label>
                      <select name="pnt_id" class="form-control form-control-sm uppercase">
                        <option value=""></option>
                        @foreach ($panitia as $pnt)
                          <option @if ($result->paket->pnt_id == $pnt->pnt_id) selected @endif value="{{$pnt->pnt_id}}">{{$pnt->pnt_nama}}</option>
                        @endforeach
                      </select>
                      @if($errors->first('pnt_id'))
                      <span class="help-block">Isian tidak boleh kosong!</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-warning no-radius" data-dismiss="modal">
                  <i class="fa fa-arrow-left space-right"></i> Batal
                </button>
                <button type="submit" class="btn btn-primary no-radius">
                  Simpan <i class="fa fa-arrow-right space-left"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    @endif
    @if (\App\Helpers\Access::route('monep.persiapan.store'))
    <form action="{{route('monep.persiapan.store')}}" method="post">
      @method('POST')
      @csrf
      <div class="modal fade" id="create-paket-persiapan">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <input type="hidden" name="pkt_id" value="{{$result->pkt_id}}">
              <input type="hidden" name="lls_id" value="{{$result->lls_id}}">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title uppercase">Rapat Persiapan</h4>
              </div>
              <div class="modal-body">
                <div class="form-group @if($errors->first('tgl_surat_persiapan')) has-error @endif">
                  <label for="Tanggal Surat">Tanggal Surat</label>
                  <input name="tgl_surat_persiapan" value="{{old('tgl_surat_persiapan')}}" data-date-format="yyyy-mm-dd" data-provide="datepicker-inline" type="text" class="form-control date-pick" readonly="" autocomplete="off" />
                  @if($errors->first('tgl_surat_persiapan'))
                    <span class="help-block">Isian tidak boleh kosong!</span>
                  @endif
                </div>
                <div class="form-group @if($errors->first('agenda_persiapan')) has-error @endif">
                  <label for="Nomor">Agenda</label>
                  <textarea name="agenda_persiapan" class="form-control">{{old('agenda_persiapan')}}</textarea>
                  @if($errors->first('agenda_persiapan'))
                    <span class="help-block">Isian tidak boleh kosong!</span>
                  @endif
                </div>
                <div class="form-group @if($errors->first('tgl_rapat_persiapan')) has-error @endif">
                  <label for="Tanggal Rapat">Tanggal Rapat</label>
                  <input name="tgl_rapat_persiapan" value="{{old('tgl_rapat_persiapan')}}" data-date-format="yyyy-mm-dd" data-provide="datepicker-inline" type="text" class="form-control date-pick" readonly="" autocomplete="off" />
                  @if($errors->first('tgl_rapat_persiapan'))
                    <span class="help-block">Isian tidak boleh kosong!</span>
                  @endif
                </div>
                <div class="form-group @if($errors->first('time_rapat_persiapan')) has-error @endif">
                  <label for="Tanggal Rapat">Jam Rapat</label>
                  <input name="time_rapat_persiapan" value="{{old('time_rapat_persiapan')}}" type="text" class="form-control timepicker" autocomplete="off" />
                  @if($errors->first('time_rapat_persiapan'))
                    <span class="help-block">Isian tidak boleh kosong!</span>
                  @endif
                </div>
                <div class="form-group @if($errors->first('tgl_input_persiapan')) has-error @endif">
                  <label for="Tanggal Rapat">Tanggal Input</label>
                  <input name="tgl_input_persiapan" value="{{old('tgl_inputt_persiapan')}}" data-date-format="yyyy-mm-dd" data-provide="datepicker-inline" type="text" class="form-control date-pick" readonly="" autocomplete="off" />
                  @if($errors->first('tgl_input_persiapan'))
                    <span class="help-block">Isian tidak boleh kosong!</span>
                  @endif
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-warning no-radius" data-dismiss="modal">
                  <i class="fa fa-arrow-left space-right"></i> Batal
                </button>
                <button type="submit" class="btn btn-primary no-radius">
                  Simpan <i class="fa fa-arrow-right space-left"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    @endif
  @else
    @if (\App\Helpers\Access::route('monep.surat.store'))
    <form class="paketmasuk" method="post" action="{{route('monep.surat.store')}}" id="create-paket-form">
      @csrf
      @method('POST')
      <div class="modal fade" id="create-paket-modal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <input type="hidden" name="pkt_id" value="{{$result->pkt_id}}">
              <input type="hidden" name="lls_id" value="{{$result->lls_id}}">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title uppercase">Buat Paket</h4>
            </div>
            <div class="modal-body">
              <div class="box box-primary paket-template">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group @if($errors->first('tgl_surat')) has-error @endif">
                        <label>Tanggal Surat Pengantar</label>
                        <input name="tgl_surat" type="text" class="form-control form-control-sm" value="{{date('Y-m-d', strtotime($result->paket->pkt_tgl_buat))}}" data-date-format="yyyy-mm-dd" data-provide="datepicker-inline" autocomplete="off" />
                        @if($errors->first('tgl_surat'))
                        <span class="help-block">Isian tidak boleh kosong!</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group @if($errors->first('tgl_input')) has-error @endif">
                        <label>Tanggal Masuk</label>
                        <input name="tgl_input" type="text" class="form-control form-control-sm" value="{{date('Y-m-d')}}" data-date-format="yyyy-mm-dd" data-provide="datepicker-inline" autocomplete="off" />
                        @if($errors->first('tgl_input'))
                        <span class="help-block">Isian tidak boleh kosong!</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group @if($errors->first('rup_id')) has-error @endif">
                        <label>Kode RUP</label>
                        <input name="rup_id" type="text" class="form-control form-control-sm" value="{{$result->paket->rup_id}}" />
                        @if($errors->first('rup_id'))
                        <span class="help-block">Isian tidak boleh kosong!</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group @if($errors->first('perihal')) has-error @endif">
                        <label>Perihal</label>
                        <input name="perihal" type="text" class="form-control form-control-sm" value="{{$result->paket->pkt_nama}}" />
                        @if($errors->first('perihal'))
                        <span class="help-block">Isian tidak boleh kosong!</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group @if($errors->first('no_surat')) has-error @endif"">
                        <label>Nomor Surat</label>
                        <input name="no_surat" type="text" class="form-control form-control-sm" value="{{old('no_surat')}}" />
                        @if($errors->first('no_surat'))
                        <span class="help-block">Isian tidak boleh kosong!</span>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary no-radius" id="btn-submit-paket">
                SIMPAN <i class="fa fa-arrow-right space-left"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
    @endif
  @endif
</div>
@endsection

@section('js')
<script src="{{asset('/lib/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('/lib/timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('.timepicker').timepicker({
    showSeconds: true,
    showMeridian: false,
    defaultTime: 'current',
    minuteStep: 5,
    secondStep: 5
  });
  $('#tab-menu a[href="#tab1"]').tab('show');
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  // $("#btn-verify-document").click(function (y) {
  //   console.log($("#form_verify_document").serialize());
  //   y.preventDefault();
  //   $.ajax({
  //     url: "{{route('ajax.put', 'verify_document')}}",
  //     type: "put",
  //     dataType: "application/json",
  //     data: $("#form_verify_document").serialize(),
  //     success: function(res) {
  //       console.log(res)

  //     }
  //   });
  // });
  @if($errors->first('create-paket-modal'))
    $("#create-paket-modal").modal('show');
  @elseif($errors->first('create-paket-spt'))
    $("#create-paket-spt").modal('show');
  @elseif($errors->first('create-paket-persiapan'))
    $("#create-paket-persiapan").modal('show');
  @endif
});
</script>
@endsection