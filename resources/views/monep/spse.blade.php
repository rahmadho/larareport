@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datepicker/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/timepicker/css/bootstrap-timepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.css')}}">
<style>
  .content-header h1 { margin: 0; }
  #datatable-paket tbody tr:hover { cursor: pointer; }
  #datatable-paket_filter { visibility: hidden; display: none; }
  .space-right { margin-right: 8px; }
  .info-box small {font-size:12px;}
</style>
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <div class="row">
      <div class="col-md-12">
        <h3>Paket SPSE</h3>
      </div>
    </div>
    <section>
      <div class="row">
        <div class="col-md-3">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check-circle"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">TENDER TAYANG</span>
              <span class="info-box-number">{{$result['count_all']}}</span>
              <div>
                <div class="progress">
                  <div class="progress-bar"></div>
                </div>
                <span class="progress-description">
                  <small>Paket tayang</small>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check-circle"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">PROSES TENDER</span>
              <span class="info-box-number">{{$result['count_jalan']}}</span>
              <div v-if="dataProgress">
                <div class="progress">
                  <div class="progress-bar"></div>
                </div>
                <span class="progress-description">
                  <small>Paket proses tender</small>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check-circle"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">TENDER SELESAI</span>
              <span class="info-box-number">{{$result['count_selesai']}}</span>
              <div v-if="dataProgress">
                <div class="progress">
                  <div class="progress-bar"></div>
                </div>
                <span class="progress-description">
                  <small>Paket selesai</small>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check-circle"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">TENDER GAGAL</span>
              <span class="info-box-number">{{$result['count_gagal_tayang']}}</span>
              <div v-if="dataProgress">
                <div class="progress">
                  <div class="progress-bar"></div>
                </div>
                <span class="progress-description">
                  <small>Paket gagal</small>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="row">
        <div class="col-md-4">
          @php $color = ['yellow','blue','red','aqua','green']; @endphp
          @foreach ($result['data']->sortBy('kgr_id')->groupBy('kgr_id')->toArray() as $kgr)
          <div class="progress-group">
            <span class="progress-text">{{App\Models\Epns\KategoriLelang::find($kgr[0]->kgr_id)['nama']}}</span> 
            <span class="progress-number"><b>{{count($kgr)}}</b><span>/{{count($result['data'])}}</span></span> 
            <div class="progress sm">
              <div class="progress-bar progress-bar-{{$color[$kgr[0]->kgr_id]}}" style="width: {{number_format(count($kgr)/count($result['data'])*100, 0)}}%;"></div>
            </div>
          </div>
          @endforeach
        </div>
        <div class="col-md-5">
          @foreach ($result['data']->sortBy('mtd_pemilihan')->groupBy('mtd_pemilihan')->toArray() as $mtdp)
          <div class="progress-group">
            <span class="progress-text">{{App\Models\Epns\MetodePemilihan::find($mtdp[0]->mtd_pemilihan)['nama']}}</span> 
            <span class="progress-number"><b>{{count($mtdp)}}</b><span>/{{count($result['data'])}}</span></span> 
            <div class="progress sm">
              <div class="progress-bar progress-bar-{{$color[rand(0,4)]}}" style="width: {{number_format(count($mtdp)/count($result['data'])*100, 0)}}%;"></div>
            </div>
          </div>
          @endforeach
        </div>
        <div class="col-md-3">
          <div class="row">
            <div class="col-md-12">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">TOTAL PAGU</span>
                  <span class="info-box-number" style="font-size:14px">{{number_format($result['data']->sum('pkt_pagu'), 2, ',', '.')}}</span>
                  <div>
                    <div class="progress">
                      <div class="progress-bar"></div>
                    </div>
                    <span class="progress-description">
                      <small>Total pagu dana</small>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">TOTAL HPS</span>
                  <span class="info-box-number" style="font-size:14px">{{number_format($result['data']->sum('pkt_hps'), 2, ',', '.')}}</span>
                  <div>
                    <div class="progress">
                      <div class="progress-bar"></div>
                    </div>
                    <span class="progress-description">
                      <small>Total hps lelang</small>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>  
  <div class="content">
    <div class="box box-primary">
      <div class="box-header">
        <div class="filterable">
          @if (Auth::user()->id_level != 4)
            <div action="" method="post" class="form-">
              @csrf
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Pencarian..." id="filterable">
                  </div>
                  <div class="col-md-3">
                    <select name="tahun" id="filter-tahun" class="form-control">
                      @php 
                        $yNow = date('Y');
                        $yMin = $yNow-5;  // 2008; 
                      @endphp
                      @for ($i = $yNow; $i >= $yMin; $i--)
                        <option value="{{$i}}">{{$i}}</option>
                      @endfor
                    </select>
                  </div>
                  <div class="col-md-3">
                    <select name="progres" id="filter-progres" class="form-control">
                      <option value="1">All Progres</option>
                      <option value="2">Paket Tayang</option>
                      <option value="3">Paket Selesai</option>
                      <option value="4">Paket Gagal</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-md-12">
                    <select name="satker" id="filter-satker" class="form-control">
                      <option value="0">SATUAN KERJA</option>
                      @foreach ($result['satker'] as $item)
                        <option value="{{$item->stk_id}}">{{$item->stk_nama}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
              </div>
            </div>
          @else
          <div action="" method="post" class="form-inline">
            @csrf
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Pencarian..." id="filterable">
            </div>
            <div class="form-group">
              <select name="tahun" id="filter-tahun" class="form-control">
                @for ($i = date('Y'); $i >= 2011; $i--)
                  <option value="{{$i}}">{{$i}}</option>
                @endfor
              </select>
            </div>
            <div class="form-group">
              <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
            </div>
          </div>
          @endif
        </div>
      </div>
      <div class="box-body">
        <section class="">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-striped" id="datatable-paket" style="width:100%">
                <thead>
                  <th style="max-width: 650px">Paket</th>
                  <th style="max-width: 200px">KPA/PPK</th>
                  <th>Pagu / HPS</th>
                </thead>
              </table>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="{{asset('/lib/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('/lib/timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script src="{{asset('/lib/datatables/datatables.min.js')}}"></script>
<script src="{{asset('/lib/datatables/1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#datatable-paket').DataTable({
      "searching": false,
      "ordering": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        "url": "{{ route('api.monep.table.paket') }}",
        "dataType": "json",
        "type": "get",
        "data": function(data){
          let filterTahun = $("#filter-tahun").val();
          let filterable = $("#filterable").val();
          @if (Auth::user()->id_level == 4)
            let pilihPanitia = $("#pilihPanitia").val();
            data.panitia = pilihPanitia;
          @else
            let in_ulp = $('input[name="in_ulp"]').prop('checked');
            let filterProgres = $("#filter-progres").val();
            let filterSatker = $("#filter-satker").val();
            let has_pokja = $('input[name="has_pokja"]').prop('checked');
            data.in_ulp = in_ulp;
            data.filter_progres = filterProgres;
            data.filter_satker = filterSatker;
            data.has_pnt = has_pokja;
          @endif
          data._token = "{{csrf_token()}}";
          data.tahun = filterTahun;
          data.filter = filterable;
          data.tayang = 1;
        }
      },
      "columns": [
        { 
          "data": "pkt_nama",
          "render": function(data, type, row) {
            let ulang = "";
            let tayang = "";
            let on_sibaja = "";
            let evaluasi_ulang = "";
            let pokja = "";
            if (row.ulang) {
              ulang = `<span class="label label-warning space-right">${row.ulang}</span>`
            }
            if (row.lelang.eva_versi > 1) {
              evaluasi_ulang = `<span class="label label-danger space-right">Evaluasi Ulang</span>`
            }
            if(row.lelang.lls_status == 0){
              tayang = `<i class="fa fa-star-o space-right" style="color: orange !important"></i>`
            } else if(row.lelang.lls_status == 1){
              tayang = `<i class="fa fa-star space-right" style="color: orange !important"></i>`
            } else if(row.lelang.lls_status == 2){
              tayang = `<i class="fa fa-star space-right" style="color: red !important"></i>`
            }
            if(row.on_sibaja === true){
              on_sibaja = `<i class="fa fa-check space-right" style="color: green !important"></i>`
            }
            if(row.pnt_nama) {
              pokja = `<span class="label label-primary space-right" style='text-transform: capitalize'>${row.pnt_nama}</span>`
            }
            return `
            <div>
              ${on_sibaja}
              ${tayang}
              <span class="label label-info space-right">${row.lelang.lls_id}</span>
              <span class="label label-success space-right">${row.spse_versi}</span>
              ${ulang}
              ${evaluasi_ulang}
              ${pokja}
            </div>
            <div><a><b>${data}</b></a></div>
            <div><small><i class="fa fa-building-o space-right"></i> ${row.satker.stk_nama}</small></div>
            <div><small><i class="fa fa-flag-o space-right"></i><b>${row.kategori_lelang.nama} - ${row.sbd_id} - ${row.ang_tahun}</b></small></div>
            `
          }
        },
        { 
          "data": "ppk",
          "render": function(data, type, row) {
            if(row.ppk != null){
              return `<div><span class="label label-success">${row.ppk.peg_nip}</span></div><div class="status_lelang"><small>${row.ppk.peg_nama}</small></div>`
            } else {
              return `<div class="status_lelang"><small>Tidak ada KPA / PPK </small></div>`
            }
          }
        },
        // { 
        //   "data": "status_lelang",
        //   "render": function(data, type, row) {
        //     if(data.id == 0){
        //       return `<a class="status_lelang"><small>${data.nama}</small></a>`
        //     } else if(data.id != 0 && row.tahap == 'Tender Sudah Selesai'){
        //       return `<a class="status_lelang"><small>Tender Sudah Selesai</small></a>`
        //     } else {
        //       return `<a class="status_lelang"><small>${row.tahap}</small></a>`
        //     }
        //   }
        // },
        { 
          "data": "pkt_pagu",
          "render": function(data, type, row) {
            return `<div>
            <div style='font-size: 8px;border-bottom: 1px solid #3c8dbc;font-weight: 700'>PAGU</div>
            <div class='pkt_pagu' style='margin-bottom: 8px;font-weight: 700'>Rp ${row.rp_pagu}</div>
            <div style='font-size: 8px;border-bottom: 1px solid #00a65a;font-weight: 700'>HPS</div>
            <div class='pkt_hps' style='font-size: 11px;'>Rp ${row.rp_hps}</div>
            </div>
            `
          }
        }
      ]
    });
    table.on( 'click', 'tr', function ( e, dt, type, indexes ) {
      var pilih = table.row(this).data();
      window.open('{{url('/')}}/monep/lelang/'+pilih.lelang.lls_id+'/info', '_blank');
    });
    $('#filterable').unbind();
    $('#filterable').bind('keyup', function(e) {
      if(e.keyCode == 13) {
        table.draw();
      }
    });

    $('#btn-filter').bind('click', function(e) {
      console.log('filter tahun '+$("#filter-tahun").val())
      table.draw();
    });
    $('input[name="has_pokja"]').bind('change', function(e) {
      table.draw();
    });
  });
</script>
@endsection