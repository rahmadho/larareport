@extends('layout')
@section('css')
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1 class="uppercase">Route</h1>
  </div>
  <div class="content">
    <form class="paketmasuk" method="post" action="{{ route('monep.menu.store') }}">
      @csrf
      @method('POST')
      <div class="box box-primary paket-template">
        <div class="box-header">
          Tambah Route
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group @if($errors->first('req_method')) has-error @endif">
                <label>Request Method</label>
                <select name="req_method" class="form-control form-control-sm">
                  <option @if (old('req_method') == 'get') selected @endif value="get">GET</option>
                  <option @if (old('req_method') == 'post') selected @endif value="post">POST</option>
                  <option @if (old('req_method') == 'put') selected @endif value="put">PUT</option>
                  <option @if (old('req_method') == 'delete') selected @endif value="delete">DELETE</option>
                </select>
                @if($errors->first('req_method'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
              <div class="form-group @if($errors->first('slug')) has-error @endif">
                <label>Slug</label>
                <input name="slug" type="text" class="form-control form-control-sm" value="{{old('slug')}}" />
                <span style="color:#777">Contoh : /menu/{id}/edit</span>
                @if($errors->first('slug'))
                <span class="help-block">{{$errors->first('slug')}}</span>
                @endif
              </div>
              <div class="form-group @if($errors->first('controller')) has-error @endif"">
                <label>Controller</label>
                <input name="controller" type="text" class="form-control form-control-sm" value="{{old('controller')}}" />
                @if($errors->first('controller'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
              <div class="form-group @if($errors->first('method')) has-error @endif"">
                <label>Method</label>
                <input name="method" type="text" class="form-control form-control-sm" value="{{old('method')}}" />
                @if($errors->first('method'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
              <div class="form-group @if($errors->first('route_name')) has-error @endif"">
                <label>Nama Route</label>
                <input name="route_name" type="text" class="form-control form-control-sm" value="{{old('route_name')}}" />
                @if($errors->first('route_name'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
              <div class="form-group @if($errors->first('label')) has-error @endif"">
                <label>Label</label>
                <input name="label" type="text" class="form-control form-control-sm" value="{{old('label')}}" />
                @if($errors->first('label'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
              <div class="form-group @if($errors->first('icon')) has-error @endif"">
                <label>Icon</label>
                <input name="icon" type="text" class="form-control form-control-sm" value="{{old('icon')}}" />
                @if($errors->first('icon'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
              <div class="form-group @if($errors->first('urut')) has-error @endif"">
                <label>Urutan</label>
                <input name="urut" type="text" class="form-control form-control-sm" value="{{old('urut')}}" />
                @if($errors->first('urut'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group @if($errors->first('role')) has-error @endif">
                <label>Permission</label>
                @php
                  $old_role = (is_array(old('role'))) ? old('role') : [];
                @endphp
                <div>
                  <label class="switch">
                    <input name="role[]" type="checkbox" value="1" @if (in_array(1, $old_role)) checked @endif>
                    <span class="slider round"></span>
                  </label> Pelayanan
                </div>
                <div>
                  <label class="switch">
                    <input name="role[]" type="checkbox" value="2" @if (in_array(2, $old_role)) checked @endif>
                    <span class="slider round"></span>
                  </label> Administrator
                </div>
                <div>
                  <label class="switch">
                    <input name="role[]" type="checkbox" value="3" @if (in_array(3, $old_role)) checked @endif>
                    <span class="slider round"></span>
                  </label> Kepala PBJ
                </div>
                <div>
                  <label class="switch">
                    <input name="role[]" type="checkbox" value="4" @if (in_array(4, $old_role)) checked @endif>
                    <span class="slider round"></span>
                  </label> Pokja
                </div>

                @if($errors->first('role'))
                <span class="help-block">Isian tidak boleh kosong!</span>
                @endif
              </div>
              <div class="form-group @if($errors->first('is_menu')) has-error @endif">
                <label>Menu Utama</label>
                <div>
                  <label class="switch">
                    <input name="is_menu" type="checkbox">
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
              <div class="form-group @if($errors->first('aktif')) has-error @endif">
                <label>Aktif</label>
                <div>
                  <label class="switch">
                    <input name="aktif" type="checkbox" checked>
                    <span class="slider round"></span>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <button type="submit" class="btn btn-primary no-radius">
        SIMPAN <i class="fa fa-arrow-right space-left"></i>
      </button>
    </form>
  </div>
</div>
@endsection
@section('js')
@endsection