@extends('layout')
@section('css')
<link rel="stylesheet" href="{{asset('/lib/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.css')}}">
@endsection
@section('content')
<div class="page-template">
  <div class="content-header">
    <h1 class="uppercase">MENU</h1>
  </div>
  <div class="content">
    <div class="box box-primary">
      <div class="box-header clearfix">
        <div class="pull-left uppercase"></div>
        <div class="pull-right">
          <a href="{{ route('monep.menu.add') }}" class="btn btn-primary">
            <i class="fa fa-plus space-right"></i> Tambah
          </a>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-striped" id="datatable-menu">
          <thead>
            <tr>
              <th>No</th>
              <th>Menu</th>
              <th>Slug</th>
              <th>Method</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($result as $menu)
              <tr>
                <td>{{$no++}}</td>
                <td><i class="space-right {{$menu->icon}}"></i> {{$menu->label}}</td>
                <td>{{$menu->slug}}</td>
                <td>{{$menu->request_method}}</td>
                <td>
                  <form action="{{route('monep.menu.delete', $menu->id)}}" class="form-inline">
                    <a href="{{route('monep.menu.edit', $menu->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></button></form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('template')
@endsection
@section('js')
<script src="{{asset('/lib/datatables/datatables.min.js')}}"></script>
<script src="{{asset('/lib/datatables/1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('/lib/datatables/responsive/responsive.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('.style-select2').select2();
    $("#datatable-menu").dataTable({
      "aaSorting": []
    });
  });
</script>
@endsection