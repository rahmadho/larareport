<?php

namespace App\Observers;

use App\Models\Monep\PaketDokumen;
use App\Models\Monep\PaketPersiapan;
use App\Models\Monep\PaketSpt;
use App\Models\Monep\Surat;

class SuratObserver
{
    /**
     * Handle the surat "created" event.
     *
     * @param  \App\Models\Monep\Surat  $surat
     * @return void
     */
    public function created(Surat $surat)
    {
        //
    }

    /**
     * Handle the surat "updated" event.
     *
     * @param  \App\Models\Monep\Surat  $surat
     * @return void
     */
    public function updated(Surat $surat)
    {
        //
    }

    /**
     * Handle the surat "deleted" event.
     *
     * @param  \App\Models\Monep\Surat  $surat
     * @return void
     */
    public function deleted(Surat $surat)
    {
        PaketDokumen::where('pkt_id', $surat->pkt_id)->delete();
        PaketPersiapan::where('pkt_id', $surat->pkt_id)->delete();
        PaketSpt::where('pkt_id', $surat->pkt_id)->delete();
    }

    /**
     * Handle the surat "restored" event.
     *
     * @param  \App\Models\Monep\Surat  $surat
     * @return void
     */
    public function restored(Surat $surat)
    {
        //
    }

    /**
     * Handle the surat "force deleted" event.
     *
     * @param  \App\Models\Monep\Surat  $surat
     * @return void
     */
    public function forceDeleted(Surat $surat)
    {
        //
    }
}
