<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

use App\Models\Monep\Surat;
use App\Observers\SuratObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(app_path('Helpers/*.php')) as $filename) {
            require_once $filename;
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        setlocale(LC_ALL, 'id_ID.utf8');
        setlocale(LC_TIME, 'IND');
        config(['app.locale' => 'id']);
        Carbon::setLocale('id_ID.utf8');
        Carbon::setlocale(LC_TIME, 'IND');
        Surat::observe(SuratObserver::class);
    }
}
