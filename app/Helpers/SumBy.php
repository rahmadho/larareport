<?php
if (!function_exists('sum_by')){
  function sum_by(array $arr, $key) : float {
    if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key)) {
      trigger_error('sum_by(): The key should be a string, an integer, a float, or a function', E_USER_ERROR);
    }
    $sum = 0.00;
    foreach ($arr as $value) {
      if (is_object($value)) {
        if (is_numeric($value->{$key})) $sum += $value->{$key};
      } else {
        if (is_numeric($value[$key])) $sum += $value[$key];
      }
    }
    return $sum;
  }
}