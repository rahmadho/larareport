<?php
namespace App\Helpers;

use Carbon\Carbon;

class Tanggal
{
  public static function hari($tgl)
  {
    $dt = new Carbon($tgl);
    setlocale(LC_TIME, 'IND');
    return $dt->formatLocalized('%A');
  }
  public static function dmy($tgl)
  {
    $dt = new Carbon($tgl);
    setlocale(LC_TIME, 'IND');
    return $dt->formatLocalized('%d %B %Y');
  }
  public static function long($tgl)
  {
    $dt = new Carbon($tgl);
    setlocale(LC_TIME, 'IND');
    return $dt->formatLocalized('%d %B %Y %H:%M:%S');
  }
}
