<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use App\Models\Monep\RouteController;

class Access
{
  public static function route($route)
  {
    return RouteController::whereRaw(Auth::user()->id_level . ' = any(role)')->where('route', $route)->first();
  }
  public static function method($controller, $method)
  {
    return RouteController::whereRaw(Auth::user()->id_level . ' = any(role)')
      ->where('controller_name', $controller)
      ->where('controller_method', $method)
      ->first();
  }
}
