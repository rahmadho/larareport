<?php

if (!function_exists('toFloat')) {
  function toFloat($input)
  {
    return floatval(str_replace(',', '.', str_replace('.', '', $input)));
  }
}
