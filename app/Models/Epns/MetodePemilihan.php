<?php

namespace App\Models\Epns;

class MetodePemilihan
{
    public static function find($mtd_pemilihan, $tampil_nama = false)
    {
        $metode_pemilihan = collect([
            array("id" => 0,  "nama" => "Lelang Umum", "label" => "Pelelangan Umum"),
            array("id" => 1,  "nama" => "Lelang Sederhana", "label" => "Pelelangan Sederhana"),
            array("id" => 2,  "nama" => "Lelang Pemilihan Langsung", "label" => "Pemilihan Langsung"),
            array("id" => 3,  "nama" => "Seleksi Umum", "label" => "Seleksi Umum"),
            array("id" => 4,  "nama" => "Seleksi Sederhana", "label" => "Seleksi Sederhana"),
            array("id" => 5,  "nama" => "Seleksi Langsung", "label" => "Seleksi Langsung"),
            array("id" => 6,  "nama" => "Penunjukan Langsung", "label" => "Penunjukan Langsung"),
            array("id" => 7,  "nama" => "Swakelola", "label" => "Swakelola"),
            array("id" => 8,  "nama" => "Lelang Terbatas", "label" => "Pelelangan Terbatas"),
            array("id" => 9,  "nama" => "Tender Cepat", "label" => "Tender Cepat"),
            array("id" => 10, "nama" => "Seleksi Cepat", "label" => "E-Seleksi Cepat"),
            array("id" => 11, "nama" => "Pengadaan Langsung", "label" => "Pengadaan Langsung"),
            array("id" => 12, "nama" => "Penunjukan Langsung", "label" => "Penunjukan Langsung"),
            array("id" => 13, "nama" => "Kontes", "label" => "Kontes"),
            array("id" => 14, "nama" => "Sayembara", "label" => "Sayembara"),
            array("id" => 15, "nama" => "Tender", "label" => "Tender"),
            array("id" => 16, "nama" => "Seleksi", "label" => "Seleksi"),
            array("id" => 17, "nama" => "Pengecualian", "label" => "Pengecualian"),
            array("id" => 18, "nama" => "Darurat", "label" => "Darurat"),
            array("id" => 19, "nama" => "Tender Internasional", "label" => "Tender Internasional")
        ]);
        $mtd = $metode_pemilihan->where("id", "=", $mtd_pemilihan)->first();
        if ($tampil_nama) {
            return $mtd['nama'];
        } else {
            return $mtd;
        }
    }
}
