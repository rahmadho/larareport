<?php

namespace App\Models\Epns;

use App\Models\Monep\Dokumen;

class PaketDokumen
{
    public static function docs()
    {
        return collect([
            0 => array(1, 2, 3, 4, 7, 8, 9, 10),
            1 => array(1, 2, 3, 4, 7, 8),
            2 => array(1, 2, 3, 4, 5, 6, 7, 8),
            3 => array(1, 2, 3, 4, 7, 8, 9, 10)
        ]);
    }
    public static function get_key($kgr_id)
    {
        $filtered = self::docs()->filter(function ($v, $k) use ($kgr_id) {
            return ($k == $kgr_id);
        });
        return $filtered->first();
    }
    public static function find($kgr_id)
    {
        $dokumen = self::docs();
        $filtered = $dokumen->filter(function ($v, $k) use ($kgr_id) {
            return ($k == $kgr_id);
        });
        $filtered = $filtered->first();
        $all_dokumens = Dokumen::all();
        $dokumen = $all_dokumens->whereIn('id', $filtered);
        return $dokumen;
    }
}
