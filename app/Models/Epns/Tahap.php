<?php

namespace App\Models\Epns;

use App\Models\Monep\LelangSeleksi;
use Illuminate\Support\Facades\DB;

class Tahap
{
    public static function tahaps()
    {
        return collect([
            array("id" => 18776, "key" => "AMBIL_DOKUMEN_PEMILIHAN", "nama" => "Download Dokumen Pemilihan"),
            array("id" => 18777, "key" => "AMBIL_DOKUMEN", "nama" => "Download Dokumen Pemilihan"),
            array("id" => 18778, "key" => "AMBIL_DOK_PRA", "nama" => "Download Dokumen Kualifikasi"),
            array("id" => 18779, "key" => "EVALUASI_DOK_PRA", "nama" => "Evaluasi Dokumen Kualifikasi"),
            array("id" => 18781, "key" => "EVALUASI_PENAWARAN", "nama" => "Evaluasi Penawaran"),
            array("id" => 18782, "key" => "EVALUASI_PENAWARAN_ADM_TEKNIS", "nama" => "Evaluasi penawaran administrasi dan teknis"),
            array("id" => 18783, "key" => "EVALUASI_PENAWARAN_BIAYA", "nama" => "Evaluasi Penawaran Harga"),
            array("id" => 18784, "key" => "KLARIFIKASI_TEKNIS_BIAYA", "nama" => "Klarifikasi dan Negosiasi Teknis dan Biaya"),
            array("id" => 18785, "key" => "KLARIFIKASI_KEWAJARAN_HARGA", "nama" => "Klarifikasi Kewajaran harga"),
            array("id" => 18786, "key" => "KOREKSI_ARITMATIK", "nama" => "Koreksi Aritmatik dan Klarifikasi Kewajaran Harga"),
            array("id" => 18788, "key" => "SANGGAH_ADM_TEKNIS", "nama" => "Masa Sanggah atas Hasil Kelulusan Administrasi dan Teknis"),
            array("id" => 18789, "key" => "SANGGAH", "nama" => "Masa Sanggah Hasil Tender"),
            array("id" => 18791, "key" => "SANGGAH_PRA", "nama" => "Masa Sanggah Prakualifikasi"),
            array("id" => 18792, "key" => "PEMASUKAN_DAN_EVALUASI_DOK_PRA", "nama" => "Pemasukan dan Evaluasi Dokumen Kualifikasi"),
            array("id" => 18795, "key" => "PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS", "nama" => "Pembukaan dan Evaluasi Penawaran File I: Administrasi dan Teknis "),
            array("id" => 18796, "key" => "PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA", "nama" => "Pembukaan dan Evaluasi Penawaran File II: Harga"),
            array("id" => 18797, "key" => "PEMBUKAAN_PENAWARAN_BIAYA", "nama" => "Pembukaan dokumen penawaran file II : Penawaran Harga"),
            array("id" => 18798, "key" => "PEMBUKAAN_PENAWARAN", "nama" => "Pembukaan Dokumen Penawaran"),
            array("id" => 18802, "key" => "VERIFIKASI_KUALIFIKASI", "nama" => "Pembuktian Kualifikasi"),
            array("id" => 18803, "key" => "TANDATANGAN_KONTRAK", "nama" => "Penandatanganan Kontrak"),
            array("id" => 18804, "key" => "PENETAPAN_PEMENANG_AKHIR", "nama" => "Penetapan Pemenang"),
            array("id" => 18805, "key" => "PENETAPAN_PEMENANG_ADM_TEKNIS", "nama" => "Penetapan Peringkat Teknis"),
            array("id" => 18807, "key" => "PENGUMUMAN_LELANG", "nama" => "Pengumuman Pascakualifikasi"),
            array("id" => 18808, "key" => "UMUM_PRAKUALIFIKASI", "nama" => "Pengumuman Prakualifikasi"),
            array("id" => 18809, "key" => "PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK", "nama" => "Pengumuman Hasil Prakualifikasi dan Download Dokumen Pemilihan"),
            array("id" => 18793, "key" => "PENGUMUMAN_PEMENANG_ADM_TEKNIS", "nama" => "Pengumuman Hasil Evaluasi Administrasi dan Teknis"),
            array("id" => 18810, "key" => "PENGUMUMAN_PEMENANG_AKHIR", "nama" => "Pengumuman Pemenang"),
            array("id" => 18811, "key" => "PENGUMUMAN_HASIL_PRA", "nama" => "Pengumuman Hasil Prakualifikasi"),
            array("id" => 18812, "key" => "PENJELASAN", "nama" => "Pemberian Penjelasan"),
            array("id" => 18832, "key" => "PENJELASAN_PRA", "nama" => "Penjelasan Dokumen Prakualifikasi"),
            array("id" => 18814, "key" => "PENYETARAAN_TEKNIS", "nama" => "Penyetaraan Penawaran Teknis (apabila diperlukan)"),
            array("id" => 18817, "key" => "PENUNJUKAN_PEMENANG", "nama" => "Surat Penunjukan Penyedia Barang/Jasa"),
            array("id" => 18819, "key" => "UPLOAD_BA_EVALUASI_PENAWARAN", "nama" => "Upload Berita Acara Evaluasi Penawaran"),
            array("id" => 18820, "key" => "UPLOAD_BA_EVALUASI_HARGA", "nama" => "Upload Berita Acara Hasil Evaluasi Harga"),
            array("id" => 18821, "key" => "UPLOAD_BA_EVALUASI_ADM_TEKNIS", "nama" => "Upload Berita Acara Hasil Evaluasi Administrasi dan Teknis"),
            array("id" => 18822, "key" => "UPLOAD_BA_HASIL_LELANG", "nama" => "Upload Berita Acara Hasil Pemilihan"),
            array("id" => 18824, "key" => "PEMASUKAN_PENAWARAN_ADM_TEKNIS", "nama" => "Upload dokumen penawaran File I: Penawaran Administrasi dan Teknis"),
            array("id" => 18826, "key" => "PEMASUKAN_PENAWARAN_BIAYA", "nama" => "Upload Dokumen Penawaran File II: Penawaran Harga"),
            array("id" => 18827, "key" => "PEMASUKAN_DOK_PRA", "nama" => "Kirim Persyaratan Kualifikasi"),
            array("id" => 18828, "key" => "PENETAPAN_PEMENANG_PENAWARAN", "nama" => "Usulan Calon Pemenang"),
            array("id" => 18830, "key" => "PENETAPAN_HASIL_PRA", "nama" => "Penetapan Hasil Kualifikasi"),
            array("id" => 18833, "key" => "PEMASUKAN_PENAWARAN", "nama" => "Upload Dokumen Penawaran"),
            array("id" => 18839, "key" => "UPLOAD_BA_TAMBAHAN", "nama" => "Upload Berita Acara Tambahan"),
            array("id" => 18840, "key" => "KLARIFIKASI_NEGOSIASI", "nama" => "Klarifikasi Teknis dan Negosiasi"),
            array("id" => 18841, "key" => "TANDATANGAN_SPK", "nama" => "Penandatanganan Kontrak"),
            array("id" => 18842, "key" => "TANDATANGAN_SMPK", "nama" => "Penandatanganan SMPK"),
            array("id" => 18843, "key" => "EVALUASI_PENAWARAN_KUALIFIKASI", "nama" => "Evaluasi Administrasi, Kualifikasi, Teknis, dan Harga"),
            array("id" => 18844, "key" => "PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI", "nama" => "Pembukaan dan Evaluasi Penawaran File I: Administrasi, Dokumen Kualifikasi, dan Teknis"),
            array("id" => 18845, "key" => "PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR", "nama" => "Penetapan dan Pengumuman Pemenang"),
            array("id" => 0, "key" => "BELUM_DILAKSANAKAN", "nama" => "Tender Belum Dilaksanakan"),
            array("id" => 0, "key" => "TIDAK_ADA_JADWAL", "nama" => "Tidak Ada Jadwal"),
            array("id" => 0, "key" => "SUDAH_SELESAI", "nama" => "Tender Sudah Selesai"),
            array("id" => 0, "key" => "PAKET_SUDAH_SELESAI", "nama" => "Paket Sudah Selesai"),
            array("id" => 0, "key" => "PAKET_BELUM_DILAKSANAKAN", "nama" => "Paket Belum Dilaksanakan")
        ]);
    }

    public static function find($key)
    {
        $tahaps = explode(',', $key);
        $tahap = self::tahaps()->where("key", $tahaps[0])->first();
        if ($tahap) {
            return $tahap['nama'];
        } else {
            return null;
        }
    }

    public static function jadwal(array $jadwals)
    {
        $tahaps = self::tahaps();
        foreach ($jadwals as $key => $item) {
            $filter = $tahaps->where("key", $item->akt_jenis)->first();
            $jadwals[$key]->tahap = $filter['nama'];
        }
        return $jadwals;
    }

    public static function findId($thp_id)
    {
        $tahap = self::tahaps()->where("id", $thp_id)->first();
        if ($tahap) {
            return $tahap['nama'];
        } else {
            return null;
        }
    }

    public static function toString($lelang)
    {
        $paket = $lelang;
        $tahapanBA = array('UPLOAD_BA_EVALUASI_ADM_TEKNIS', 'UPLOAD_BA_EVALUASI_HARGA', 'UPLOAD_BA_EVALUASI_PENAWARAN', 'UPLOAD_BA_HASIL_LELANG');
        $tahaps = explode(',', $paket->tahaps);
        $tahaps_source = self::tahaps();
        if (sizeof($tahaps) == 1) {
            $label = $tahaps[0];
            if (($paket->mtd_pemilihan == 9 or $paket->mtd_pemilihan == 10) and $tahaps[0] == 'BELUM_DILAKSANAKAN') {
                $label = "TIDAK_ADA_JADWAL";
            }
            return self::find($label);
        } else {
            $label_info = [];
            foreach ($tahaps as $k => $v) {
                $label = $v;
                $tahap = self::find($label);
                if (in_array($v, $tahapanBA)) {
                    $label_info[$k] = str_replace('Upload', 'Dokumen', $tahap);
                } else {
                    $label_info[$k] = $tahap;
                }
            }
            return implode(',', $label_info);
        }
    }

    public static function findByLlsId($lls_id)
    {
        $q = LelangSeleksi::select(DB::raw("tahap_now(lls_id, localtimestamp) as tahap_now"))->where('lls_id', $lls_id)->first();
        return self::find($q->tahap_now);
    }
}
