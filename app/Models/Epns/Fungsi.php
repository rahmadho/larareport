<?php

namespace App\Models\Epns;

use App\Models\Monep\SatuanKerja;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class Fungsi
{
    // SIKAP_URL = Play.configuration.getProperty("sikap.url", "https://sikap.lkpp.go.id");
    // SIRUP_URL = Play.configuration.getProperty("sirup.url", "https://sirup.lkpp.go.id/sirup");
    // INAPROC_URL = Play.configuration.getProperty("inaproc.url", "http://inaproc.id");
    // DCE_URL = Play.configuration.getProperty("dce.url", "https://sirup.lkpp.go.id/sirup");
    // DEFAULT_CACHE = Play.configuration.getProperty("cache.expires", "1mn");
    public static function jadwal($lls_id)
    {
        $sql = "SELECT lls_id, j.akt_id, akt_urut, dtj_id, dtj_tglawal, dtj_tglakhir , akt_jenis, (SELECT count(dtj_id) FROM history_jadwal where dtj_id=j.dtj_id) jml_history FROM jadwal j, aktivitas a where j.akt_id=a.akt_id and j.lls_id=:lls_id order by akt_urut asc";
        $query  = DB::connection('psql')->select($sql, ['lls_id' => $lls_id]);
        $return = Tahap::jadwal($query);
        return $return;
    }
    // buat melihat persetujuan tayang terakhir (tidak dipakai, karena tgl tayang adalah lls_tgl_setuju di tabel lls_seleksi)
    public static function tayang($data)
    {
        $result = new stdClass;
        $result->sudah_tayang = false;
        $tayang = DB::connection('psql')->select("SELECT pst_tgl_setuju, (SELECT COUNT(*) FROM persetujuan WHERE pst_jenis=0 AND lls_id=p.lls_id) as semua,(SELECT COUNT(*) FROM persetujuan WHERE pst_jenis=0 AND pst_status = 1 AND lls_id=p.lls_id) as setuju FROM persetujuan p WHERE pst_jenis=0 AND lls_id=:lls_id ORDER BY pst_tgl_setuju DESC NULLS LAST LIMIT 1", ['lls_id' => $data->lls_id]);
        if ($tayang) {
            if ($tayang[0]->semua / $tayang[0]->setuju < 2) {
                $result->sudah_tayang = true;
                $result->tanggal = $tayang[0]->pst_tgl_setuju;
            }
        }
        return $result;
    }
    public static function peserta($lls_id)
    {
        // $peserta = null;
        // $query = DB::connection('pgsql')->select("SELECT * FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4)", ["lls_id" => $paket->lls_id]);
        // return $query;

        $query = DB::connection('pgsql')->select("SELECT p.psr_id,r.rkn_id, r.rkn_nama, r.rkn_npwp, psr_harga, psr_harga_terkoreksi, d.dok_id as fileHarga, k.nev_lulus AS kualifikasi, k.nev_skor AS skor, k.nev_uraian AS als_kualifikasi, a.nev_lulus AS administrasi, a.nev_uraian AS als_administrasi, t.nev_lulus AS teknis, t.nev_skor as skor_teknis, t.nev_uraian AS als_teknis, h.nev_lulus as harga, h.nev_skor as skor_harga,  h.nev_harga, h.nev_harga_terkoreksi, s.nev_lulus AS pemenang, p.is_pemenang_verif AS pemenang_verif,h.nev_uraian AS als_harga, q.nev_lulus as pembuktian, q.nev_skor as skor_pembuktian, q.nev_uraian as als_pembuktian, s.nev_skor AS skor_akhir, psr_alasan_menang, s.nev_harga_negosiasi FROM peserta p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id LEFT JOIN dok_penawaran d ON p.psr_id=d.psr_id AND d.dok_jenis in (2,3) LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 5 AND eva_versi=:versi ) q ON p.psr_id=q.psr_id LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 0 AND eva_versi=:versi ) k ON p.psr_id=k.psr_id LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 1 AND eva_versi=:versi ) a ON p.psr_id=a.psr_id LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 2 AND eva_versi=:versi ) t ON p.psr_id=t.psr_id LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 3 AND eva_versi=:versi ) h ON p.psr_id=h.psr_id LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 4 AND eva_versi=:versi ) s ON p.psr_id=s.psr_id WHERE p.lls_id=:lls_id order by psr_harga asc NULLS LAST;", ["lls_id" => $lls_id, "versi" => 4]);
        return $query;

        $query = DB::connection('pgsql')->table('peserta as p')
            ->select('r.rkn_nama', 'r.rkn_alamat', 'r.rkn_npwp', 'p.psr_harga')
            ->leftJoin('rekanan as r', 'r.rkn_id', '=', 'p.rkn_id')
            ->where('p.lls_id', $lls_id)
            ->get();
        return $query;
    }
    public static function panitia($pkt_id = null)
    {
        $query = DB::connection('pgsql')->table('paket_panitia as pp')
            ->select('p.pnt_id', 'p.pnt_nama', 'pp.auditupdate')
            ->leftJoin('panitia as p', 'p.pnt_id', '=', 'pp.pnt_id')
            ->orderBy('pp.auditupdate', 'desc');
        if ($pkt_id) {
            $result = $query->where('pp.pkt_id', $pkt_id)->first();
            if ($result) {
                $result->anggota = ($result) ? self::anggota_panitia($result->pnt_id) : [];
            } else {
                $result = null;
            }
            return $result;
        }
    }

    public static function anggota_panitia($pnt_id)
    {
        $query = DB::connection('pgsql')->table('anggota_panitia as ap')
            ->select('p.peg_nip', 'p.peg_nama', 'p.peg_golongan', 'p.peg_pangkat')
            ->leftJoin('pegawai as p', 'p.peg_id', '=', 'ap.peg_id')
            ->where('ap.pnt_id', $pnt_id)
            ->get();
        return $query;
    }

    public static function pemenang($paket)
    {
        $paket = DB::connection('pgsql')->table('paket as p')->select('lls_id', 'mtd_pemilihan')->leftJoin('lelang_seleksi as l', 'l.pkt_id', '=', 'p.pkt_id')->where('lls_id', $paket->lls_id)->first();
        $select = "nev_lulus,nev_urutan,nev_harga,nev_harga_terkoreksi,nev_harga_negosiasi,is_pemenang,is_pemenang_verif,psr.psr_id,psr_harga,psr_harga_terkoreksi,(SELECT rkn_nama FROM rekanan WHERE rkn_id=psr.rkn_id) as rkn_nama, (SELECT rkn_alamat FROM rekanan WHERE rkn_id=psr.rkn_id) as rkn_alamat, (SELECT rkn_npwp FROM rekanan WHERE rkn_id=psr.rkn_id) as rkn_npwp";
        $pemenang = null;
        if ($paket) {
            $mtd_pemilihan = $paket->mtd_pemilihan;
            if ($mtd_pemilihan == 9 || $mtd_pemilihan == 10) {
                $pemenang = DB::connection('pgsql')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang_verif =:is_pemenang", ["lls_id" => $paket->lls_id, "is_pemenang" => 1]);
            }
            if ($pemenang == null) {
                $pemenang = DB::connection('pgsql')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang =:is_pemenang", ["lls_id" => $paket->lls_id, "is_pemenang" => 1]);
            }
            if (sizeof($pemenang) > 1) {
                // klo perlu tambah and nev_urutan = 1
                if ($mtd_pemilihan == 9 || $mtd_pemilihan == 10) {
                    $pemenang = DB::connection('pgsql')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang_verif =:is_pemenang", ["lls_id" => $paket->lls_id, "is_pemenang" => 1]);
                } else {
                    $pemenang = DB::connection('pgsql')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang =:is_pemenang", ["lls_id" => $paket->lls_id, "is_pemenang" => 1]);
                }
            }
        }
        return ($pemenang) ? $pemenang[0] : $pemenang;
    }

    public static function hasil_evaluasi($lls_id)
    {
        $query = DB::connection('pgsql')->select("SELECT p.psr_id,r.rkn_id, r.rkn_nama, r.rkn_npwp, psr_harga, psr_harga_terkoreksi, d.dok_id as fileHarga, k.nev_lulus AS kualifikasi, k.nev_skor AS skor, k.nev_uraian AS als_kualifikasi, a.nev_lulus AS administrasi, a.nev_uraian AS als_administrasi, t.nev_lulus AS teknis, t.nev_skor as skor_teknis, t.nev_uraian AS als_teknis, h.nev_lulus as harga, h.nev_skor as skor_harga,  h.nev_harga, h.nev_harga_terkoreksi, s.nev_lulus AS pemenang, p.is_pemenang_verif AS pemenang_verif,h.nev_uraian AS als_harga, q.nev_lulus as pembuktian, q.nev_skor as skor_pembuktian, q.nev_uraian as als_pembuktian, s.nev_skor AS skor_akhir, psr_alasan_menang, s.nev_harga_negosiasi FROM peserta p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id LEFT JOIN dok_penawaran d ON p.psr_id=d.psr_id AND d.dok_jenis in (2,3) LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 5 AND eva_versi=(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id)) q ON p.psr_id=q.psr_id LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 0 AND eva_versi=(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id)) k ON p.psr_id=k.psr_id LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 1 AND eva_versi=(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id)) a ON p.psr_id=a.psr_id LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 2 AND eva_versi=(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id)) t ON p.psr_id=t.psr_id LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 3 AND eva_versi=(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id)) h ON p.psr_id=h.psr_id LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 4 AND eva_versi=(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id)) s ON p.psr_id=s.psr_id WHERE p.lls_id=:lls_id order by nev_harga asc", ['lls_id' => $lls_id]);
        return $query;
    }

    public static function penetapan_pemenang($lls_id)
    {
        // SELECT pst_tgl_setuju FROM persetujuan WHERE pst_jenis=1 AND pst_status=1 AND lls_id = 13353016 ORDER BY pst_tgl_setuju ASC LIMIT 1 OFFSET 2;
        $q = DB::connection('pgsql')->table('persetujuan')
            ->select('pst_tgl_setuju')
            ->where('pst_jenis', 1)->where('pst_status', 1)
            ->where('lls_id', $lls_id)
            ->limit(1)->offset(2)
            ->first();
        return $q;
    }

    public static function get_ppk($pkt_id)
    {
        // SELECT CONCAT(peg_nama, ' / ', peg_nip) FROM pegawai JOIN ppk ON pegawai.peg_id=ppk.peg_id JOIN paket_ppk pppk ON ppk.ppk_id = pppk.ppk_id WHERE pkt_id = 13353016;
        $q = DB::connection('pgsql')->table('pegawai as ta')
            ->select('peg_nama', 'peg_nip')
            ->join('ppk as tb', 'ta.peg_id', '=', 'tb.peg_id')
            ->join('paket_ppk as tc', 'tc.ppk_id', '=', 'tb.ppk_id')
            ->where('pkt_id', $pkt_id)
            ->first();
        return $q;
    }

    // get satker per tahun
    public static function get_satker($tahun = null)
    {
        if ($tahun == null) {
            $tahun = date('Y');
        }
        return SatuanKerja::select('stk_nama', 'stk_id')
            ->where('instansi_id', 'D462') // paket pemda sumatera barat
            ->whereRaw("string_to_array(rup_stk_tahun, ',')  && array['" . $tahun . "']")
            ->orderBy('stk_nama', 'asc')->get();
    }

    // MONEP
    public static function stat()
    {
        $tahun_now = date('Y');
        $query = DB::connection('monep')->table('paket as ta')
            ->leftJoin('lelang_seleksi as tb', 'ta.pkt_id', '=', 'tb.pkt_id')
            ->leftJoin('satuan_kerja as tc', 'tc.stk_id', '=', 'ta.stk_id')
            ->select('ta.pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_status', 'pkt_flag', 'kgr_id', 'rup_id', 'sbd_id', 'ang_tahun', 'ppk_nip', 'pnt_id', 'ta.stk_id', 'pkt_tgl_buat', 'mtd_id', 'mtd_pemilihan', 'pemenang', 'lls_id', 'tahap', 'lls_status');
        $query->addSelect(DB::raw("CASE WHEN (pemenang->>'nev_harga_negosiasi')::numeric > 0 THEN pemenang->>'nev_harga_negosiasi'
            WHEN (pemenang->>'nev_harga_terkoreksi')::numeric > 0 THEN pemenang->>'nev_harga_terkoreksi'
            WHEN (pemenang->>'nev_harga')::numeric > 0 THEN pemenang->>'nev_harga'
            WHEN (pemenang->>'psr_harga_terkoreksi')::numeric > 0 THEN pemenang->>'psr_harga_terkoreksi'
            ELSE pemenang->>'psr_harga' END AS harga"));
        $query->where('ang_tahun', $tahun_now)->where('lls_status', 1); //->where('instansi_id', 'D462');
        //return $get = $query->toSql();
        $get = $query->get();
        $query2 = DB::connection('monep')->table('paket as ta')
            ->leftJoin('lelang_seleksi as tb', 'ta.pkt_id', '=', 'tb.pkt_id')
            ->leftJoin('satuan_kerja as tc', 'tc.stk_id', '=', 'ta.stk_id')
            ->select('ta.pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_status', 'pkt_flag', 'kgr_id', 'rup_id', 'sbd_id', 'ang_tahun', 'ppk_nip', 'pnt_id', 'ta.stk_id', 'pkt_tgl_buat', 'mtd_id', 'mtd_pemilihan', 'pemenang', 'lls_id', 'tahap', 'lls_status');
        $query2->where('ang_tahun', $tahun_now)->where('lls_status', 2);


        $sum_pagu_all = $get->sum(function ($p) {
            return $p->pkt_pagu;
        });
        $paket_jalan = $get->filter(function ($v, $k) {
            //return ($v->tahap != 'SUDAH_SELESAI' && $v->tahap != 'PENUNJUKAN_PEMENANG');
            return ($v->tahap != 'SUDAH_SELESAI');
        });
        $paket_selesai = $get->filter(function ($v, $k) {
            //return ($v->tahap == 'SUDAH_SELESAI' || $v->tahap == 'PENUNJUKAN_PEMENANG');
            return ($v->tahap == 'SUDAH_SELESAI');
        });
        $paket_selesai_pemenang = $paket_selesai->filter(function ($v, $k) {
            return $v->pemenang != null;
        });
        $sum_harga_winner = $paket_selesai_pemenang->sum('harga');
        $sum_pagu_winner = $paket_selesai_pemenang->sum(function ($p) {
            return $p->pkt_pagu;
        });
        $satker = SatuanKerja::select('stk_nama', 'stk_id')->whereRaw("instansi_id='D462' AND string_to_array(rup_stk_tahun, ',')  && array['2019']")->orderBy('stk_nama', 'asc')->get();
        $result = [
            "count_all" => count($get),
            "count_jalan" => count($paket_jalan),
            "count_selesai" => count($paket_selesai_pemenang),
            "count_gagal_tayang" => count($paket_selesai->whereIn('pemenang', [null])),
            "count_gagal" => count($paket_selesai->whereIn('pemenang', [null])) + count($query2->get()),
            "sum_pagu_all" => number_format($sum_pagu_all, 2),
            "sum_pagu_pemenang" => number_format($sum_pagu_winner, 2),
            "sum_harga_pemenang" => number_format($sum_harga_winner, 2),
            "satker" => $satker,
            "data" => $get
        ];
        return $result;
    }
}
