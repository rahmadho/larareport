<?php

namespace App\Models\Epns;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MonepQuery extends Model
{
    public static function query_table_paket_monep()
    {
        $query = DB::connection('monep')->table('paket as ta');
        $query->select('ta.pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_flag', 'kgr_id', 'ta.rup_id', 'sbd_id', 'ang_tahun', 'pkt_status', 'pkt_tgl_buat', 'ta.stk_id', 'ta.pnt_id', 'ppk_nip',);
        $query->addSelect('stk_nama', 'pnt_nama', 'lls_id', 'mtd_id', 'mtd_pemilihan', 'lls_status', 'tahap', 'pemenang', 'lls_versi_lelang', 'lls_tgl_setuju', 'eva_versi');
        $query->leftJoin('lelang_seleksi as tb', 'ta.pkt_id', '=', 'tb.pkt_id')
            ->leftJoin('panitia as tc', 'ta.pnt_id', '=', 'tc.pnt_id')
            ->leftJoin('satuan_kerja as td', 'ta.stk_id', '=', 'td.stk_id');
        // select ppk
        $query->addSelect(DB::raw("(SELECT peg_nama FROM pegawai WHERE peg_id = ta.ppk_peg_id) as ppk_nama"));
        $query->addSelect(DB::raw("CASE WHEN (pemenang->>'nev_harga_negosiasi')::numeric > 0 THEN pemenang->>'nev_harga_negosiasi'
            WHEN (pemenang->>'nev_harga_terkoreksi')::numeric > 0 THEN pemenang->>'nev_harga_terkoreksi'
            WHEN (pemenang->>'nev_harga')::numeric > 0 THEN pemenang->>'nev_harga'
            WHEN (pemenang->>'psr_harga_terkoreksi')::numeric > 0 THEN pemenang->>'psr_harga_terkoreksi'
            ELSE pemenang->>'psr_harga' END AS pemenang_harga"));
        $query->addSelect(DB::raw("pemenang->>'rkn_id' as pemenang_rekanan"));
        $query->orderBy('lls_tgl_setuju', 'desc');

        // $query->whereRaw('lls_versi_lelang=(SELECT max(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=ta.pkt_id)');
        return $query;
    }
}
