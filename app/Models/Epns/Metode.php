<?php

namespace App\Models\Epns;

class Metode
{
    public static function find($mtd_id)
    {
        // mtd_id
        $metode = collect([
            array("id" => 1, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 19, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 2, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 4, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 3, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua Tahap"),
            array("id" => 41, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 51, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 42, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 46, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 47, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 44, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua Tahap"),
            array("id" => 48, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 50, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 52, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 53, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 54, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 55, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Dua File")
        ]);
        return $metode->where("id", $mtd_id)->first();
    }
}
