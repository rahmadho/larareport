<?php

namespace App\Models\Epns;

use App\Models\Monep\Rekanan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reporting extends Model
{
    public static function lelang($lls_status = 1)
    {
        $query = DB::connection('monep')->table('paket as ta');
        $query->select('ta.pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_flag', 'kgr_id', 'ta.rup_id', 'sbd_id', 'ang_tahun', 'pkt_status', 'pkt_tgl_buat', 'ta.stk_id', 'ta.pnt_id', 'ppk_nip',);
        $query->addSelect('stk_nama', 'pnt_nama', 'lls_id', 'mtd_id', 'mtd_pemilihan', 'lls_status', 'tahap', 'pemenang', 'lls_versi_lelang', 'lls_tgl_setuju', 'instansi_id', 'is_sppbj', 'is_kontrak');
        $query->addSelect(DB::raw("CASE WHEN tg.nama IS NULL THEN (SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct agc_nama), ', ') FROM agency WHERE agc_id = tf.agc_id) ELSE tg.nama END as instansi"));
        $query->addSelect(DB::raw("CASE WHEN (pemenang->>'nev_harga_negosiasi')::numeric > 0 THEN pemenang->>'nev_harga_negosiasi'
            WHEN (pemenang->>'nev_harga_terkoreksi')::numeric > 0 THEN pemenang->>'nev_harga_terkoreksi'
            WHEN (pemenang->>'nev_harga')::numeric > 0 THEN pemenang->>'nev_harga'
            WHEN (pemenang->>'psr_harga_terkoreksi')::numeric > 0 THEN pemenang->>'psr_harga_terkoreksi'
            ELSE pemenang->>'psr_harga' END AS pemenang_harga"));
        $query->addSelect(DB::raw("pemenang->>'rkn_id' as pemenang_rekanan"));
        $query->leftJoin('lelang_seleksi as tb', 'ta.pkt_id', '=', 'tb.pkt_id')
            ->leftJoin('panitia as td', 'ta.pnt_id', '=', 'td.pnt_id')
            ->leftJoin('satuan_kerja as tf', 'ta.stk_id', '=', 'tf.stk_id')
            ->leftJoin('instansi as tg', 'tf.instansi_id', '=', 'tg.id');
        $query->where('lls_status', $lls_status);
        $query->orderBy('ang_tahun', 'desc')->orderBy('lls_tgl_setuju', 'desc');
        return $query;
    }
    public static function rekanan()
    {
        // $rekanan = DB::connection('monep')->table('rekanan as ta')
        $query = DB::table('rekanan as ta')
            ->select('rkn_id', 'rkn_nama', 'rkn_alamat', 'rkn_npwp', 'rkn_status', 'ta.kbp_id', 'kbp_nama', 'tb.prp_id', 'prp_nama', 'ta.btu_id')
            // ->addSelect(DB::raw('(SELECT count(psr_id) FROM peserta WHERE rkn_id=ta.rkn_id) as jml_ikut_tender')) // data peserta belum di import
            ->leftJoin('kabupaten as tb', 'ta.kbp_id', '=', 'tb.kbp_id')
            ->leftJoin('propinsi as tc', 'tb.prp_id', '=', 'tc.prp_id')
            ->leftJoin('bentuk_usaha as td', 'ta.btu_id', '=', 'td.btu_id');
        // ->orderBy('jml_ikut_tender', 'desc');
        return $query;
    }
    public static function count_jumlah_rekanan($tahun = null)
    {
        $query = DB::table('peserta as ta')
            ->selectRaw("COUNT(rkn_id) as jumlah")
            ->join('lelang_seleksi as tb', 'ta.lls_id', '=', 'tb.lls_id')
            ->groupBy('rkn_id')->where('lls_status', 1)->whereRaw("date_part('year',ta.auditupdate) = 2019");
        return $query->toSql();
    }
    public static function count_jumlah_rekanan_by_tahun_daftar($tahun = null)
    {
        $query = Rekanan::whereRaw("date_part('year',rkn_tgl_daftar) = ?", [$tahun]);
        return $query->get()->count();
    }
}
