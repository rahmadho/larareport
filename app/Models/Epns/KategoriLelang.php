<?php

namespace App\Models\Epns;

class KategoriLelang
{
    public static function find($kgr_id, $tampilkan_nama = false)
    {
        $kategori_lelang = collect([
            array('id' => 0, 'nama' => 'Pengadaan Barang'),
            array('id' => 1, 'nama' => 'Jasa Konsultansi Badan Usaha'),
            array('id' => 2, 'nama' => 'Pekerjaan Konstruksi'),
            array('id' => 3, 'nama' => 'Jasa Lainnya'),
            array('id' => 4, 'nama' => 'Jasa Konsultansi Perorangan')
        ]);
        $kgr = $kategori_lelang->where("id", "=", $kgr_id)->first();
        if ($tampilkan_nama) {
            return $kgr['nama'];
        } else {
            return $kgr;
        }
    }
}
