<?php

namespace App\Models\Epns;

class StatusLelang
{
    public static function find($lls_status)
    {
        $status_lelang = collect([
            array('id' => 0, 'nama' => "Draft"),
            array('id' => 1, 'nama' => "Tender Sedang Berjalan"),
            array('id' => 2, 'nama' => "Tender Ditutup"),
            array('id' => 3, 'nama' => "Tender Sudah Selesai"),
            array('id' => 4, 'nama' => "Tender DiTolak"),
            array('id' => 5, 'nama' => "Draft UKPBJ"),
            array('id' => 6, 'nama' => "Draft Pokja")
        ]);
        return $status_lelang->where("id", "=", $lls_status)->first();
    }
}
