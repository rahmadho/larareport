<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    protected $connection = 'monep';
    protected $table = 'kabupaten';
    protected $primaryKey = 'kbp_id';
    public $timestamps = false;
    public $incrementing = false;

    public function propinsi()
    {
        return $this->belongsTo(Propinsi::class, 'prp_id', 'prp_id');
    }
}
