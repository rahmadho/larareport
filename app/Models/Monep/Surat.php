<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    protected $connection = 'monep';
    protected $table = 'surat';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'pkt_id', 'pkt_id')->with(['satker', 'lelang']);
    }
}
