<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class LelangSeleksi extends Model
{
    protected $connection = 'monep';
    protected $table = 'lelang_seleksi';
    protected $primaryKey = 'lls_id';
    public $timestamps = false;
    public $incrementing = false;

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'pkt_id', 'pkt_id');
    }
    public function jadwal()
    {
        return $this->hasMany(Jadwal::class, 'lls_id', 'lls_id');
    }
    public function aktivitas()
    {
        return $this->belongsToMany(Aktivitas::class, 'jadwal', 'lls_id', 'akt_id')->orderBy('akt_urut', 'asc')->withPivot('thp_id', 'dtj_tglawal', 'dtj_tglakhir');
    }
    public function persetujuan()
    {
        return $this->hasMany(Persetujuan::class, 'lls_id', 'lls_id');
    }
    public function tgl_penetapan_pemenang()
    {
        return $this->hasOne(Persetujuan::class, 'lls_id', 'lls_id')->where('pst_jenis', 1)->orderBy('pst_tgl_setuju', 'asc')->skip(2)->take(1);
    }
    public function tahap_sekarang()
    {
        return $this->hasOne(Jadwal::class, 'lls_id', 'lls_id')
            ->select('thp_id')
            ->whereRaw('dtj_tglawal <= localtimestamp')
            ->whereRaw('dtj_tglakhir >= localtimestamp');
    }

    public function scopeTayang($query)
    {
        return $query->where('lls_status', 1);
    }
    public function scopeGagal($query)
    {
        return $query->where('lls_status', 2);
    }
}
