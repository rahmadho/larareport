<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Panitia extends Model
{
    protected $connection = 'monep';
    protected $table = 'panitia';
    protected $primaryKey = 'pnt_id';
    public $timestamps = false;
    public $incrementing = false;

    public function kabupaten()
    {
        return $this->belongsTo(Kabupaten::class, 'kbp_id', 'kbp_id');
    }
    public function satker()
    {
        return $this->belongsTo(SatuanKerja::class, 'stk_id', 'stk_id');
    }
    public function agency()
    {
        return $this->belongsTo(Agency::class, 'agc_id', 'agc_id');
    }
    public function ukpbj()
    {
        return $this->belongsTo(Ukpbj::class, 'ukpbj_id', 'ukpbj_id');
    }
    public function anggota_panitia()
    {
        return $this->hasMany(AnggotaPanitia::class, 'pnt_id', 'pnt_id');
    }
    public function pegawai()
    {
        // return $this->hasManyThrough(Pegawai::class, AnggotaPanitia::class, 'pnt_id', 'peg_id', 'pnt_id', 'peg_id');
        return $this->belongsToMany(Pegawai::class, 'anggota_panitia', 'pnt_id', 'peg_id')->withPivot(['agp_jabatan']);
    }
    public function paket()
    {
        return $this->hasMany(Paket::class, 'pnt_id', 'pnt_id');
    }
    public function scopeSumbar($query)
    {
        return $query->where('agc_id', 498016);
    }
    public function scopeActive($query)
    {
        return $query->where('is_active', '-1');
    }
    public function scopeTahun($query, $tahun)
    {
        return $query->where('pnt_tahun', $tahun);
    }
}
