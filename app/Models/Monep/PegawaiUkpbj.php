<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class PegawaiUkpbj extends Model
{
    protected $connection = 'monep';
    protected $table = 'pegawai_ukpbj';
    protected $primaryKey = ['peg_id', 'ukpbj_id'];
    public $timestamps = false;
    public $incrementing = false;

    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class, 'peg_id', 'peg_id');
    }
    public function ukpbj()
    {
        return $this->belongsTo(Ukpbj::class, 'ukpbj_id', 'ukpbj_id');
    }
}
