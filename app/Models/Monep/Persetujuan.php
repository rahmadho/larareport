<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Persetujuan extends Model
{
    protected $connection = 'monep';
    protected $table = 'persetujuan';
    protected $primaryKey = 'pst_id';
    public $timestamps = false;
    public $incrementing = false;

    public function lelang()
    {
        return $this->belongsTo(LelangSeleksi::class, 'lls_id', 'lls_id');
    }
}
