<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class BentukUsaha extends Model
{
    protected $connection = 'monep';
    protected $table = 'bentuk_usaha';
    protected $primaryKey = 'btu_id';
    public $timestamps = false;
    public $incrementing = false;
}
