<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $connection = 'monep';
    protected $table = 'pegawai';
    protected $primaryKey = 'peg_id';
    public $timestamps = false;
    public $incrementing = false;

    public function agency()
    {
        return $this->belongsTo(Agency::class, 'agc_id', 'agc_id');
    }
    public function panitia()
    {
        return $this->belongsToMany(Panitia::class, 'anggota_panitia', 'peg_id', 'pnt_id')->withPivot(['agp_jabatan']);
    }
}
