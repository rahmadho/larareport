<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Rekanan extends Model
{
    protected $connection = 'monep';
    protected $table = 'rekanan';
    protected $primaryKey = 'rkn_id';
    public $timestamps = false;
    public $incrementing = false;

    public function bentuk_usaha()
    {
        return $this->belongsTo(BentukUsaha::class, 'btu_id', 'btu_id');
    }
    public function kabupaten()
    {
        return $this->belongsTo(Kabupaten::class, 'kbp_id', 'kbp_id');
    }
}
