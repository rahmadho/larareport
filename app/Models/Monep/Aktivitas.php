<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Aktivitas extends Model
{
    protected $connection = 'monep';
    protected $table = 'aktivitas';
    protected $primaryKey = 'akt_id';
    public $timestamps = false;
    public $incrementing = false;
}
