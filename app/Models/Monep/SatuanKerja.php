<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class SatuanKerja extends Model
{
    protected $connection = 'monep';
    protected $table = 'satuan_kerja';
    protected $primaryKey = 'stk_id';
    public $timestamps = false;
    public $incrementing = false;

    public function agency()
    {
        return $this->belongsTo(Agency::class, 'agc_id', 'agc_id');
    }
    public function instansi()
    {
        return $this->belongsTo(Instansi::class, 'instansi_id', 'id');
    }
    public function paket()
    {
        return $this->hasMany(Paket::class, 'stk_id', 'stk_id');
    }
}
