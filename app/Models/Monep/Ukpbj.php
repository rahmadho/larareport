<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Ukpbj extends Model
{
    protected $connection = 'monep';
    protected $table = 'ukpbj';
    protected $primaryKey = 'ukpbj_id';
    public $timestamps = false;
    public $incrementing = false;

    public function pegawai()
    {
        return $this->belongsToMany(Pegawai::class, 'pegawai_ukpbj', 'ukpbj_id', 'peg_id');
    }
    public function agency()
    {
        return $this->belongsTo(Agency::class, 'agc_id', 'agc_id');
    }
}
