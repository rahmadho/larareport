<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Propinsi extends Model
{
    protected $connection = 'monep';
    protected $table = 'propinsi';
    protected $primaryKey = 'prp_id';
    public $timestamps = false;
    public $incrementing = false;
}
