<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class RouteController extends Model
{
    protected $connection = 'monep';
    protected $table = '_controller';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}
