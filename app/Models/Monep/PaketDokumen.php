<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class PaketDokumen extends Model
{
    protected $connection = 'monep';
    protected $table = 'paket_dokumen';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    public $fillable = ['pkt_id', 'id_dokumen', 'tgl_buat', 'is_ada'];
}
