<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Instansi extends Model
{
    protected $connection = 'monep';
    protected $table = 'instansi';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = false;

    public function kabupaten()
    {
        return $this->belongsTo(Kabupaten::class, 'kbp_id', 'kbp_id');
    }
    public function propinsi()
    {
        return $this->belongsTo(Propinsi::class, 'prp_id', 'prp_id');
    }
}
