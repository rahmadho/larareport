<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class AnggotaPanitia extends Model
{
    protected $connection = 'monep';
    protected $table = 'anggota_panitia';
    protected $primaryKey = ['pnt_id', 'peg_id'];
    public $timestamps = false;
    public $incrementing = false;

    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class, 'peg_id', 'peg_id');
    }
    public function panitia()
    {
        return $this->belongsTo(Panitia::class, 'pnt_id', 'pnt_id');
    }
}
