<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
    protected $connection = 'monep';
    protected $table = 'user_level';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}
