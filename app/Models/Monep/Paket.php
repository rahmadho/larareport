<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    protected $connection = 'monep';
    protected $table = 'paket';
    protected $primaryKey = 'pkt_id';
    public $timestamps = false;
    public $incrementing = false;

    public function satker()
    {
        return $this->belongsTo(SatuanKerja::class, 'stk_id', 'stk_id');
    }
    public function panitia()
    {
        return $this->belongsTo(Panitia::class, 'pnt_id', 'pnt_id');
    }
    public function ppk()
    {
        return $this->belongsTo(Pegawai::class, 'ppk_nip', 'peg_nip');
    }
    // public function lelang()
    // {
    //     return $this->hasMany(LelangSeleksi::class, 'pkt_id', 'pkt_id')->orderBy('lls_dibuat_tanggal', 'desc');
    // }
    public function lelang()
    {
        return $this->hasOne(LelangSeleksi::class, 'pkt_id', 'pkt_id')->orderBy('lls_versi_lelang', 'desc')->orderBy('lls_dibuat_tanggal', 'desc');
    }
    public function lelangs()
    {
        return $this->hasMany(LelangSeleksi::class, 'pkt_id', 'pkt_id')->orderBy('lls_tgl_setuju', 'asc');
    }
    public function dokumen()
    {
        return $this->hasMany(PaketDokumen::class, 'pkt_id', 'pkt_id');
    }
    public function surat()
    {
        return $this->hasOne(Surat::class, 'pkt_id', 'pkt_id')->orderBy('id', 'desc');
    }
    public function spt()
    {
        return $this->hasMany(PaketSpt::class, 'pkt_id', 'pkt_id')->orderBy('tgl_input', 'desc');
    }
    public function persiapan()
    {
        return $this->hasMany(PaketPersiapan::class, 'pkt_id', 'pkt_id')->orderBy('tgl_input', 'desc');
    }
    public function scopeUlp($query)
    {
        return $query->whereHas('surat');
    }
}
