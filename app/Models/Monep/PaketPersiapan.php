<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class PaketPersiapan extends Model
{
    protected $connection = 'monep';
    protected $table = 'paket_persiapan';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    public $fillable = ['pkt_id', 'tgl_rapat', 'agenda', 'tgl_surat', 'tgl_input', 'peg_id'];

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'pkt_id', 'pkt_id');
    }
}
