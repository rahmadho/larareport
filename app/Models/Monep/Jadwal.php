<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $connection = 'monep';
    protected $table = 'jadwal';
    protected $primaryKey = 'dtj_id';
    public $timestamps = false;
    public $incrementing = false;

    public function aktivitas()
    {
        return $this->belongsTo(Aktivitas::class, 'akt_id', 'akt_id')->orderBy('akt_urut', 'asc');
    }

    public function scopeOfSelesai($query, $lls_id)
    {
        return $query->where('thp_id', 18817)->where('lls_id', $lls_id);
    }
}
