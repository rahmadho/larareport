<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $connection = 'monep';
    protected $table = 'agency';
    protected $primaryKey = 'agc_id';
    public $timestamps = false;
    public $incrementing = false;

    public function kabupaten()
    {
        return $this->belongsTo(Kabupaten::class, 'kbp_id', 'kbp_id');
    }
}
