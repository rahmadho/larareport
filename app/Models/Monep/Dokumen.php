<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model
{
    protected $connection = 'monep';
    protected $table = 'dokumen';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}
