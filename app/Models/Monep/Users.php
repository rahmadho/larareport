<?php

namespace App\Models\Monep;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Users extends Authenticatable
{
    use Notifiable;

    protected $connection = 'monep';
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    public function level()
    {
        return $this->belongsTo(UserLevel::class, 'id_level', 'id');
    }
}
