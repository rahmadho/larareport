<?php

namespace App\Models\Monep;

use Illuminate\Database\Eloquent\Model;

class PaketSpt extends Model
{
    protected $connection = 'monep';
    protected $table = 'paket_spt';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    public $fillable = ['pkt_id', 'pnt_id', 'tgl_input', 'no_spt'];

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'pkt_id', 'pkt_id');
    }
    public function panitia()
    {
        return $this->belongsTo(Panitia::class, 'pnt_id', 'pnt_id');
    }
}
