<?php

namespace App\Http\Middleware;

use Closure;

class Sign
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->exists('username')) {
            return $next($request);
        } else {
            $flash = [
                'error' => false,
                'pesan' => 'Berhasil melakukan perubahan data!'
            ];
            return redirect()->route('login')->with('flash', $flash);
        }
    }
}
