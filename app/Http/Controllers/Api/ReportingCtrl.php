<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Epns\KategoriLelang;
use App\Models\Epns\Metode;
use App\Models\Epns\MetodePemilihan;
use App\Models\Epns\Reporting;
use App\Models\Epns\StatusLelang;
use App\Models\Epns\Tahap;
use App\Models\Monep\BentukUsaha;
use App\Models\Monep\LelangSeleksi;
use App\Models\Monep\PaketPersiapan;
use App\Models\Monep\PaketSpt;
use App\Models\Monep\Rekanan;
use Illuminate\Support\Facades\DB;

class ReportingCtrl extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function query()
    {
        $query = DB::connection('monep')->table('paket as ta');
        $query->select('ta.pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_flag', 'kgr_id', 'ta.rup_id', 'sbd_id', 'ang_tahun', 'pkt_status', 'pkt_tgl_buat', 'ta.stk_id', 'ta.pnt_id', 'ppk_nip',);
        $query->addSelect('stk_nama', 'pnt_nama', 'lls_id', 'mtd_id', 'mtd_pemilihan', 'lls_status', 'tahap', 'pemenang', 'lls_versi_lelang', 'lls_tgl_setuju', 'instansi_id', 'is_sppbj', 'is_kontrak');
        $query->addSelect(DB::raw('(SELECT nama FROM instansi WHERE id = instansi_id) as instansi'));
        $query->addSelect(DB::raw("CASE WHEN (pemenang->>'nev_harga_negosiasi')::numeric > 0 THEN pemenang->>'nev_harga_negosiasi'
            WHEN (pemenang->>'nev_harga_terkoreksi')::numeric > 0 THEN pemenang->>'nev_harga_terkoreksi'
            WHEN (pemenang->>'nev_harga')::numeric > 0 THEN pemenang->>'nev_harga'
            WHEN (pemenang->>'psr_harga_terkoreksi')::numeric > 0 THEN pemenang->>'psr_harga_terkoreksi'
            ELSE pemenang->>'psr_harga' END AS pemenang_harga"));
        $query->addSelect(DB::raw("pemenang->>'rkn_id' as pemenang_rekanan"));
        $query->leftJoin('lelang_seleksi as tb', 'ta.pkt_id', '=', 'tb.pkt_id')
            ->leftJoin('panitia as td', 'ta.pnt_id', '=', 'td.pnt_id')
            ->leftJoin('satuan_kerja as tf', 'ta.stk_id', '=', 'tf.stk_id');
        if ($this->request->has('tahun')) {
            $query->where('ang_tahun', $this->request->input('tahun'));
        }
        if ($this->request->has('panitia')) {
            $query->where('ta.pnt_id', $this->request->panitia); // filter menampilkan paket per pokja
        };
        if ($this->request->input('filter_satker') > 0) {
            $query->where('ta.stk_id', $this->request->input('filter_satker')); // menampilkan paket per satuan kerja
        }
        if ($this->request->has('ulang')) {
            $query->where('lls_versi_lelang', '>', 1); // lelang pernah ulang
        }
        if ($this->request->has('gagal')) {
            $query->where('lls_status', 2);
        } else {
            $query->where('lls_status', 1);
        }
        return $query->get();
    }
    public function stats($tahun = null)
    {
        if ($tahun) {
            $per_bulan = [];
            $result = Reporting::lelang(1)->where('ang_tahun', $tahun)->get();
            for ($y = 1; $y <= 12; $y++) {
                $pMonth = Reporting::lelang(1)->where('ang_tahun', $tahun)
                    ->whereRaw("date_part('month', lls_tgl_setuju)=?", [$y])->get();
                $per_bulan[$y] = ["bulan" => $y, "count" => $pMonth->count(), 'pagu' => $pMonth->sum('pkt_pagu'), 'hps' => $pMonth->sum('pkt_hps')];
            }
        } else {
            $result = Reporting::lelang(1)->get();
        }
        $all = [
            "count" => count($result),
            "pagu" => $result->sum('pkt_pagu'),
            "hps" => $result->sum('pkt_hps')
        ];
        $doneCollection = $result->where('tahap', 'SUDAH_SELESAI');
        $done = [
            "count" => count($doneCollection),
            "pagu" => $doneCollection->sum('pkt_pagu'),
            "hps" => $doneCollection->sum('pkt_hps')
        ];
        $runningCollection = $result->where('tahap', '<>', 'SUDAH_SELESAI');
        $running = [
            "count" => count($runningCollection),
            "pagu" => $runningCollection->sum('pkt_pagu'),
            "hps" => $runningCollection->sum('pkt_hps')
        ];
        $successCollection = $result->where('tahap', 'SUDAH_SELESAI')->where('pemenang', '<>', NULL);
        $success = [
            "count" => count($successCollection),
            "pagu" => $successCollection->sum('pkt_pagu'),
            "hps" => $successCollection->sum('pkt_hps'),
            "harga" => $successCollection->sum('pemenang_harga'),
            //"data" => $failCollection->all()
        ];
        $failCollection = $result->where('tahap', 'SUDAH_SELESAI')->where('pemenang', NULL);
        $fail = [
            "count" => count($failCollection),
            "pagu" => $failCollection->sum('pkt_pagu'),
            "hps" => $failCollection->sum('pkt_hps'),
            //"data" => $failCollection->all()
        ];
        $per_sbd_id = array();
        $groupSbd = $result->groupBy('sbd_id');
        foreach ($groupSbd as $key => $val) {
            $per_sbd_id["$key"] = [
                "nama" => $key,
                "count" => count($val),
                "pagu" => $val->sum('pkt_pagu'),
                "hps" => $val->sum('pkt_hps')
            ];
        }
        $per_tahun = array();
        $groupTahun = $result->groupBy('ang_tahun');
        foreach ($groupTahun as $key => $val) {
            $per_tahun["$key"] = [
                "nama" => $key,
                "count" => count($val),
                "pagu" => $val->sum('pkt_pagu'),
                "hps" => $val->sum('pkt_hps')
            ];
        }
        $per_kgr = array();
        $groupKgr = $result->groupBy('kgr_id')->sortBy('kgr_id');
        foreach ($groupKgr as $key => $val) {
            $per_kgr["$key"] = [
                "nama" => KategoriLelang::find($key, true),
                "count" => count($val),
                "pagu" => $val->sum('pkt_pagu'),
                "hps" => $val->sum('pkt_hps')
            ];
        }
        $per_mtd_pemilihan = array();
        $groupMtdPemilihan = $result->groupBy('mtd_pemilihan')->sortBy('mtd_pemilihan');
        foreach ($groupMtdPemilihan as $key => $val) {
            $per_mtd_pemilihan["$key"] = [
                "nama" => MetodePemilihan::find($key, true),
                "count" => count($val),
                "pagu" => $val->sum('pkt_pagu'),
                "hps" => $val->sum('pkt_hps')
            ];
        }
        $per_mtd = array();
        $groupMtd = $result->groupBy('mtd_id')->sortBy('mtd_id');
        foreach ($groupMtd as $key => $val) {
            $per_mtd["$key"] = [
                "nama" => Metode::find($key),
                "count" => count($val),
                "pagu" => $val->sum('pkt_pagu'),
                "hps" => $val->sum('pkt_hps')
            ];
        }
        $per_versi_spse = array();
        $groupFlag = $result->groupBy('pkt_flag')->sortBy('pkt_flag');
        foreach ($groupFlag as $key => $val) {
            if ($key == 3) {
                $spse_versi = "SPSE 4.3";
            } else if ($key == 2) {
                $spse_versi = "SPSE 4.2";
            } else if ($key < 2) {
                $spse_versi = "SPSE 3";
            }
            $per_versi_spse[$spse_versi] = [
                "nama" => $spse_versi,
                "count" => (isset($per_versi_spse[$spse_versi])) ? $per_versi_spse[$spse_versi]['count'] + count($val) : count($val),
                "pagu" => (isset($per_versi_spse[$spse_versi])) ? $per_versi_spse[$spse_versi]['pagu'] + $val->sum('pkt_pagu') : $val->sum('pkt_pagu'),
                "hps" => (isset($per_versi_spse[$spse_versi])) ? $per_versi_spse[$spse_versi]['hps'] + $val->sum('pkt_hps') : $val->sum('pkt_hps'),
                "total" => $result->count()
            ];
        }
        $response = [
            "per_progres" => [
                "all" => $all,
                "selesai" => $done,
                "jalan" => $running,
                "sukses" => $success,
                "gagal" => $fail,
            ],
            "per_sumber_dana" => $per_sbd_id,
            "per_tahun" => $per_tahun,
            "per_kategori" => $per_kgr,
            "per_mtd_pemilihan" => $per_mtd_pemilihan,
            "per_mtd" => $per_mtd,
            "per_versi_spse" => $per_versi_spse,
            "jumlah_rekanan" => ($tahun != null) ? Reporting::count_jumlah_rekanan_by_tahun_daftar($tahun) : Reporting::rekanan()->count()
        ];
        if ($tahun) {
            $response['per_bulan'] = $per_bulan;
        }
        return $response;
    }
    public function stats_per_satker($tahun = null)
    {
        if ($tahun) {
            $results = Reporting::lelang(1)->where('ang_tahun', $tahun)->get();
        } else {
            $results = Reporting::lelang(1)->get();
        }
        $data = [];
        $satker = $results->sortBy('stk_nama')->pluck('stk_nama', 'stk_id')->all();
        $instansi = $results->sortBy('instansi')->pluck('instansi', 'instansi_id')->all();
        $group_satker = $results->sortBy('stk_nama')->groupBy('stk_id');
        foreach ($group_satker as $key => $val) {
            $result = collect($val);
            $all = [
                "count" => count($result),
                "pagu" => $result->sum('pkt_pagu'),
                "hps" => $result->sum('pkt_hps')
            ];
            $doneCollection = $result->where('tahap', 'SUDAH_SELESAI');
            $done = [
                "count" => count($doneCollection),
                "pagu" => $doneCollection->sum('pkt_pagu'),
                "hps" => $doneCollection->sum('pkt_hps')
            ];
            $runningCollection = $result->where('tahap', '<>', 'SUDAH_SELESAI');
            $running = [
                "count" => count($runningCollection),
                "pagu" => $runningCollection->sum('pkt_pagu'),
                "hps" => $runningCollection->sum('pkt_hps')
            ];
            $successCollection = $result->where('tahap', 'SUDAH_SELESAI')->where('pemenang', '<>', NULL);
            $success = [
                "count" => count($successCollection),
                "pagu" => $successCollection->sum('pkt_pagu'),
                "hps" => $successCollection->sum('pkt_hps'),
                "harga" => $successCollection->sum('pemenang_harga'),
            ];
            $failCollection = $result->where('tahap', 'SUDAH_SELESAI')->where('pemenang', NULL);
            $fail = [
                "count" => count($failCollection),
                "pagu" => $failCollection->sum('pkt_pagu'),
                "hps" => $failCollection->sum('pkt_hps'),
            ];
            $spbbjCollection = $result->where('tahap', 'SUDAH_SELESAI')->where('pemenang', '<>', NULL)->where('is_sppbj', TRUE);
            $spbbj = [
                "count" => count($spbbjCollection),
                "pagu" => $spbbjCollection->sum('pkt_pagu'),
                "hps" => $spbbjCollection->sum('pkt_hps'),
            ];
            $kontrakCollection = $result->where('tahap', 'SUDAH_SELESAI')->where('pemenang', '<>', NULL)->where('is_kontrak', TRUE);
            $kontrak = [
                "count" => count($kontrakCollection),
                "pagu" => $kontrakCollection->sum('pkt_pagu'),
                "hps" => $kontrakCollection->sum('pkt_hps'),
            ];
            $get = $result->where('stk_id', $key)->first();
            $data[] = [
                "stk_id" => $key,
                "satker" => $get->stk_nama,
                "instansi" => $get->instansi,
                "tayang" => $all,
                "selesai" => $done,
                "jalan" => $running,
                "sukses" => $success,
                "gagal" => $fail,
                "sppbj" => $spbbj,
                "kontrak" => $kontrak,
            ];
        }
        $response = [
            "per_satker" => $data,
            "satker" => array_values($satker),
            "instansi" => array_values($instansi),
        ];
        return $response;
    }
    public function paket()
    {
        // foreach ($result as $key => $res) {
        //     $result[$key]->metode_pemilihan = MetodePemilihan::find($res->mtd_pemilihan);
        //     $result[$key]->metode = Metode::find($res->mtd_id);
        //     $result[$key]->kategori_lelang = KategoriLelang::find($res->kgr_id);
        //     $result[$key]->rp_pagu = number_format($res->pkt_pagu, 2, ',', '.');
        //     $result[$key]->rp_hps = number_format($res->pkt_hps, 2, ',', '.');
        //     $result[$key]->status_lelang = StatusLelang::find($res->lls_status);
        //     $ulang_label = ($res->kgr_id == 1) ? "Seleksi Ulang" : "Tender Ulang";
        //     $result[$key]->ulang = ($res->lls_versi_lelang > 1) ? $ulang_label : false;
        //     $result[$key]->spse_versi = "";
        //     if ($res->pkt_flag == 3) {
        //         $result[$key]->spse_versi = "SPSE 4.3";
        //     } else if ($res->pkt_flag == 2) {
        //         $result[$key]->spse_versi = "SPSE 4.2";
        //     } else if ($res->pkt_flag < 2) {
        //         $result[$key]->spse_versi = "SPSE 3";
        //     }
        //     $result[$key]->on_sibaja = false;
        //     $result[$key]->tahap_now = Tahap::find($res->tahap);
        //     $result[$key]->pemenang = ($res->pemenang) ? json_decode($res->pemenang) : null;
        //     if ($result[$key]->pemenang) {
        //         $result[$key]->pemenang->rekanan = Rekanan::find($result[$key]->pemenang->rkn_id);
        //     }
        //     $result[$key]->spt = PaketSpt::where('pkt_id', $res->pkt_id)->get();
        //     $result[$key]->persiapan = PaketPersiapan::where('pkt_id', $res->pkt_id)->get();
        //     $result[$key]->lelang = LelangSeleksi::find($res->lls_id);
        // }
    }
    public function table_lelang($tahun = null)
    {
        $query = Reporting::lelang(1);
        if ($this->request->has('query')) {
            $query->where(function ($q) {
                $filter = $this->request->input('query');
                $q->orWhereRaw("LOWER(pkt_nama) LIKE '%" . $filter . "%'");
                $q->orWhereRaw("LOWER(stk_nama) LIKE '%" . $filter . "%'");
                $q->orWhere("lls_id", $filter);
                $q->orWhere("ang_tahun", $filter);
            });
        }
        if ($tahun) {
            $query->where('ang_tahun', $tahun);
        }
        if ($this->request->has('stk_id')) {
            $filter_stk = $this->request->stk_id;
            $query->where('ta.stk_id', $filter_stk);
            $all_count = $query->get()->count();
            $result = $query->get();
        } else {
            $all_count = $query->get()->count();
            $page = ($this->request->has('limit')) ? $this->request->page : 1;
            $limit = ($this->request->has('limit')) ? $this->request->limit : 10;
            $offset = ($page - 1) * $limit;
            $query->offset($offset)->limit($limit);
            $result = $query->get();
        }

        foreach ($result as $idx => $row) {
            $result[$idx]->rekanan = null;
            if ($row->pemenang != null) {
                $result[$idx]->rekanan = Rekanan::find($row->pemenang_rekanan);
                $result[$idx]->jumlah_peserta = DB::table('peserta')->where('lls_id', $row->lls_id)->count();
            }
        }
        $response = [
            "table" => [
                "rows" => $result,
                "count" => $all_count
            ]
        ];
        // $response = [
        //     "rows" => $result,
        //     "count" => $all_count
        // ];

        // $perY = $result->groupBy('ang_tahun')->all();
        // $pertahun = [];
        // foreach ($perY as $k => $v) {
        //     $pertahun[$k]['tahun'] = $k;
        //     $pertahun[$k]['count'] = count($v);
        //     $pertahun[$k]['pagu'] = $perY[$k]->sum('pkt_pagu');
        //     $pertahun[$k]['hps'] = $perY[$k]->sum('pkt_hps');
        // }
        // $ofset = $this->request->ofset ?? 0;
        // $limit = $this->request->limit ?? 10;
        // $response = [
        //     "table" => $result->slice($ofset, $limit),
        //     "pertahun" => $pertahun
        // ];
        return $response;
    }
    public function rekanan($tahun = null)
    {
        $filter = $this->request->query ?? null;
        $ofset = $this->request->ofset ?? 0;
        $limit = $this->request->limit ?? 10;
        $query = Reporting::rekanan();
        if ($tahun != null) {
            $query->whereRaw("date_part('year', rkn_tgl_daftar) = ?", [$tahun]);
        }
        if ($filter) {
            $query->whereRaw("lower(rkn_nama) like '%" . strtolower($this->request->input('query')) . "%'");
        }
        if ($this->request->input('ascending')) {
            $query->orderBy('rkn_nama', 'asc');
        } else {
            $query->orderBy('rkn_nama', 'desc');
        }
        $rekanan = $query->get();
        $bentuk_usaha = BentukUsaha::all();
        foreach ($bentuk_usaha as $k => $v) {
            $bentuk_usaha[$k]->jumlah_penyedia = $rekanan->where('btu_id', $v->btu_id)->count();
        }
        $per_prp = [];
        $group_perprovinsi = $rekanan->groupBy('prp_id');
        $provinsi = [];
        foreach ($group_perprovinsi as $k => $v) {
            $per_prp[$k]['provinsi'] = ($v->first()->prp_nama) ?? "Lainnya";
            $per_prp[$k]['terdaftar'] = $v->count();
            $per_prp[$k]['verif'] = $v->where('rkn_status', 1)->count();
            $per_prp[$k]['tolak'] = $v->where('rkn_status', -2)->count();
            $per_prp[$k]['unverif'] = $v->where('rkn_status', 0)->count();
            $per_prp[$k]['expired'] = $v->where('rkn_status', -3)->count();
            $provinsi[] = ($v->first()->prp_nama) ?? "Lainnya";
        }
        $per_kbp = [];
        $group_perkbp = $rekanan->groupBy('kbp_id');
        $kabupaten = [];
        foreach ($group_perkbp as $k => $v) {
            $per_kbp[$k]['kabupaten'] = ($v->first()->kbp_nama) ?? "Lainnya";
            $per_kbp[$k]['provinsi'] = ($v->first()->prp_nama) ?? "Lainnya";
            $per_kbp[$k]['count'] = $v->count();
            $kabupaten[] = ($v->first()->kbp_nama) ?? "Lainnya";
        }
        $data_rekanan = $rekanan->slice($ofset, $limit);
        foreach ($data_rekanan as $k => $v) {
            $jml_ikut = DB::table('peserta')->where("rkn_id", $v->rkn_id)->get()->count();
            $jml_menang = DB::connection('monep')->table('lelang_seleksi')->whereRaw("pemenang->>'rkn_id'=?", [$v->rkn_id])->get()->count();
            $v->jml_ikut = $jml_ikut;
            $v->jml_menang = $jml_menang;
        }
        $response = [
            "total_penyedia" => $rekanan->count(),
            "bentuk_usaha" => $bentuk_usaha,
            "per_provinsi" => $per_prp,
            "per_kabupaten" => $per_kbp,
            "rekanan" => $data_rekanan,
            "provinsi" => $provinsi,
            "kabupaten" => $kabupaten
        ];
        return $response;
    }
}
