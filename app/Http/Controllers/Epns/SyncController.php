<?php

namespace App\Http\Controllers\Epns;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class SyncController extends Controller
{
    private $firstTime;
    private $result;
    private $request;
    public function sync(Request $request, $table = null, $firstTime = false)
    {
        $this->lastUpdate = Carbon::now()->subDay(30); // kemarin
        $this->request = $request;
        $this->firstTime = false; //$firstTime;
        $this->$table();
        dd($this->result);
    }
    public function sync_all(Request $request)
    {
        $this->lastUpdate = Carbon::now()->subDay(30); // 30 hari terakhir
        $this->request = $request;
        $this->firstTime = false;
        self::propinsi();
        self::kabupaten();
        self::instansi();
        self::agency();
        self::satuan_kerja();
        self::rekanan();
        self::panitia();
        self::ukpbj();
        self::anggota_panitia();
        self::pegawai();
        self::pegawai_ukpbj();
        self::aktivitas();
        self::jadwal();
        self::persetujuan();
        self::paket();
        self::lelang();
        dd($this->result);
    }
    public function propinsi()
    {
        $get = DB::connection('epns')->table('propinsi')->select('prp_id', 'prp_nama', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get = $get->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $data[] = (array) $arr;
            if (!$this->firstTime) {
                $this->result = DB::connection('monep')->table('propinsi')->updateOrInsert(['prp_id' => $arr->prp_id], (array) $arr);
            }
        }
        if ($this->firstTime) {
            $this->result = DB::connection('monep')->table('propinsi')->insert($data);
        }
    }
    public function kabupaten()
    {
        $get = DB::connection('epns')->table('kabupaten')->select('kbp_id', 'prp_id', 'kbp_nama', 'kbp_statusadm', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get = $get->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $data[] = (array) $arr;
            if (!$this->firstTime) {
                $this->result = DB::connection('monep')->table('kabupaten')->updateOrInsert(['kbp_id' => $arr->kbp_id], (array) $arr);
            }
        }
        if ($this->firstTime) {
            $this->result = DB::connection('monep')->table('kabupaten')->insert($data);
        }
    }
    public function instansi()
    {
        $get = DB::connection('epns')->table('instansi')->select('id', 'alamat', 'jenis', 'kbp_id', 'nama', 'prp_id');
        $get = $get->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $data[] = (array) $arr;
            if (!$this->firstTime) {
                $this->result = DB::connection('monep')->table('instansi')->updateOrInsert(['id' => $arr->id], (array) $arr);
            }
        }
        if ($this->firstTime) {
            $this->result = DB::connection('monep')->table('instansi')->insert($data);
        }
    }
    public function agency()
    {
        $get = DB::connection('epns')->table('agency')->select('agc_id', 'kbp_id', 'agc_nama', 'agc_alamat', 'agc_telepon', 'agc_fax', 'agc_website', 'agc_tgl_daftar', 'jna_id', 'sub_agc_id', 'is_lpse', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get = $get->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $data[] = (array) $arr;
            if (!$this->firstTime) {
                $this->result = DB::connection('monep')->table('agency')->updateOrInsert(['agc_id' => $arr->agc_id], (array) $arr);
            }
        }
        if ($this->firstTime) {
            $this->result = DB::connection('monep')->table('agency')->insert($data);
        }
    }
    public function satuan_kerja()
    {
        $get = DB::connection('epns')->table('satuan_kerja')->select('stk_id', 'agc_id', 'stk_nama', 'stk_alamat', 'stk_telepon', 'stk_contact_person', 'stk_fax', 'stk_kodepos', 'stk_kode', 'instansi_id', 'rup_stk_id', 'rup_stk_tahun', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        // $get->where('auditupdate', '>', $this->lastUpdate);
        $get = $get->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $arr->rup_stk_tahun = str_replace(['{', '}', '[', ']', ' '], ['', '', '', '', ''], $arr->rup_stk_tahun);
            $data[] = (array) $arr;
            if (!$this->firstTime) {
                $this->result = DB::connection('monep')->table('satuan_kerja')->updateOrInsert(['stk_id' => $arr->stk_id], (array) $arr);
            }
        }
        if ($this->firstTime) {
            $this->result = DB::connection('monep')->table('satuan_kerja')->insert($data);
        }
    }
    public function rekanan()
    {
        DB::connection('monep')->disableQueryLog();
        $get = DB::connection('epns')->table('rekanan')->select('rkn_id', 'btu_id', 'kbp_id', 'rkn_nama', 'rkn_alamat', 'rkn_kodepos', 'rkn_telepon', 'rkn_fax', 'rkn_mobile_phone', 'rkn_npwp', 'rkn_pkp', 'rkn_statcabang', 'rkn_email', 'rkn_website', 'rkn_tgl_daftar', 'rkn_tgl_setuju', 'rkn_almtpusat', 'rkn_telppusat', 'rkn_faxpusat', 'rkn_emailpusat', 'ngr_id', 'kota', 'rkn_status', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get->orderBy('rkn_id', 'asc')->chunk(2000, function ($part) {
            $get = $part->toArray();
            $data = [];
            foreach ($get as $arr) {
                $data[] = (array) $arr;
                if (!$this->firstTime) {
                    $this->result = DB::connection('monep')->table('rekanan')->updateOrInsert(['rkn_id' => $arr->rkn_id], (array) $arr);
                }
            }
            if ($this->firstTime) {
                $this->result = DB::connection('monep')->table('rekanan')->insert($data);
            }
        });
    }
    public function peserta()
    {
        $get = DB::connection('epns')->table('peserta as ta')->select('psr_id', 'rkn_id', 'tb.lls_id', 'psr_tanggal', 'is_pemenang', 'is_pemenang_verif', 'psr_harga', 'sudah_verifikasi_sikap', 'is_dikirim_pesan', 'sudah_evaluasi_kualifikasi', 'is_pemenang_klarifikasi', 'auditupdate');
        $get->join('lelang_seleksi as tb', 'ta.lls_id', '=', 'tb.lls_id');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get->orderBy('psr_id', 'asc')->chunk(2000, function ($part) {
            $get = $part->toArray();
            $data = [];
            foreach ($get as $arr) {
                $data[] = (array) $arr;
                if (!$this->firstTime) {
                    $this->result = DB::connection('monep')->table('peserta')->updateOrInsert(['psr_id' => $arr->psr_id], (array) $arr);
                }
            }
            if ($this->firstTime) {
                $this->result = DB::connection('monep')->table('peserta')->insert($data);
            }
        });
    }
    public function panitia()
    {
        $get = DB::connection('epns')->table('panitia')->select('pnt_id', 'stk_id', 'pnt_nama', 'pnt_tahun', 'pnt_no_sk', 'agc_id', 'pnt_alamat', 'pnt_email', 'pnt_fax', 'pnt_telp', 'pnt_website', 'kbp_id', 'ukpbj_id', 'is_active', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get = $get->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $data[] = (array) $arr;
            if (!$this->firstTime) {
                $this->result = DB::connection('monep')->table('panitia')->updateOrInsert(['pnt_id' => $arr->pnt_id], (array) $arr);
            }
        }
        if ($this->firstTime) {
            $this->result = DB::connection('monep')->table('panitia')->insert($data);
        }
    }
    public function ukpbj()
    {
        $get = DB::connection('epns')->table('ukpbj')->select('ukpbj_id', 'agc_id', 'kpl_unit_pemilihan_id', 'nama', 'alamat', 'telepon', 'fax', 'tgl_daftar', 'is_lpse', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get = $get->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $data[] = (array) $arr;
            if (!$this->firstTime) {
                $this->result = DB::connection('monep')->table('ukpbj')->updateOrInsert(['ukpbj_id' => $arr->ukpbj_id], (array) $arr);
            }
        }
        if ($this->firstTime) {
            $this->result = DB::connection('monep')->table('ukpbj')->insert($data);
        }
    }
    public function anggota_panitia()
    {
        $get = DB::connection('epns')->table('anggota_panitia')->select('pnt_id', 'agp_jabatan', 'peg_id', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get = $get->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $data[] = (array) $arr;
            if (!$this->firstTime) {
                $this->result = DB::connection('monep')->table('anggota_panitia')->updateOrInsert(['pnt_id' => $arr->pnt_id, 'peg_id' => $arr->peg_id], (array) $arr);
            }
        }
        if ($this->firstTime) {
            $this->result = DB::connection('monep')->table('anggota_panitia')->insert($data);
        }
    }
    public function pegawai()
    {
        $get = DB::connection('epns')->table('pegawai');
        // $get->select('peg_id', 'peg_nip', 'peg_nama', 'peg_alamat', 'peg_telepon', 'peg_mobile', 'peg_email', 'peg_golongan', 'peg_pangkat', 'peg_jabatan', 'peg_isactive', 'peg_namauser', 'peg_no_sk', 'peg_masa_berlaku', 'agc_id', 'auditupdate');
        $get->select('peg_id', 'peg_nip', 'peg_nama', 'peg_alamat', 'peg_telepon', 'peg_mobile', 'peg_email', 'peg_isactive', 'peg_namauser', 'peg_no_sk', 'peg_masa_berlaku', 'agc_id', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get = $get->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $data[] = (array) $arr;
            if (!$this->firstTime) {
                $this->result = DB::connection('monep')->table('pegawai')->updateOrInsert(['peg_id' => $arr->peg_id], (array) $arr);
            }
        }
        if ($this->firstTime) {
            $this->result = DB::connection('monep')->table('pegawai')->insert($data);
        }
    }
    public function pegawai_ukpbj()
    {
        $get = DB::connection('epns')->table('pegawai_ukpbj')->select('ukpbj_id', 'peg_id', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get = $get->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $data[] = (array) $arr;
            if (!$this->firstTime) {
                $this->result = DB::connection('monep')->table('pegawai_ukpbj')->updateOrInsert(['peg_id' => $arr->peg_id, 'ukpbj_id' => $arr->ukpbj_id], (array) $arr);
            }
        }
        if ($this->firstTime) {
            $this->result = DB::connection('monep')->table('pegawai_ukpbj')->insert($data);
        }
    }
    public function aktivitas()
    {
        $get = DB::connection('epns')->table('aktivitas')->select('akt_id', 'mtd_id', 'akt_jenis', 'akt_urut', 'akt_status', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get = $get->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $data[] = (array) $arr;
            if (!$this->firstTime) {
                $this->result = DB::connection('monep')->table('aktivitas')->updateOrInsert(['akt_id' => $arr->akt_id], (array) $arr);
            }
        }
        if ($this->firstTime) {
            $this->result = DB::connection('monep')->table('aktivitas')->insert($data);
        }
    }
    public function jadwal()
    {
        DB::connection('monep')->disableQueryLog();
        $get = DB::connection('epns')->table('jadwal')->select('dtj_id', 'thp_id', 'lls_id', 'dtj_tglawal', 'dtj_tglakhir', 'dtj_keterangan', 'akt_id', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get->orderBy('dtj_id', 'asc')->chunk(2000, function ($part) {
            $get = $part->toArray();
            $data = [];
            foreach ($get as $arr) {
                $data[] = (array) $arr;
                if (!$this->firstTime) {
                    $this->result = DB::connection('monep')->table('jadwal')->updateOrInsert(['dtj_id' => $arr->dtj_id], (array) $arr);
                }
            }
            if ($this->firstTime) {
                $this->result = DB::connection('monep')->table('jadwal')->insert($data);
            }
        });
    }
    public function persetujuan()
    {
        DB::connection('monep')->disableQueryLog();
        $get = DB::connection('epns')->table('persetujuan')->select('pst_id', 'pst_alasan', 'pst_jenis', 'pst_status', 'pst_tgl_setuju', 'lls_id', 'peg_id', 'auditupdate');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get->orderBy('pst_id', 'asc')->chunk(2000, function ($part) {
            $get = $part->toArray();
            $data = [];
            foreach ($get as $arr) {
                $data[] = (array) $arr;
                if (!$this->firstTime) {
                    $this->result = DB::connection('monep')->table('persetujuan')->updateOrInsert(['pst_id' => $arr->pst_id], (array) $arr);
                }
            }
            if ($this->firstTime) {
                $this->result = DB::connection('monep')->table('persetujuan')->insert($data);
            }
        });
    }
    public function bentuk_usaha()
    {
        DB::connection('monep')->disableQueryLog();
        $get = DB::connection('epns')->table('bentuk_usaha')->select('btu_id', 'auditupdate', 'btu_nama');
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get->orderBy('btu_id', 'asc')->chunk(2000, function ($part) {
            $get = $part->toArray();
            $data = [];
            foreach ($get as $arr) {
                $data[] = (array) $arr;
                if (!$this->firstTime) {
                    $this->result = DB::connection('monep')->table('bentuk_usaha')->updateOrInsert(['btu_id' => $arr->btu_id], (array) $arr);
                }
            }
            if ($this->firstTime) {
                $this->result = DB::connection('monep')->table('bentuk_usaha')->insert($data);
            }
        });
    }
    public function paket()
    {
        $get = DB::connection('epns')->table('paket as p')->select('p.pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_tgl_realisasi', 'pkt_tgl_assign', 'pkt_tgl_buat', 'pkt_tgl_ppk_setuju', 'pkt_status', 'kls_id', 'pkt_flag', 'kgr_id', 'auditupdate');
        $get->addSelect(DB::raw("(SELECT distinct stk_id FROM paket_satker ps WHERE ps.pkt_id=p.pkt_id LIMIT 1) as stk_id"));
        $get->addSelect(DB::raw("(SELECT distinct rup_id FROM paket_anggaran as pa WHERE pa.pkt_id=p.pkt_id LIMIT 1) as rup_id"));
        $get->addSelect(DB::raw("(SELECT distinct ang.ang_tahun FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id LIMIT 1) as ang_tahun"));
        $get->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.sbd_id), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as sbd_id"));
        $get->addSelect(DB::raw("(SELECT peg_nip FROM pegawai JOIN ppk ON pegawai.peg_id=ppk.peg_id WHERE ppk_id = (SELECT DISTINCT ppk_id FROM paket_ppk WHERE paket_ppk.pkt_id=p.pkt_id LIMIT 1)) as ppk_nip"));
        $get->addSelect(DB::raw("(SELECT peg_id FROM ppk WHERE ppk_id = (SELECT DISTINCT ppk_id FROM paket_ppk WHERE paket_ppk.pkt_id=p.pkt_id LIMIT 1)) as ppk_peg_id"));
        $get->addSelect(DB::raw("(SELECT distinct(pnt_id) FROM paket_panitia ps WHERE ps.pkt_id=p.pkt_id LIMIT 1) as pnt_id"));
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get->orderBy('pkt_id', 'asc')->chunk(1000, function ($part) {
            $get = $part->toArray();
            $data = [];
            foreach ($get as $arr) {
                $data[] = (array) $arr;
                if (!$this->firstTime) {
                    $this->result = DB::connection('monep')->table('paket')->updateOrInsert(['pkt_id' => $arr->pkt_id], (array) $arr);
                }
            }
            if ($this->firstTime) {
                $this->result = DB::connection('monep')->table('paket')->insert($data);
            }
        });
    }
    public function lelang()
    {
        $get = DB::connection('epns')->table('lelang_seleksi as ta')->select('lls_id', 'pkt_id', 'mtd_id', 'lls_versi_lelang', 'lls_keterangan', 'lls_dibuat_tanggal', 'lls_diulang_karena', 'lls_ditutup_karena', 'lls_terverifikasi', 'lls_tgl_setuju', 'lls_status', 'mtd_pemilihan', 'mtd_evaluasi', 'auditupdate', 'lls_kontrak_pembayaran', 'lls_kontrak_pekerjaan', 'lls_kontrak_tahun', 'lls_kontrak_sbd', 'lls_penetapan_pemenang');
        $get->addSelect(DB::raw("(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=ta.lls_id) as eva_versi"));
        $get->addSelect(DB::raw("CASE WHEN (SELECT COUNT(*) FROM sppbj WHERE lls_id=ta.lls_id) > 0 THEN true ELSE false END AS is_sppbj"));
        $get->addSelect(DB::raw('CASE WHEN (SELECT COUNT(*) FROM kontrak WHERE lls_id=ta.lls_id) > 0 THEN true ELSE false END AS is_kontrak'));
        $get->addSelect(DB::raw('(SELECT kontrak_nilai FROM kontrak WHERE lls_id=ta.lls_id ORDER BY auditupdate DESC LIMIT 1) AS nilai_kontrak'));
        $get->addSelect(DB::raw('tahap_now(lls_id, localtimestamp) as tahap'));
        // $if_cepat = "(mtd_pemilihan=9 OR mtd_pemilihan=10) AND (SELECT COUNT(*) FROM peserta WHERE lls_id=ta.lls_id AND is_pemenang_verif=1) > 0";
        // $harga_cepat = "(SELECT DISTINCT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=ta.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=ta.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1 LIMIT 1)";
        // $harga_ = "(SELECT DISTINCT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=ta.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=ta.lls_id) AND eva_jenis=4) AND psr.is_pemenang=1 AND nev_urutan=1 LIMIT 1)";
        // $get->addSelect(DB::raw("CASE WHEN {$if_cepat} THEN {$harga_cepat} ELSE {$harga_} END as pemenang_harga"));
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', ta.auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get->orderBy('lls_id', 'asc')->chunk(2000, function ($part) {
            $get = $part->toArray();
            $data = [];
            foreach ($get as $arr) {
                $pemenang = $this->pemenang($arr);
                $exculde = [1236016, 1235016]; // pemenang paket ini susah ngambil harga penawaran
                if (!in_array($arr->lls_id, $exculde)) {
                    $arr->pemenang = ($pemenang) ? json_encode($pemenang) : null;
                }
                $data[] = (array) $arr;
                if (!$this->firstTime) {
                    $this->result = DB::connection('monep')->table('lelang_seleksi')->updateOrInsert(['lls_id' => $arr->lls_id], (array) $arr);
                }
            }
            if ($this->firstTime) {
                $this->result = DB::connection('monep')->table('lelang_seleksi')->insert($data);
            }
        });
    }

    private function pemenang($lelang)
    {
        $select = "nev_lulus,nev_urutan,nev_harga,nev_harga_terkoreksi,nev_harga_negosiasi,psr.psr_id,rkn_id,psr_harga,psr_harga_terkoreksi,is_pemenang,is_pemenang_verif";
        $pemenang = null;
        if ($lelang) {
            $mtd_pemilihan = $lelang->mtd_pemilihan;
            if ($mtd_pemilihan == 9 || $mtd_pemilihan == 10) {
                $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang_verif =1", ["lls_id" => $lelang->lls_id]);
            }
            if ($pemenang == null) {
                $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang =1", ["lls_id" => $lelang->lls_id]);
            }
            if (sizeof($pemenang) > 1) {
                // klo perlu tambah and nev_urutan = 1
                if ($mtd_pemilihan == 9 || $mtd_pemilihan == 10) {
                    $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang_verif =1 AND nev_urutan=1", ["lls_id" => $lelang->lls_id]);
                    //if (!$pemenang) $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang_verif =1", ["lls_id" => $lelang->lls_id]);
                } else {
                    $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang =1 AND nev_urutan=1", ["lls_id" => $lelang->lls_id]);
                    //if (!$pemenang) $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang =1", ["lls_id" => $lelang->lls_id]);
                }
            }
        }
        return ($pemenang) ? $pemenang[0] : $pemenang;
    }
}
