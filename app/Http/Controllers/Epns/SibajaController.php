<?php

namespace App\Http\Controllers\Epns;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sibaja\PaketPanitia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Sibaja\PaketPersiapan;
use App\Models\Sibaja\Paket;
use App\Models\Sibaja\PaketDokumen;
use Illuminate\Support\Facades\DB;
use App\Models\Sibaja\Surat;
use App\Models\Epns\PaketDokumen as EpnsPaketDokumen;

class SibajaController extends Controller
{
    public function __construct()
    {
        $this->dokumen = collect([
            0 => array(1, 2, 3, 4, 7, 8, 9, 10),
            1 => array(1, 2, 3, 4, 7, 8),
            2 => array(1, 2, 3, 4, 5, 6, 7, 8),
            3 => array(1, 2, 3, 4, 7, 8, 9, 10)
        ]);
    }
    public function store(Request $request)
    {
        $id = $request->input('lls_id');
        if (Paket::where('lls_id', $id)->first()) {
            $flash = [
                'error' => true,
                'pesan' => 'Paket ini sudah dibuat!'
            ];
            return redirect()->route('epns.lelang.index')->with('flash', $flash);
        }
        $query = DB::connection('epns')->table('paket as p')
            ->selectRaw("p.pkt_id, pkt_nama, pkt_tgl_buat, p.pnt_id, pn.pnt_nama,pkt_pagu,pkt_hps,kgr_id, pkt_status, lls_id, mtd_pemilihan, mtd_id")
            ->leftJoin('lelang_seleksi as lls', 'lls.pkt_id', '=', 'p.pkt_id')
            ->leftJoin('panitia as pn', function ($join) {
                $join->on('pn.pnt_id', '=', 'p.pnt_id')
                    ->whereRaw('lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=p.pkt_id)');
            });
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.ang_tahun), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as tahun_anggaran"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.sbd_id), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as sumber_dana"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct rup_id), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as rup_id"));
        $paket = $query->where('lls_id', $id)->first();
        $satker = DB::connection('epns')->table("satuan_kerja as s")->select('s.stk_id', 's.stk_nama')->join("paket_satker as ps", "ps.stk_id", "=", "s.stk_id")->where("ps.pkt_id", $paket->pkt_id)->first();
        $surat = new Surat;
        $surat->no_surat = 'AUTO';
        $surat->stk_id = (isset($satker->stk_id)) ? $satker->stk_id : null;
        $surat->pengirim = $satker->stk_nama;
        $surat->id_jenis = 1;
        $surat->tgl_surat = $request->input('tgl_surat_pengantar');
        $surat->penerima = 'ULP';
        $surat->isi = 'Paket Lelang';
        $surat->tgl_masuk = date('Y-m-d H:i:s');
        if ($surat->save()) {
            $new = new Paket;
            $new->id_surat = $surat->id;
            $new->mtd_id = $paket->mtd_id;
            $new->pkt_id = $paket->pkt_id;
            $new->pkt_pagu = $paket->pkt_pagu;
            $new->pkt_hps = $paket->pkt_hps;
            $new->kgr_id = $paket->kgr_id;
            $new->sbd_id = $paket->sumber_dana;
            $new->rup_id = $paket->rup_id;
            $new->lls_id = $id;
            $new->stk_id = (isset($satker->stk_id)) ? $satker->stk_id : null;
            $new->stk_nama = $satker->stk_nama;
            $new->pkt_nama = $paket->pkt_nama;
            $new->ppk_nip = $request->input('ppk_nip');
            $new->ppk_nama = $request->input('ppk');
            $new->kegiatan = $request->input('kegiatan');
            $new->tahun = $paket->tahun_anggaran;
            $new->mtd_pemilihan = $paket->mtd_pemilihan;
            $new->tgl_surat = $request->input('tgl_surat_pengantar');
            if ($request->has('id_surat')) {
                $new->id_surat = $request->input('id_surat');
            }
            if ($new->save()) {
                // insert paket dokumen 
                $paket_doc = EpnsPaketDokumen::get_key($paket->kgr_id);
                foreach ($paket_doc as $doc) {
                    PaketDokumen::firstOrCreate(
                        ['pkt_id' => $paket->pkt_id, 'id_dokumen' => $doc],
                        ['is_ada' => false, 'is_ok' => false, 'tgl_buat' => date('Y-m-d H:i:s')]
                    );
                }
                $flash = [
                    'error' => false,
                    'pesan' => 'Berhasil melakukan perubahan data!'
                ];
            } else {
                $flash = [
                    'error' => true,
                    'pesan' => 'Gagal melakukan perubahan data!'
                ];
            }
            if ($request->has('id_surat')) {
                return redirect()->route('epns.lelang.index')->with('flash', $flash);
            } else {
                return redirect()->route('epns.lelang.index')->with('flash', $flash);
            }
        }
    }
    public function dokumen(Request $request, $id)
    {
        // id_paket merujuk pkt_id
        $array = [];
        $paket_dokumen = PaketDokumen::where('pkt_id', $id)->get();
        foreach ($paket_dokumen as $doc) {
            $dokumen = PaketDokumen::findOrFail($doc->id);
            if (isset($request->input('is_ada')[$doc->id])) $dokumen->is_ada = true;
            if (isset($request->input('is_ok')[$doc->id])) $dokumen->is_ok = true;
            $dokumen->save();
            array_push($array, $dokumen);
        }
        return redirect()->route('epns.lelang.info', $id);
    }
    public function spt(Request $request, $id)
    {
        // id_paket merujuk pkt_id
        $do = PaketPanitia::updateOrCreate(
            [
                'pkt_id' => $request->input('pkt_id'),
                'pnt_id' => $request->input('pnt_id')
            ],
            [
                'user_buat' => Auth::user()->username,
                'no_surat' => $request->input('no-surat-spt'),
                'tgl_buat' => date('Y-m-d H:i:s'),
                'pnt_nama' => $request->input('pnt_nama')
            ]
        );
        if ($do) {
            $flash = [
                'error' => false,
                'pesan' => 'Berhasil melakukan perubahan data!'
            ];
        } else {
            $flash = [
                'error' => true,
                'pesan' => 'Gagal melakukan perubahan data!'
            ];
        }
        return redirect()->route('epns.lelang.info', $id)->with('flash', $flash);
    }
    public function rapat_persiapan(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'tgl_surat' => 'required',
            'agenda' => 'required',
            'tgl_rapat' => 'required',
            'time_rapat' => 'required'
        ]);
        if ($validator->fails()) {
            $flash = [
                'error' => true,
                'pesan' => 'Input agenda rapat persiapan tidak lengkap!'
            ];
            return redirect()->route('epns.lelang.info', $id)
                ->withInput()
                ->withErrors($validator)
                ->with('flash', $flash);
        }
        $do = PaketPersiapan::create(['pkt_id' => $id, 'tgl_rapat' => "{$request->input('tgl_rapat')} {$request->input('time_rapat')}", 'agenda' => $request->input('agenda')]);
        if ($do) {
            $flash = [
                'error' => false,
                'pesan' => 'Berhasil melakukan perubahan data!'
            ];
        } else {
            $flash = [
                'error' => true,
                'pesan' => 'Gagal melakukan perubahan data!'
            ];
        }
        return redirect()->route('epns.lelang.info', $id)->with('flash', $flash);
    }
}
