<?php

namespace App\Http\Controllers\Epns;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Sibaja\Paket;
use App\Models\Sibaja\PaketDokumen;
use App\Models\Sibaja\Surat;
use stdClass;
use App\Models\Sibaja\PaketPersiapan;
use App\Models\Sibaja\PaketPanitia;
use App\Models\Epns\PaketDokumen as EpnsPaketDokumen;
use App\Models\Epns\Metode;
use App\Models\Epns\Tahap;
use App\Models\Epns\MetodePemilihan;
use App\Models\Epns\StatusLelang;
use App\Models\Epns\KategoriLelang;
use App\Models\Epns\Fungsi;

class PaketController extends Controller
{
    public function index()
    {
        return view('monep.index');
        // $sql = "SELECT p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, pn.pnt_nama, (SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama, pkt_pagu, pkt_status, pkt_flag, lls_id, mtd_pemilihan, tahap_now(lls_id, localtimestamp) AS tahaps, lls_versi_lelang, lls_status 
        //     FROM paket p 
        //     LEFT JOIN satuan_kerja s ON p.stk_id=s.stk_id 
        //     LEFT JOIN lelang_seleksi lls ON lls.pkt_id=p.pkt_id 
        //     LEFT JOIN panitia pn ON pn.pnt_id = p.pnt_id  AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=p.pkt_id) 
        //     WHERE lls.lls_status = 1
        //     ORDER BY pkt_tgl_buat DESC NULLS LAST LIMIT 10";
        // $pakets = DB::connection('epns')->select($sql);
        // foreach ($pakets as $key => $paket) {
        //     $pakets[$key]->persetujuan_tayang = DB::connection('epns')->select("SELECT * FROM persetujuan WHERE pst_jenis = 0 AND lls_id = ?", [$paket->lls_id]);
        // }
        // return $pakets;
    }
    // id -> pkt_id
    public function info(Request $request, $id)
    {
        $query = DB::connection('epns')->table('paket as p')
            ->selectRaw("p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, p.pnt_id, pn.pnt_nama, pkt_pagu, pkt_status, pkt_flag, lls_id, mtd_pemilihan,lls_tgl_setuju")
            ->leftJoin('satuan_kerja as s', 's.stk_id', '=', 'p.stk_id')
            ->leftJoin('lelang_seleksi as lls', 'lls.pkt_id', '=', 'p.pkt_id')
            ->leftJoin('panitia as pn', function ($join) {
                $join->on('pn.pnt_id', '=', 'p.pnt_id');
            });
        $query->addSelect(DB::raw("tahap_now(lls_id, localtimestamp) AS tahaps, lls_versi_lelang, lls_status,s.stk_id"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.ang_tahun), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as tahun_anggaran"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct rup_id), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as rup_id"));
        // uraian
        //$query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.ang_uraian), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as ang_uraian"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.sbd_id), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as sumber_dana"));
        $query->addSelect(DB::raw("p.pkt_hps, lls.mtd_id, lls.mtd_pemilihan, lls.mtd_evaluasi, kgr_id, paket_instansi(p.pkt_id) as nama_instansi, lls_penawaran_ulang,(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) as eva_versi"));
        $query->whereRaw('lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=p.pkt_id)');
        $paket = $query->where('p.pkt_id', $id)->first();
        //return [$paket->kgr_id];
        $dokumen = EpnsPaketDokumen::find($paket->kgr_id);
        $panitia = Fungsi::panitia($paket->pkt_id);
        $jadwal = Fungsi::jadwal($paket->lls_id);
        $peserta = Fungsi::peserta($paket->lls_id);
        $hasil_evaluasi = Fungsi::hasil_evaluasi($paket->lls_id);
        $pemenang = Fungsi::pemenang($paket);
        if ($paket) {
            $paket->sibaja = Paket::access()->with(['surat', 'persiapan'])->where('pkt_id', $paket->pkt_id)->first();
            $paket->spt = PaketPanitia::where('pkt_id', $paket->lls_id)->orderBy('tgl_buat', 'desc')->first();
            $persiapan = PaketPersiapan::where('pkt_id', $paket->pkt_id)->orderBy('id', 'desc')->get();
        }
        $paket = $this->show_result($paket);
        $return = [
            "paket" => $this->show_result($paket),
            "panitia" => $panitia,
            "jadwal" => $jadwal,
            "peserta" => $peserta,
            "hasil_evaluasi" => $hasil_evaluasi,
            "pemenang" => $pemenang
        ];
        $paket_dokumen = PaketDokumen::select('paket_dokumen.*', 'dokumen.nama')->join('dokumen', 'paket_dokumen.id_dokumen', '=', 'dokumen.id')->where('pkt_id', $paket->pkt_id)->orderBy('dokumen.id', 'asc')->get();
        //return [$pemenang];
        return view('monep.info', compact('paket', 'jadwal', 'peserta', 'pemenang', 'panitia', 'hasil_evaluasi', 'dokumen', 'paket_dokumen', 'persiapan'));
        return $return;
    }
    public function show_result($data)
    {
        $data->metode_pemilihan = MetodePemilihan::find($data->mtd_pemilihan);
        $data->kategori_lelang = KategoriLelang::find($data->kgr_id);
        $data->rp_pagu = number_format($data->pkt_pagu, 2, ',', '.');
        $data->status_lelang = StatusLelang::find($data->lls_status);
        $ulang_label = ($data->kgr_id == 1) ? "Seleksi Ulang" : "Tender Ulang";
        $data->ulang = ($data->lls_versi_lelang > 1) ? $ulang_label : false;
        $data->spse_versi = "";
        if ($data->pkt_flag == 3) {
            $data->spse_versi = "SPSE 4.3";
        } else if ($data->pkt_flag == 2) {
            $data->spse_versi = "SPSE 4.2";
        } else if ($data->pkt_flag < 2) {
            $data->spse_versi = "SPSE 3";
        }
        $data->tahap = Tahap::toString($data);
        $data->metode = Metode::find($data->mtd_id);
        $data->ppk = Fungsi::get_ppk($data->pkt_id);
        return $data;
    }

    // SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=?
    //findPenetapanPemenang => SELECT eva_id FROM evaluasi WHERE lls_id=? AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=?) AND eva_jenis = 4;
    // isSelesai >> eva_status = 1 (0 berati sedang evaluasi)
    //findByWithNevUrut => SELECT FROM nilai_evaluasi WHERE eva_id= ? ORDER BY nev_urutan ASC
    // hargaFinal if(!nev_harga_negosiasi) {nev_harga_terkoreksi}
    //calonPemenang => SELECT * FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id = ? AND psr.is_pemenang = ?
    //calonPemenangTenderCepat/seleksiCepat => SELECT * FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id = ? AND psr.is_pemenang_verif = ?


}
