<?php

namespace App\Http\Controllers\Epns;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sibaja\Paket;
use App\Models\Sibaja\PaketDokumen;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use App\Models\Sibaja\PaketPanitia;
use App\Models\Epns\Metode;
use App\Models\Epns\Fungsi;

class PdfController extends Controller
{
    public function __construct()
    {
        $this->dokumen = collect([
            0 => array(1, 2, 3, 4, 7, 8, 9, 10),
            1 => array(1, 2, 3, 4, 7, 8),
            2 => array(1, 2, 3, 4, 5, 6, 7, 8),
            3 => array(1, 2, 3, 4, 7, 8, 9, 10)
        ]);
        // mtd_id
        $this->metode = collect([
            array("id" => 1, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 19, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 2, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 4, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 3, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua Tahap"),
            array("id" => 41, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 51, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 42, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 46, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 47, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 44, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Dua Tahap"),
            array("id" => 48, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 50, "metode_kualifikasi" => "Prakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 52, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 53, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Dua File"),
            array("id" => 54, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Satu File"),
            array("id" => 55, "metode_kualifikasi" => "Pascakualifikasi", "metode_dokumen" => "Dua File")
        ]);
    }
    public function dokumen($id)
    {
        $paket = Paket::with(['dokumen', 'jenis', 'sumber_dana'])->where('lls_id', $id)->first();
        $query = DB::connection('epns')->table('paket_panitia as pp')
            ->select('p.pnt_id', 'p.pnt_nama', 'pp.auditupdate')
            ->leftJoin('panitia as p', 'p.pnt_id', '=', 'pp.pnt_id')
            ->orderBy('pp.auditupdate', 'desc');
        $paket->panitia = $query->where('pp.pkt_id', $paket->pkt_id)->first();
        $pdf = PDF::loadView('sibaja/epns/pdf/dokumen', compact('paket'))->setPaper('legal', 'potrait');
        //return view('sibaja/epns/pdf/badokumen', compact('paket'));
        return $pdf->stream();
    }
    public function spt($id)
    {
        $paket = Paket::with(['surat', 'jenis', 'sumber_dana'])->findOrFail($id);
        $query = DB::connection('epns')->table('paket_panitia as pp')
            ->select('p.pnt_id', 'p.pnt_nama', 'pp.auditupdate')
            ->leftJoin('panitia as p', 'p.pnt_id', '=', 'pp.pnt_id')
            ->orderBy('pp.auditupdate', 'desc');
        $paket->panitia = $query->where('pp.pkt_id', $paket->pkt_id)->first();
        $paket->panitia->spt = PaketPanitia::where('pkt_id', $paket->pkt_id)->orderBy('tgl_buat', 'desc')->first();
        if (!$paket->panitia->spt) {
            $flash = [
                'error' => true,
                'pesan' => 'Nomor surat tugas belum dibuat!'
            ];
            return redirect()->route('epns.lelang.info', $id)->with('flash', $flash);
        }
        $paket->panitia->anggota = DB::connection('epns')->table('anggota_panitia as ap')
            ->select('p.peg_nip', 'p.peg_nama', 'p.peg_golongan', 'p.peg_pangkat')
            ->leftJoin('pegawai as p', 'p.peg_id', '=', 'ap.peg_id')
            ->where('ap.pnt_id', $paket->panitia->pnt_id)
            ->get();
        // PDF::setOptions(['defaultPaperSize' => 'f4']);
        $pdf = PDF::loadView('sibaja/epns/pdf/spt', compact('paket'))->setPaper('legal', 'potrait');
        // return $pdf->download('spt.pdf'); // untuk direct download 
        return $pdf->stream(); // preview in browser
    }
    public function rapat_persiapan($id)
    {
        $paket = Paket::with(['dokumen', 'jenis', 'sumber_dana', 'persiapan'])->whereHas('persiapan', function ($q) use ($id) {
            return $q->where('id', $id);
        })->first();
        if ($paket) {
            $query = DB::connection('epns')->table('paket_panitia as pp')
                ->select('p.pnt_id', 'p.pnt_nama', 'pp.auditupdate')
                ->leftJoin('panitia as p', 'p.pnt_id', '=', 'pp.pnt_id')
                ->orderBy('pp.auditupdate', 'desc');
            $paket->panitia = $query->where('pp.pkt_id', $paket->pkt_id)->first();
            $paket->panitia->spt = PaketPanitia::where('pkt_id', $paket->pkt_id)->orderBy('tgl_buat', 'desc')->first();
            $paket->panitia->anggota = DB::connection('epns')->table('anggota_panitia as ap')
                ->select('p.peg_nip', 'p.peg_nama', 'p.peg_golongan', 'p.peg_pangkat')
                ->leftJoin('pegawai as p', 'p.peg_id', '=', 'ap.peg_id')
                ->where('ap.pnt_id', $paket->panitia->pnt_id)
                ->get();
            return $paket;
            $pdf = PDF::loadView('sibaja/epns/pdf/persiapan', compact('paket'))->setPaper('a4', 'potrait');
            //return view('sibaja/epns/pdf/persiapan', compact('paket'));
            return $pdf->stream();
        }
    }
    public function hasil($id)
    {
        $paket = Paket::with(['persiapan'])->findOrFail($id);
        $paket->metode = Metode::find($paket->mtd_id);
        $query = DB::connection('epns')->table('paket_panitia as pp')
            ->select('p.pnt_id', 'p.pnt_nama', 'pp.auditupdate')
            ->leftJoin('panitia as p', 'p.pnt_id', '=', 'pp.pnt_id')
            ->orderBy('pp.auditupdate', 'desc');
        $paket->panitia = $query->where('pp.pkt_id', $paket->pkt_id)->first();
        $paket->ppk = Fungsi::get_ppk($paket->pkt_id);
        $paket->panitia->spt = PaketPanitia::where('pkt_id', $paket->lls_id)->orderBy('tgl_buat', 'desc')->first();
        return $paket;
        if ($paket->panitia->spt == null) {
            $flash = [
                'error' => true,
                'pesan' => 'Nomor surat tugas belum dibuat!'
            ];
            return redirect()->route('epns.lelang.info', $id)->with('flash', $flash);
        }
        $paket->panitia->anggota = DB::connection('epns')->table('anggota_panitia as ap')
            ->select('p.peg_nip', 'p.peg_nama', 'p.peg_golongan', 'p.peg_pangkat')
            ->leftJoin('pegawai as p', 'p.peg_id', '=', 'ap.peg_id')
            ->where('ap.pnt_id', $paket->panitia->pnt_id)
            ->get();
        $paket->pemenang = Fungsi::get_pemenang($id);
        //return $paket;
        $pdf = PDF::loadView('sibaja/epns/pdf/hasil', compact('paket'))->setPaper('a4', 'potrait');
        return view('sibaja/epns/pdf/hasil', compact('paket'));
        return $pdf->stream(); // preview in browser
    }
    public function pemenang($id)
    {
        # PRINT
    }

    public function filter_dokumen($data)
    {
        $filtered = $this->dokumen->filter(function ($value, $key) use ($data) {
            return ($key == $data->kgr_id);
        });
        return $filtered->first();
    }
    public function get_pemenang($paket)
    {
        $select = "nev_lulus,nev_urutan,nev_harga,nev_harga_terkoreksi,nev_harga_negosiasi,is_pemenang,is_pemenang_verif,psr.psr_id,psr_harga,psr_harga_terkoreksi,(SELECT rkn_nama FROM rekanan WHERE rkn_id=psr.rkn_id) as rkn_nama, (SELECT rkn_alamat FROM rekanan WHERE rkn_id=psr.rkn_id) as rkn_alamat, (SELECT rkn_npwp FROM rekanan WHERE rkn_id=psr.rkn_id) as rkn_npwp";
        $pemenang = null;
        $mtd_pemilihan = $paket->mtd_pemilihan['id'];
        if ($mtd_pemilihan == 9 || $mtd_pemilihan == 10) {
            $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang_verif =:is_pemenang", ["lls_id" => $paket->lls_id, "is_pemenang" => 1]);
        }
        if ($pemenang == null) {
            $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang =:is_pemenang", ["lls_id" => $paket->lls_id, "is_pemenang" => 1]);
        }
        if (sizeof($pemenang) > 1) {
            // klo perlu tambah and nev_urutan = 1
            if ($mtd_pemilihan == 9 || $mtd_pemilihan == 10) {
                $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang_verif =:is_pemenang", ["lls_id" => $paket->lls_id, "is_pemenang" => 1]);
            } else {
                $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang =:is_pemenang", ["lls_id" => $paket->lls_id, "is_pemenang" => 1]);
            }
        }
        return ($pemenang) ? $pemenang[0] : $pemenang;
    }
}
