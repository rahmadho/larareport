<?php

namespace App\Http\Controllers\Epns;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Epns\Tahap;
use App\Models\Epns\MetodePemilihan;
use App\Models\Epns\KategoriLelang;
use App\Models\Epns\StatusLelang;
use App\Models\Sibaja\Paket;
use App\Models\Epns\Metode;

class ApiController extends Controller
{
    public function __construct()
    { }
    public function datatable_paket(Request $request)
    {
        $tahun = $request->input('tahun');
        $search = $request->input('filter');
        $has_pokja = $request->input('has_pokja');
        $columns = array(
            0 => 'pkt_id',
            1 => 'lls_id',
            2 => 'pkt_nama',
            3 => 'pkt_tgl_buat',
            4 => 'stk_nama',
            5 => 'pkt_pagu',
            6 => 'pkt_flag',
            7 => 'pkt_status',
            8 => 'tahaps',
            9 => 'tahun_anggaran',
            10 => 'sumber_dana',
        );
        $q = DB::connection('epns')->table('paket as p')
            ->selectRaw("COUNT(*) as jml")
            ->leftJoin('satuan_kerja as s', 's.stk_id', '=', 'p.stk_id')
            ->leftJoin('lelang_seleksi as lls', 'lls.pkt_id', '=', 'p.pkt_id')
            ->leftJoin('panitia as pn', function ($join) {
                $join->on('pn.pnt_id', '=', 'p.pnt_id');
            })
            //->whereRaw('pkt_hps_enable = true')
            ->whereRaw('pkt_hps > 0')
            ->whereRaw('lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=p.pkt_id)')
            ->whereRaw("(SELECT distinct ang.ang_tahun FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) = :tahun", ["tahun" => $tahun]);
        if ($has_pokja == 'true') {
            $q->where('pnt_nama', '<>', null);
        }
        $q = $q->first();
        $totalData = $q->jml;
        $totalFiltered = $totalData;

        $limit = ($request->has('length')) ? $request->input('length') : 10;
        $start = ($request->has('start')) ? $request->input('start') : 0;

        $query = DB::connection('epns')->table('paket as p')
            ->selectRaw("p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, pkt_pagu, pkt_status, pkt_flag, pkt_status, lls_id, mtd_pemilihan, tahap_now(lls_id, localtimestamp) AS tahaps, lls_versi_lelang, lls_status")
            ->leftJoin('satuan_kerja as s', 's.stk_id', '=', 'p.stk_id')
            ->leftJoin('lelang_seleksi as lls', 'lls.pkt_id', '=', 'p.pkt_id')
            ->leftJoin('panitia as pn', function ($join) {
                $join->on('pn.pnt_id', '=', 'p.pnt_id');
                //->whereRaw('lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=p.pkt_id)');
            });
        // Filter paket selain paket yang ditutup 
        $query->whereRaw('lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=p.pkt_id)');
        // tambah filter: yang HPS nya di isi
        //$query->whereRaw('pkt_hps_enable = true');
        $query->whereRaw('pkt_hps > 0');
        // group by rup_id nya sama, ambil tgl terakhir
        // Satker
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct s.stk_id), ', ') FROM satuan_kerja s, paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_id"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama"));
        //->whereRaw("tahap_now(lls_id, localtimestamp) = 'BELUM_DILAKSANAKAN'")
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct rup_id), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as rup_id"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.ang_tahun), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as tahun_anggaran"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.sbd_id), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as sumber_dana"));
        $query->addSelect(DB::raw("p.pkt_hps, lls.mtd_id, lls.mtd_pemilihan, lls.mtd_evaluasi, kgr_id, paket_instansi(p.pkt_id) as nama_instansi, lls_penawaran_ulang,(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) as eva_versi"));
        $query->addSelect('lls_dibuat_tanggal', 'p.pnt_id', 'pn.pnt_nama');
        if ($has_pokja == 'true') {
            $query->where('pnt_nama', '<>', null);
        }
        if (!empty($search)) {
            $query = $query->where(function ($q) use ($search) {
                return $q->whereRaw("LOWER(p.pkt_nama) LIKE LOWER('%{$search}%')")
                    ->orWhereRaw("LOWER(stk_nama) LIKE LOWER('%{$search}%')")
                    ->orWhereRaw("lls.lls_id::text LIKE '%{$search}%'")
                    ->orWhereRaw("p.pkt_id::text LIKE '%{$search}%'");
            });
        }
        //->orderBy($order, $dir);
        $query->whereRaw("(SELECT distinct(ang.ang_tahun) FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) = :tahun", ["tahun" => $tahun]);
        //return $result = $query->offset($start)->limit($limit)->orderBy('pkt_tgl_buat', 'desc')->orderBy('lls_versi_lelang', 'desc')->toSql();
        $totalFiltered = $query->count();
        $result = $query->offset($start)->limit($limit)->orderBy('pkt_tgl_buat', 'desc')->orderBy('lls_versi_lelang', 'desc')->get();
        $data = array();
        $paket_sibaja = Paket::all();
        if (!empty($result)) {
            foreach ($result as $res) {
                if ($paket_sibaja->where('lls_id', $res->lls_id)->first()) {
                    $res->on_sibaja = true;
                } else {
                    $res->on_sibaja = false;
                }
                $data[] = $this->show_result($res);
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return $json_data;
    }

    public function evaluasi(Request $request)
    { }

    public function lelang_tayang()
    {
        $tahun = 2011;
        // query lelang
        $query = DB::connection('epns')->table('paket as p')
            ->selectRaw("l.*, pkt_pagu, (SELECT DISTINCT lls_tgl_setuju FROM lelang_seleksi WHERE lls_id=l.lls_id) as lls_tgl_setuju")
            ->leftJoin('lelang_query as l', 'l.pkt_id', '=', 'p.pkt_id');
        $query->addSelect(DB::raw("CASE WHEN (mtd_pemilihan=9 OR mtd_pemilihan=10) AND (SELECT COUNT(*) FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=l.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=l.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1) > 0 THEN (SELECT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi WHEN (nev_harga >0 AND nev_harga NOTNULL) THEN nev_harga ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=l.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=l.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1 LIMIT 1) ELSE (SELECT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi WHEN (nev_harga >0 AND nev_harga NOTNULL) THEN nev_harga ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=l.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=l.lls_id) AND eva_jenis=4) AND psr.is_pemenang=1 AND nev_urutan=1 LIMIT 1) END as pemenang_harga"));
        $query->addSelect(DB::raw("CASE WHEN (mtd_pemilihan=9 OR mtd_pemilihan=10) AND (SELECT COUNT(*) FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=l.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=l.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1) > 0 THEN (SELECT (SELECT rkn_nama FROM rekanan WHERE rkn_id=psr.rkn_id) FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=l.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=l.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1 LIMIT 1) ELSE (SELECT (SELECT rkn_nama FROM rekanan WHERE rkn_id=psr.rkn_id) FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=l.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=l.lls_id) AND eva_jenis=4) AND psr.is_pemenang=1 AND nev_urutan=1 LIMIT 1) END as pemenang_rekanan"));
        $query->where('l.lls_status', 1);
        $query->where('l.anggaran', $tahun);
        return $results = $query->orderBy('pkt_tgl_buat', 'desc')->toSql();
        foreach ($results as $key => $paket) {
            $results[$key]->metode = Metode::find($paket->mtd_id);
            $results[$key]->metode_pemilihan = MetodePemilihan::find($paket->mtd_pemilihan);
            $results[$key]->kategori = KategoriLelang::find($paket->kgr_id);
        }
        $response = [
            "paket_has_pemenang" => $results->where('pemenang_harga', '<', 1)->where('pemenang_rekanan', '<>', null)->count(),
            "paketnya" => $results->where('pemenang_harga', '<', 1)->where('pemenang_rekanan', '<>', null)->all(),
            //"lelang" => $results
        ];
        return $response;
    }

    public function stats()
    {
        $query = DB::connection('epns')->table('paket as p')
            ->selectRaw("p.pkt_id,lls_id,pkt_nama,p.kgr_id,pkt_tgl_buat,pkt_pagu,p.pkt_hps,pkt_status,pkt_flag")
            ->leftJoin('satuan_kerja as s', 's.stk_id', '=', 'p.stk_id')
            ->leftJoin('lelang_seleksi as lls', 'lls.pkt_id', '=', 'p.pkt_id');
        $query->addSelect(DB::raw("mtd_pemilihan,lls.mtd_id,lls_versi_lelang,lls_status,lls_tgl_setuju"));
        $query->addSelect(DB::raw("paket_instansi(p.pkt_id) as nama_instansi,tahap_now(lls_id, localtimestamp) AS tahaps"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.sbd_id), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as sumber_dana"));
        $pemenang_harga_verif = "(SELECT DISTINCT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1 LIMIT 1)";
        $pemenang_harga = "(SELECT DISTINCT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang=1 AND nev_lulus=1 LIMIT 1)";
        $has_verif = "(SELECT DISTINCT psr.rkn_id FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1 LIMIT 1)";
        $pemenang_rekanan_verif = "(SELECT DISTINCT (SELECT rkn_nama FROM rekanan WHERE rkn_id=psr.rkn_id) FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1 LIMIT 1)";
        $pemenang_rekanan = "(SELECT DISTINCT (SELECT rkn_nama FROM rekanan WHERE rkn_id=psr.rkn_id) FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang=1 AND nev_urutan=1 AND nev_lulus=1 LIMIT 1)";
        $query->addSelect(DB::raw("CASE WHEN (mtd_pemilihan=9 OR mtd_pemilihan=10) AND {$has_verif} > 0 THEN {$pemenang_harga_verif} ELSE {$pemenang_harga} END as pemenang_harga"));
        $query->addSelect(DB::raw("CASE WHEN (mtd_pemilihan=9 OR mtd_pemilihan=10) AND {$has_verif} > 0 THEN {$pemenang_rekanan_verif} ELSE {$pemenang_rekanan} END as pemenang_rekanan"));
        $query->where('lls_status', 1);
        $query->whereRaw("lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=p.pkt_id)");
        //$query->whereRaw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.ang_tahun), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id)='2019'");
        //$query->whereRaw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.sbd_id), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id)='APBD'");
        //return $query->orderBy('pkt_tgl_buat', 'desc')->toSql();
        $results = $query->orderBy('pkt_tgl_buat', 'desc')->get();
        //$results = DB::connection('epns')->select("select p.pkt_id,lls_id,pkt_nama,p.kgr_id,pkt_tgl_buat,pkt_pagu,p.pkt_hps,pkt_status,pkt_flag,mtd_pemilihan,lls.mtd_id,lls_versi_lelang,lls_status, paket_instansi(p.pkt_id) as nama_instansi,tahap_now(lls_id, localtimestamp) AS tahaps, (SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama, (SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.sbd_id), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id) as sumber_dana, CASE WHEN (mtd_pemilihan=9 OR mtd_pemilihan=10) AND (SELECT DISTINCT psr.rkn_id FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1) > 0 THEN (SELECT DISTINCT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1) ELSE (SELECT DISTINCT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang=1 AND nev_lulus=1) END as pemenang_harga, CASE WHEN (mtd_pemilihan=9 OR mtd_pemilihan=10) AND (SELECT DISTINCT psr.rkn_id FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1) > 0 THEN (SELECT DISTINCT (SELECT rkn_nama FROM rekanan WHERE rkn_id=psr.rkn_id) FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1) ELSE (SELECT DISTINCT (SELECT rkn_nama FROM rekanan WHERE rkn_id=psr.rkn_id) FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang=1 AND nev_urutan=1 AND nev_lulus=1) END as pemenang_rekanan from paket as p left join satuan_kerja as s on s.stk_id = p.stk_id left join lelang_seleksi as lls on lls.pkt_id = p.pkt_id where lls_status = 1 and lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=p.pkt_id) order by pkt_tgl_buat desc;");
        $response = [
            "count_paket" => count($results)
        ];
        return $response;
    }

    public static function show_result($data)
    {
        $data->metode_pemilihan = MetodePemilihan::find($data->mtd_pemilihan);
        $data->kategori_lelang = KategoriLelang::find($data->kgr_id);
        $data->rp_pagu = number_format($data->pkt_pagu, 2, ',', '.');
        $data->rp_hps = number_format($data->pkt_hps, 2, ',', '.');
        $data->status_lelang = StatusLelang::find($data->lls_status);
        $ulang_label = ($data->kgr_id == 1) ? "Seleksi Ulang" : "Tender Ulang";
        $data->ulang = ($data->lls_versi_lelang > 1) ? $ulang_label : false;
        $data->spse_versi = "";
        if ($data->pkt_flag == 3) {
            $data->spse_versi = "SPSE 4.3";
        } else if ($data->pkt_flag == 2) {
            $data->spse_versi = "SPSE 4.2";
        } else if ($data->pkt_flag < 2) {
            $data->spse_versi = "SPSE 3";
        }
        $data->tahap = Tahap::toString($data);
        return $data;
    }
}
