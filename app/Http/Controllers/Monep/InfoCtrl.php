<?php

namespace App\Http\Controllers\Monep;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Epns\Fungsi;
use App\Models\Epns\KategoriLelang;
use App\Models\Epns\Metode;
use App\Models\Epns\MetodePemilihan;
use App\Models\Epns\StatusLelang;
use App\Models\Epns\Tahap;
use App\Models\Monep\LelangSeleksi;
use App\Models\Monep\PaketDokumen as PaketDokumenTable;
use App\Models\Epns\PaketDokumen;
use App\Models\Monep\Panitia;
use App\Models\Monep\Rekanan;
use App\Models\Monep\Dokumen;
use App\Models\Monep\Paket;
use App\Models\Monep\Pegawai;

class InfoCtrl extends Controller
{
    public function __construct(Request $req)
    {
        $this->tahun_now = 2019;
        $this->stk_ukpbj = 1805016;
        $this->request = $req;
    }
    public function lelang_info($id = null)
    {
        $lls_id = ($this->request->input('lls_id')) ? $this->request->input('lls_id') : $id;
        $query = LelangSeleksi::with([
            'paket' => function ($q) {
                $q->select(['pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_status', 'pkt_flag', 'kgr_id', 'rup_id', 'sbd_id', 'ang_tahun', 'ppk_nip', 'pnt_id', 'stk_id', 'pkt_tgl_buat']);
            }, 'paket.ppk' => function ($q) {
                $q->select(['peg_nip', 'peg_nama']);
            }, 'paket.panitia' => function ($q) {
                $q->select(['pnt_id', 'pnt_nama']);
            }, 'paket.satker' => function ($q) {
                $q->select(['stk_id', 'stk_nama', 'instansi_id']);
            }, 'jadwal', 'aktivitas', 'tgl_penetapan_pemenang', 'paket.panitia.pegawai', 'paket.satker.instansi', 'paket.surat', 'paket.spt.panitia', 'paket.persiapan'
        ]);
        $query->select(DB::raw("*,tahap_now(lls_id, localtimestamp) as tahap_now"));
        $query->where('lls_id', $lls_id);
        $result = $query->first();
        if (!$result) {
            abort(404);
        }
        $result->metode_pemilihan = MetodePemilihan::find($result->mtd_pemilihan);
        $result->kategori_lelang = KategoriLelang::find($result->paket->kgr_id);
        $result->rp_pagu = number_format($result->paket->pkt_pagu, 2, ',', '.');
        $result->rp_hps = number_format($result->paket->pkt_hps, 2, ',', '.');
        $result->status_lelang = StatusLelang::find($result->lls_status);
        $ulang_label = ($result->paket->kgr_id == 1) ? "Seleksi Ulang" : "Tender Ulang";
        $result->ulang = ($result->lls_versi_lelang > 1) ? $ulang_label : false;
        $result->spse_versi = "";
        if ($result->paket->pkt_flag == 3) {
            $result->spse_versi = "SPSE 4.3";
        } else if ($result->paket->pkt_flag == 2) {
            $result->spse_versi = "SPSE 4.2";
        } else if ($result->paket->pkt_flag < 2) {
            $result->spse_versi = "SPSE 3";
        }
        $result->on_sibaja = false;
        $paket_doc = PaketDokumen::get_key($result->paket->kgr_id);
        $result->dokumen = Dokumen::whereIn('id', $paket_doc)->get();
        $result->metode = Metode::find($result->mtd_id);
        $result->selesai = $result->jadwal->where('thp_id', 18817)->first();
        foreach ($result->dokumen as $key => $doc) {
            $adaTidak = PaketDokumenTable::where('pkt_id', $result->paket->pkt_id)->where('id_dokumen', $doc['id'])->first();
            $result->dokumen[$key]->ada = ($adaTidak && $adaTidak->is_ada) ? true : false;
        }
        foreach ($result->aktivitas as $key2 => $tahap) {
            $result->aktivitas[$key2]->keterangan = Tahap::find($tahap->akt_jenis);
        }
        $result->tahap_now = Tahap::find($result->tahap_now);
        $result->pemenang = ($result->pemenang) ? json_decode($result->pemenang) : null;
        if ($result->pemenang) {
            $result->pemenang->rekanan = Rekanan::with(['bentuk_usaha', 'kabupaten'])->select('rkn_nama', 'rkn_alamat', 'rkn_npwp', 'btu_id', 'kbp_id')->where('rkn_id', $result->pemenang->rkn_id)->first();
        }
        $panitia = Panitia::with('pegawai')->where('is_active', -1)->where('stk_id', $this->stk_ukpbj)->where('pnt_tahun', $this->tahun_now)->orderBy('pnt_id', 'asc')->get();

        // diambil dari source
        $peserta = Fungsi::peserta($lls_id);
        $hasil_evaluasi = Fungsi::hasil_evaluasi($lls_id);

        //return view('monep.pdf.paket', compact('result', 'panitia', 'peserta', 'hasil_evaluasi')); // $result; //
        return view('monep.info', compact('result', 'panitia', 'peserta', 'hasil_evaluasi')); // $result; //
    }

    public function paket_history($id = null)
    {
        $history = Paket::with(['lelangs', 'satker.instansi'])->find($id);
        if ($history->pkt_flag == 3) {
            $history->spse_versi = "SPSE 4.3";
        } else if ($history->paket->pkt_flag == 2) {
            $history->spse_versi = "SPSE 4.2";
        } else {
            $history->spse_versi = "SPSE 3";
        }
        $history->kategori_lelang = KategoriLelang::find($history->kgr_id);
        $history->rp_pagu = number_format($history->pkt_pagu, 2, ',', '.');
        $history->rp_hps = number_format($history->pkt_hps, 2, ',', '.');
        $ulang_label = ($history->kgr_id == 1) ? "Seleksi Ulang" : "Tender Ulang";
        foreach ($history->lelangs as $row) {
            $row->metode_pemilihan = MetodePemilihan::find($row->mtd_pemilihan);
            $row->status_lelang = StatusLelang::find($row->lls_status);
            $row->ulang = ($row->lls_versi_lelang > 1) ? $ulang_label : false;
            $row->metode = Metode::find($row->mtd_id);
        }
        $history->ppk = Pegawai::find($history->ppk_peg_id);
        $result = $history;
        return view('monep.paket.history', compact('result')); // $result; //
    }

    public function panitia_info($id)
    {
        $panitia = Panitia::with('pegawai')->where('is_active', -1)->where('pnt_id', $id)->first();
        return view('monep.panitia.info', compact('panitia'));
    }
}
