<?php

namespace App\Http\Controllers\Monep;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Monep\RouteController;
use Illuminate\Support\Facades\DB;

class MenuCtrl extends Controller
{
    public function __contruct(Request $req)
    {
        $this->request = $req;
    }
    public function index()
    {
        $result = RouteController::orderByRaw('urut nulls last')->get();
        $no = 1;
        return view('monep.menu.index', compact('result', 'no'));
    }
    public function create()
    {
        return view('monep.menu.add');
    }
    public function edit($id)
    {
        $query = RouteController::where('id', $id);
        $query->select(DB::raw('* , array_to_json(role) as roles'));
        $result = $query->first();
        $roles = json_decode($result->roles);
        return view('monep.menu.edit', compact('result', 'roles'));
    }
    public function store(Request $req)
    {
        $req->validate([
            'req_method' => 'required',
            'slug' => 'required',
            'controller' => 'required',
            'method' => 'required',
            'route_name' => 'required',
            // 'label' => 'required',
            // 'icon' => 'required',
            // 'urut' => 'required',
            'role' => 'required',
            //'is_menu' 
            //'active'
        ]);
        //return '[' . implode($req->role, ',') . ']';
        $route = new RouteController();
        $route->request_method = $req->req_method;
        $route->slug = $req->slug;
        $route->route_controller = $req->controller;
        $route->controller_method = $req->method;
        $route->route = $req->route_name;
        $route->urut = $req->urut;
        $route->is_menu = ($req->has('is_menu')) ? true : false;
        $route->label = $req->label;
        $route->icon = $req->icon;
        $route->role = '{' . implode(',', $req->role) . '}';
        //$route->controller_name = $req->input->controller_name;
        $route->active = ($req->has('active')) ? true : false;
        if ($route->save()) {
            $flash = [
                'error' => false,
                'pesan' => 'Berhasil melakukan update data menu!'
            ];
        } else {
            $flash = [
                'error' => true,
                'pesan' => 'Gagal melakukan update data menu!'
            ];
        }
        return redirect()->route('monep.menu.index')->with('flash', $flash);
    }
    public function update(Request $req, $id)
    {
        $req->validate([
            'req_method' => 'required',
            'slug' => 'required',
            'controller' => 'required',
            'method' => 'required',
            'route_name' => 'required',
            'role' => 'required'
        ]);
        $route = RouteController::findOrFail($id);
        $route->request_method = $req->req_method;
        $route->slug = $req->slug;
        $route->route_controller = $req->controller;
        $route->controller_method = $req->method;
        $route->route = $req->route_name;
        $route->urut = $req->urut;
        $route->is_menu = ($req->has('is_menu')) ? true : false;
        $route->label = $req->label;
        $route->icon = $req->icon;
        $route->role = '{' . implode(',', $req->role) . '}';
        $route->active = ($req->has('active')) ? true : false;
        if ($route->save()) {
            $flash = [
                'error' => false,
                'pesan' => 'Berhasil melakukan update data menu!'
            ];
        } else {
            $flash = [
                'error' => true,
                'pesan' => 'Gagal melakukan update data menu!'
            ];
        }
        return redirect()->route('monep.menu.index')->with('flash', $flash);
    }
    public function destroy($id)
    {
        $route = RouteController::findOrFail($id);
        if ($route->delete()) {
            $flash = [
                'error' => false,
                'pesan' => 'Berhasil melakukan update data menu!'
            ];
        } else {
            $flash = [
                'error' => true,
                'pesan' => 'Gagal melakukan update data menu!'
            ];
        }
        return redirect()->route('monep.menu.index')->with('flash', $flash);
    }
}
