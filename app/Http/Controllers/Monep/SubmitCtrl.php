<?php

namespace App\Http\Controllers\Monep;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\Monep\Surat;
use App\Models\Epns\PaketDokumen;
use App\Models\Monep\Paket;
use App\Models\Monep\PaketDokumen as PaketDokumenTable;
use App\Models\Monep\PaketSpt;
use App\Models\Monep\PaketPersiapan;
use App\Models\Monep\Pegawai;
use Illuminate\Support\Facades\DB;

class SubmitCtrl extends Controller
{
    private $request;
    private $response;
    private $flash;
    private $validator;
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->response = true;
        $this->flash = null;
        $this->validator = null;
    }
    public function run($run)
    {
        if (method_exists($this, $run)) {
            return $this->$run();
            // return $this->result;
        } else {
            abort(404);
        }
    }
    public function paket_masuk()
    {
        $validator = Validator::make($this->request->all(), [
            'pkt_id' => 'required|numeric',
            'is_ada' => 'required|array',
            'is_ada.*' => 'required',
            'no_surat' => 'required',
            'tgl_surat' => 'required',
            'perihal' => 'required',
            'tgl_input' => 'required'
        ]);
        $this->dokumen_store();
        $this->surat_store();
        if ($validator->fails()) {
            $flash = [
                'error' => true,
                'pesan' => 'Tidak dapat memproses datam, input tidak lengkap!'
            ];
            return redirect()->back()->withInput()->withErrors($validator)->with('flash', $flash);
        }
        if (!$this->response) {
            return redirect()->back()->withInput()->withErrors($validator)->with('flash', $this->flash);
        } else {
            return redirect()->route('monep.lelang.info', $this->request->lls_id)
                ->with('flash', $this->flash);
        }
    }
    public function dokumen_store()
    {
        $validator = Validator::make($this->request->all(), [
            'pkt_id' => 'required|numeric',
            'is_ada' => 'required|array',
            'is_ada.*' => 'required'
        ]);
        $paket = Paket::find($this->request->pkt_id);
        if ($validator->fails()) {
            $flash = [
                'error' => true,
                'pesan' => 'Tidak dapat memproses data, input tidak lengkap!'
            ];
            $this->response = false;
            $this->flash = $flash;
            $this->validator = $validator;
            return redirect()->back()->withInput()->withErrors($validator)->with('flash', $flash);
        }
        $paket_doc = PaketDokumen::get_key($paket->kgr_id);
        // return [
        //     'lengkapanya' => $paket_doc,
        //     'inputan' => $this->request->is_ada
        // ];
        try {
            $trans = DB::transaction(function () use ($paket_doc) {
                foreach ($paket_doc as $doc) {
                    $ada = isset(($this->request->is_ada[$doc])) ? true : false;
                    PaketDokumenTable::updateOrCreate(['pkt_id' => $this->request->pkt_id, 'id_dokumen' => $doc], ['is_ada' => $ada, 'tgl_buat' => date('Y-m-d H:i:s')]);
                }
            });
            $flash = [
                'error' => false,
                'pesan' => 'Dokumen berhasil di verifikasi! '
            ];
        } catch (\Exception $e) {
            $flash = [
                'error' => true,
                'pesan' => 'Tidak dapat memproses data, ulangi beberapa saat lagi! (' . $e . ')'
            ];
        }

        $this->response = true;
        $this->flash = $flash;
        $this->validator = $validator;
        return redirect()->route('monep.lelang.info', $this->request->lls_id)
            ->with('flash', $flash);
    }
    public function surat_store()
    {
        $validator = Validator::make($this->request->all(), [
            'pkt_id' => 'required|numeric',
            'no_surat' => 'required',
            'tgl_surat' => 'required',
            'perihal' => 'required',
            'tgl_input' => 'required'
        ]);
        if ($validator->fails()) {
            $flash = [
                'error' => true,
                'pesan' => 'Tidak dapat memproses data, input tidak valid!'
            ];
            $validator->errors()->add('create-paket-modal', 'show');
            $this->response = false;
            $this->flash = $flash;
            $this->validator = $validator;
            return redirect()->route('monep.lelang.info', $this->request->lls_id)
                ->withInput()
                ->withErrors($validator)
                ->with('flash', $flash);
        } else {
            $surat = new Surat;
            $surat->no_surat = $this->request->no_surat;
            $surat->pkt_id = $this->request->pkt_id;
            $surat->tgl_surat = $this->request->tgl_surat;
            $surat->perihal = $this->request->perihal;
            $surat->tgl_input = $this->request->tgl_input; //date('Y-m-d H:i:s');
            if ($surat->save()) {
                $flash = [
                    'error' => false,
                    'pesan' => 'Berhasil memproses data!'
                ];
            } else {
                $flash = [
                    'error' => true,
                    'pesan' => 'Tidak dapat memproses data, ulangi beberapa saat lagi!'
                ];
            }
            $this->response = true;
            $this->flash = $flash;
            $this->validator = $validator;
            return redirect()->route('monep.lelang.info', $this->request->lls_id)
                ->with('flash', $flash);
        }
    }
    public function spt_store()
    {
        $validator = Validator::make($this->request->all(), [
            'lls_id' => 'required|numeric',
            'pkt_id' => 'required|numeric',
            'pnt_id' => 'required|numeric',
            'no_spt' => 'required|numeric',
            'tgl_input' => 'required',
        ]);
        if ($validator->fails()) {
            $flash = [
                'error' => true,
                'pesan' => 'Tidak dapat memproses data, input tidak valid!'
            ];
            $validator->errors()->add('create-paket-spt', 'show');
            return redirect()->route('monep.lelang.info', $this->request->lls_id)
                ->withInput()
                ->withErrors($validator)
                ->with('flash', $flash);
        } else {
            $query = PaketSpt::updateOrCreate(['no_spt' => $this->request->no_spt, 'pkt_id' => $this->request->pkt_id], ['pnt_id' => $this->request->pnt_id, 'tgl_input' => $this->request->tgl_input]);
            if ($query) {
                $flash = [
                    'error' => false,
                    'pesan' => 'Berhasil memproses data!'
                ];
            } else {
                $flash = [
                    'error' => true,
                    'pesan' => 'Tidak dapat memproses data, ulangi beberapa saat lagi!'
                ];
            }
            return redirect()->route('monep.lelang.info', $this->request->lls_id)
                ->with('flash', $flash);
        }
    }
    public function persiapan_store()
    {
        $validator = Validator::make($this->request->all(), [
            'lls_id' => 'required|numeric',
            'pkt_id' => 'required|numeric',
            'tgl_surat_persiapan' => 'required',
            'tgl_rapat_persiapan' => 'required',
            'agenda_persiapan' => 'required',
            'time_rapat_persiapan' => 'required',
            'tgl_input_persiapan' => 'required'
        ]);
        if ($validator->fails()) {
            $flash = [
                'error' => true,
                'pesan' => 'Tidak dapat memproses data, input tidak valid!'
            ];
            $validator->errors()->add('create-paket-persiapan', 'show');
            return redirect()->route('monep.lelang.info', $this->request->lls_id)
                ->withInput()
                ->withErrors($validator)
                ->with('flash', $flash);
        } else {
            $query = PaketPersiapan::updateOrCreate([
                'pkt_id' => $this->request->pkt_id,
                'tgl_rapat' => $this->request->tgl_rapat_persiapan . " " . $this->request->time_rapat_persiapan
            ], [
                'tgl_surat' => $this->request->tgl_surat_persiapan,
                'agenda' => $this->request->agenda_persiapan,
                'tgl_input' => $this->request->tgl_input_persiapan,
                'peg_id' => Auth::user()->username
            ]);
            if ($query) {
                $flash = [
                    'error' => false,
                    'pesan' => 'Berhasil memproses data!'
                ];
            } else {
                $flash = [
                    'error' => true,
                    'pesan' => 'Tidak dapat memproses data, ulangi beberapa saat lagi!'
                ];
            }
            return redirect()->route('monep.lelang.info', $this->request->lls_id)
                ->with('flash', $flash);
        }
    }
    public function pegawai_update(Request $request, $id)
    {
        $validator = Validator::make($this->request->all(), [
            'pangkat' => 'required',
            'golongan' => 'required',
            'jabatan' => 'required',
        ]);
        if ($validator->fails()) {
            $flash = [
                'error' => true,
                'pesan' => 'Tidak dapat memproses data, input tidak lengkap!'
            ];
            return redirect()->back()
                ->withInput()
                ->withErrors($validator)
                ->with('flash', $flash);
        }

        $q = Pegawai::findOrFail($id);
        $q->peg_golongan = $request->golongan;
        $q->peg_pangkat = $request->pangkat;
        $q->peg_jabatan = $request->jabatan;
        if ($q->save()) {
            $flash = [
                'error' => false,
                'pesan' => 'Update data berhasil!'
            ];
        } else {
            $flash = [
                'error' => true,
                'pesan' => 'Terjadi kesalahan saat memproses data!'
            ];
        }
        return redirect()->route('monep.pegawai.edit', $id)->with('flash', $flash);
    }
}
