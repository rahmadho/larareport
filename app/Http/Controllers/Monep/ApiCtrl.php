<?php

namespace App\Http\Controllers\Monep;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Epns\MetodePemilihan;
use App\Models\Epns\KategoriLelang;
use App\Models\Epns\StatusLelang;
use App\Models\Epns\Tahap;
use App\Models\Monep\Paket;
use App\Models\Monep\PaketDokumen as PaketDokumenTable;
use App\Models\Monep\Dokumen;
use App\Models\Epns\PaketDokumen;
use App\Models\Monep\Surat;
use App\Models\Epns\Metode;
use App\Models\Monep\LelangSeleksi;
use App\Models\Monep\PaketPersiapan;
use App\Models\Monep\PaketSpt;
use App\Models\Monep\Rekanan;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

class ApiCtrl extends Controller
{
    private $request;
    private $result;

    function __construct(Request $request)
    {
        // if ($request->input('_token') !== $request->header('X-CSRF-TOKEN')) {
        //     abort(401);
        // }
        $this->request = $request;
    }

    public function datatable_paket(Request $request)
    {
        $this->request = $request;

        $limit = ($this->request->has('length')) ? $this->request->input('length') : 10;
        $start = ($this->request->has('start')) ? $this->request->input('start') : 0;

        $query = Paket::select('pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_flag', 'kgr_id', 'rup_id', 'sbd_id', 'ang_tahun', 'pkt_status', 'pkt_tgl_buat', 'stk_id', 'pnt_id', 'ppk_nip')
            ->with([
                'ppk' => function ($q) {
                    $q->select(['peg_nip', 'peg_nama']);
                },
                'lelang',
                'satker' => function ($q) {
                    $q->select(['stk_id', 'stk_nama']);
                },
                'panitia' => function ($q) {
                    $q->select(['pnt_id', 'pnt_nama']);
                }
            ]);
        if ($this->request->has('tahun')) {
            $query->where('ang_tahun', $this->request->input('tahun'));
        }
        if ($this->request->filter_pnt == 3) {
            $query->doesntHave('panitia');
        } else if ($this->request->filter_pnt == 2) {
            $query->has('panitia');
        }
        if ($this->request->has('tayang')) {
            $query->whereHas('lelang', function ($q) {
                return $q->where('lls_status', $this->request->tayang);
            });
        }
        if ($this->request->has('filter')) {
            $query->where(function ($q) {
                $q->orWhereRaw("LOWER(pkt_nama) like '%" . strtolower($this->request->input('filter')) . "%'");
                $q->orWhere('rup_id', 'like', '%' . $this->request->input('filter') . '%');
                $q->orwhere('pkt_id', 'like', '%' . $this->request->input('filter') . '%');
                $q->orWhereHas('lelang', function ($q) {
                    $q->where('lls_id', 'like', '%' . $this->request->input('filter') . '%');
                });
                $q->orWhereHas('satker', function ($q) {
                    $q->whereRaw("LOWER(stk_nama) like '%" . strtolower($this->request->input('filter')) . "%'");
                });
            });
        }
        if ($this->request->input('filter_satker') > 0) {
            $query->where('stk_id', $this->request->input('filter_satker'));
        }

        if ($this->request->input('filter_progres') == 2) {
            $query->whereHas('lelang', function ($q) {
                return $q->where(function ($q2) {
                    return $q2->where('lls_status', 1);
                });
            });
        } else if ($this->request->input('filter_progres') == 3) {
            $query->whereHas('lelang', function ($q) {
                return $q->where(function ($q2) {
                    return $q2->whereRaw("lls_status = 1 AND pemenang NOTNULL AND tahap = 'SUDAH_SELESAI'");
                });
            });
        } else if ($this->request->input('filter_progres') == 4) {
            $query->whereHas('lelang', function ($q) {
                return $q->where(function ($q2) {
                    return $q2->whereRaw("lls_status = 1 AND pemenang IS NULL AND tahap = 'SUDAH_SELESAI'");
                });
            });
        }
        //return $query->toSql();
        $totalData = $query->get()->count();
        $totalFiltered = $totalData;
        $result = $query->offset($start)->limit($limit)->orderBy('pkt_id', 'desc')->orderBy('pkt_tgl_buat', 'desc')->get();
        foreach ($result as $key => $res) {
            $result[$key]->metode_pemilihan = MetodePemilihan::find($res->lelang->mtd_pemilihan);
            $result[$key]->kategori_lelang = KategoriLelang::find($res->kgr_id);
            $result[$key]->rp_pagu = number_format($res->pkt_pagu, 2, ',', '.');
            $result[$key]->rp_hps = number_format($res->pkt_hps, 2, ',', '.');
            $result[$key]->status_lelang = StatusLelang::find($res->lelang->lls_status);
            $ulang_label = ($res->kgr_id == 1) ? "Seleksi Ulang" : "Tender Ulang";
            $result[$key]->ulang = ($res->lelang->lls_versi_lelang > 1) ? $ulang_label : false;
            $result[$key]->spse_versi = "";
            if ($res->pkt_flag == 3) {
                $result[$key]->spse_versi = "SPSE 4.3";
            } else if ($res->pkt_flag == 2) {
                $result[$key]->spse_versi = "SPSE 4.2";
            } else if ($res->pkt_flag < 2) {
                $result[$key]->spse_versi = "SPSE 3";
            }
            $result[$key]->on_sibaja = false;
            //$result[$key]->tahap = Tahap::toString($res);
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $result
        );
        return $json_data;
    }
    public function datatable_surat(Request $request)
    {
        $this->request = $request;
        $limit = ($this->request->has('length')) ? $this->request->input('length') : 10;
        $start = ($this->request->has('start')) ? $this->request->input('start') : 0;
        $query = Surat::with('paket.satker');
        $query->has('paket');
        if ($this->request->has('filter')) {
            $query->where(function ($q) {
                $q->where('no_surat', 'like', '%' . $this->request->filter . '%');
                $q->orWhereRaw("LOWER(perihal) like '%" . strtolower($this->request->filter) . "%'");
                $q->orWhereHas('paket.satker', function ($q) {
                    $q->whereRaw("LOWER(stk_nama) like '%" . strtolower($this->request->input('filter')) . "%'");
                });
            });
        }
        if ($this->request->has('tahun')) {
            $query->where(function ($q) {
                $q->whereRaw("date_part('year', tgl_surat) =  {$this->request->tahun}");
                $q->orWhereRaw("date_part('year', tgl_input) = {$this->request->tahun}");
            });
        }
        $totalData = $query->get()->count();
        $totalFiltered = $totalData;
        $result = $query->limit($limit)->offset($start)->get();
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $result
        );
        return $json_data;
    }
    // api datatable menu report
    public function datatable_report_paket(Request $request)
    {
        $this->request = $request;

        $limit = ($this->request->has('length')) ? $this->request->input('length') : 10;
        $start = ($this->request->has('start')) ? $this->request->input('start') : 0;

        $query = Paket::select('pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_flag', 'kgr_id', 'rup_id', 'sbd_id', 'ang_tahun', 'pkt_status', 'pkt_tgl_buat', 'stk_id', 'pnt_id', 'ppk_nip')
            ->with([
                'ppk' => function ($q) {
                    $q->select(['peg_nip', 'peg_nama']);
                },
                'lelang',
                'satker' => function ($q) {
                    $q->select(['stk_id', 'stk_nama']);
                },
                'panitia' => function ($q) {
                    $q->select(['pnt_id', 'pnt_nama']);
                },
                'surat', 'spt', 'persiapan'
            ]);
        if ($this->request->has('tahun')) {
            $query->where('ang_tahun', $this->request->input('tahun'));
        }
        // untuk menampilkan paket yang sudah masuk ke ULP
        if ($this->request->has('in_ulp') && $this->request->input('in_ulp') === 'true') {
            $query->ulp();
        }
        if ($this->request->filter_pnt == 3) {
            $query->doesntHave('panitia');
        } else if ($this->request->filter_pnt == 2) {
            $query->has('panitia');
        }
        // filter menampilkan paket per pokja
        if ($this->request->has('panitia')) {
            $query->whereHas('panitia', function ($q) {
                return $q->where('pnt_id', $this->request->panitia);
            });
        };

        if ($this->request->has('filter')) {
            $query->where(function ($q) {
                $filter = strtolower($this->request->input('filter'));
                $q->orWhereRaw("LOWER(pkt_nama) like '%" . $filter . "%'");
                $q->orWhere('rup_id', 'like', '%' . $filter . '%');
                $q->orwhere('pkt_id', 'like', '%' . $filter . '%');
                $q->orWhereHas('lelang', function ($q) {
                    $q->where('lls_id', 'like', '%' . $this->request->input('filter') . '%');
                });
                $q->orWhereHas('satker', function ($q) use ($filter) {
                    $q->whereRaw("LOWER(stk_nama) like '%" . $filter . "%'");
                });
            });
        }

        if ($this->request->input('filter_progres') == 2) {
            $query->whereHas('lelang', function (Builder $q) {
                $q->where('lls_status', 1);
            });
        } else if ($this->request->input('filter_progres') == 3) {
            $query->whereHas('lelang', function (Builder $q) {
                $q->whereRaw("lls_status = 1 AND pemenang NOTNULL AND tahap = 'SUDAH_SELESAI'");
            });
        } else if ($this->request->input('filter_progres') == 4) {
            $query->whereHas('lelang', function (Builder $q) {
                $q->where('lls_status', 1)->where('pemenang', '=', NULL)->where('tahap', '=', 'SUDAH_SELESAI');
            });
        }

        if ($this->request->input('filter_satker') > 0) {
            $query->where('stk_id', $this->request->input('filter_satker'));
        }

        //return $query->toSql();
        $totalData = $query->get()->count();
        $totalFiltered = $totalData;
        $result = $query->offset($start)->limit($limit)->orderBy('pkt_tgl_buat', 'desc')->get();
        foreach ($result as $key => $res) {
            $result[$key]->metode_pemilihan = MetodePemilihan::find($res->lelang->mtd_pemilihan);
            $result[$key]->metode = Metode::find($res->lelang->mtd_id);
            $result[$key]->kategori_lelang = KategoriLelang::find($res->kgr_id);
            $result[$key]->rp_pagu = number_format($res->pkt_pagu, 2, ',', '.');
            $result[$key]->rp_hps = number_format($res->pkt_hps, 2, ',', '.');
            $result[$key]->status_lelang = StatusLelang::find($res->lelang->lls_status);
            $ulang_label = ($res->kgr_id == 1) ? "Seleksi Ulang" : "Tender Ulang";
            $result[$key]->ulang = ($res->lelang->lls_versi_lelang > 1) ? $ulang_label : false;
            $result[$key]->spse_versi = "";
            if ($res->pkt_flag == 3) {
                $result[$key]->spse_versi = "SPSE 4.3";
            } else if ($res->pkt_flag == 2) {
                $result[$key]->spse_versi = "SPSE 4.2";
            } else if ($res->pkt_flag < 2) {
                $result[$key]->spse_versi = "SPSE 3";
            }
            $result[$key]->on_sibaja = false;
            //$result[$key]->tahap_now = Tahap::findByLlsId($res->lelang->lls_id);
            $result[$key]->tahap_now = Tahap::find($res->lelang->tahap);
            $result[$key]->pemenang = ($res->lelang->pemenang) ? json_decode($res->lelang->pemenang) : null;
            if ($result[$key]->pemenang) {
                $result[$key]->pemenang->rekanan = Rekanan::find($res->pemenang->rkn_id);
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $result
        );
        return $json_data;
    }
    // api datatable menu report (active)
    public function datatable_paket_query_builder(Request $request)
    {
        $this->request = $request;

        $limit = ($this->request->has('length')) ? $this->request->input('length') : 10;
        $start = ($this->request->has('start')) ? $this->request->input('start') : 0;

        $query = DB::connection('monep')->table('paket as ta');
        $query->select('ta.pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_flag', 'kgr_id', 'ta.rup_id', 'sbd_id', 'ang_tahun', 'pkt_status', 'pkt_tgl_buat', 'ta.stk_id', 'ta.pnt_id', 'ppk_nip',);
        $query->addSelect('peg_nip', 'peg_nama', 'stk_nama', 'pnt_nama', 'lls_id', 'mtd_id', 'mtd_pemilihan', 'lls_status', 'tahap', 'pemenang', 'lls_versi_lelang', 'lls_tgl_setuju');
        $query->leftJoin('lelang_seleksi as tb', 'ta.pkt_id', '=', 'tb.pkt_id')
            ->leftJoin('pegawai as tc', 'ta.ppk_nip', '=', 'tc.peg_nip')
            ->leftJoin('panitia as td', 'ta.pnt_id', '=', 'td.pnt_id')
            ->leftJoin('surat as te', 'ta.pkt_id', '=', 'te.pkt_id')
            ->leftJoin('satuan_kerja as tf', 'ta.stk_id', '=', 'tf.stk_id');
        $query->whereRaw('lls_versi_lelang=(SELECT max(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=ta.pkt_id)');
        if ($this->request->has('tahun')) {
            $query->where('ang_tahun', $this->request->input('tahun'));
        }
        // untuk menampilkan paket yang sudah masuk ke ULP
        if ($this->request->has('in_ulp') && $this->request->input('in_ulp') === 'true') {
            $query->whereNotNull('te.pkt_id');
        }
        if ($this->request->filter_pnt == 3) {
            $query->whereNull('td.pnt_id');
        } else if ($this->request->filter_pnt == 2) {
            $query->whereNotNull('td.pnt_id');
        }
        // filter menampilkan paket per pokja
        if ($this->request->has('panitia')) {
            $query->where('ta.pnt_id', $this->request->panitia);
        };

        if ($this->request->has('filter')) {
            $query->where(function ($q) {
                $filter = strtolower($this->request->input('filter'));
                $q->orWhereRaw("LOWER(pkt_nama) like '%" . $filter . "%'");
                $q->orWhere('rup_id', 'like', '%' . $filter . '%');
                $q->orWhere('ta.pkt_id', 'like', '%' . $filter . '%');
                $q->orWhere('lls_id', 'like', '%' . $this->request->input('filter') . '%');
                $q->orWhereRaw("LOWER(stk_nama) like '%" . $filter . "%'");
            });
        }

        if ($this->request->input('filter_progres') == 2) {
            $query->where('lls_status', 1);
        } else if ($this->request->input('filter_progres') == 3) {
            $query->whereRaw("lls_status = 1 AND pemenang NOTNULL AND tahap = 'SUDAH_SELESAI'");
        } else if ($this->request->input('filter_progres') == 4) {
            $query->where('lls_status', 1)->whereNull('pemenang')->where('tahap', '=', 'SUDAH_SELESAI');
        }

        if ($this->request->input('filter_satker') > 0) {
            $query->where('ta.stk_id', $this->request->input('filter_satker'));
        }

        //return $query->toSql();
        $totalData = $query->get()->count();
        $totalFiltered = $totalData;
        $result = $query->offset($start)->limit($limit)->orderBy('pkt_tgl_buat', 'desc')->get();
        foreach ($result as $key => $res) {
            $result[$key]->metode_pemilihan = MetodePemilihan::find($res->mtd_pemilihan);
            $result[$key]->metode = Metode::find($res->mtd_id);
            $result[$key]->kategori_lelang = KategoriLelang::find($res->kgr_id);
            $result[$key]->rp_pagu = number_format($res->pkt_pagu, 2, ',', '.');
            $result[$key]->rp_hps = number_format($res->pkt_hps, 2, ',', '.');
            $result[$key]->status_lelang = StatusLelang::find($res->lls_status);
            $ulang_label = ($res->kgr_id == 1) ? "Seleksi Ulang" : "Tender Ulang";
            $result[$key]->ulang = ($res->lls_versi_lelang > 1) ? $ulang_label : false;
            $result[$key]->spse_versi = "";
            if ($res->pkt_flag == 3) {
                $result[$key]->spse_versi = "SPSE 4.3";
            } else if ($res->pkt_flag == 2) {
                $result[$key]->spse_versi = "SPSE 4.2";
            } else if ($res->pkt_flag < 2) {
                $result[$key]->spse_versi = "SPSE 3";
            }
            $result[$key]->on_sibaja = false;
            //$result[$key]->tahap_now = Tahap::findByLlsId($res->lelang->lls_id);
            $result[$key]->tahap_now = Tahap::find($res->tahap);
            $result[$key]->pemenang = ($res->pemenang) ? json_decode($res->pemenang) : null;
            if ($result[$key]->pemenang) {
                $result[$key]->pemenang->rekanan = Rekanan::find($result[$key]->pemenang->rkn_id);
            }
            $result[$key]->spt = PaketSpt::where('pkt_id', $res->pkt_id)->get();
            $result[$key]->persiapan = PaketPersiapan::where('pkt_id', $res->pkt_id)->get();
            $result[$key]->lelang = LelangSeleksi::find($res->lls_id);
        }
        $json_data = array(
            "draw"            => intval($this->request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $result
        );
        return $json_data;
    }
    public function datatable_pilih_paket(Request $request)
    {
        $this->request = $request;
        $limit = ($this->request->has('length')) ? $this->request->input('length') : 10;
        $start = ($this->request->has('start')) ? $this->request->input('start') : 0;
        $query = DB::connection('monep')->table('paket as ta')->select('ta.pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_flag', 'kgr_id', 'rup_id', 'sbd_id', 'ang_tahun', 'pkt_status', 'pkt_tgl_buat', 'ta.stk_id', 'ppk_nip')
            ->leftJoin('lelang_seleksi as tb', function ($q) {
                $q->on('ta.pkt_id', '=', 'tb.pkt_id')
                    ->where('lls_status', 0);
                //->whereRaw("ta.pkt_id NOT IN (SELECT tsa.pkt_id FROM lelang_seleksi tsa JOIN paket tsb ON tsa.pkt_id=tsb.pkt_id WHERE tsa.pkt_id=ta.pkt_id AND lls_status=1 OR (tsb.pkt_nama=ta.pkt_nama AND tsb.stk_id=ta.stk_id))");
            })
            ->leftJoin('satuan_kerja as tc', 'ta.stk_id', '=', 'tc.stk_id')->orderBy('pkt_id', 'desc');
        $query->addSelect('tb.lls_id', 'tb.mtd_pemilihan', 'tc.stk_nama', 'tb.lls_status', 'tb.lls_versi_lelang', 'tb.eva_versi', 'rup_id');
        $query->whereRaw('lls_versi_lelang = (SELECT MAX(lls_versi_lelang) FROM lelang_seleksi where lls_id = tb.lls_id)');
        $query->where('pkt_hps', '>', 0)->whereRaw("ta.pkt_nama NOT IN (SELECT tsb.pkt_nama FROM lelang_seleksi tsa JOIN paket tsb ON tsa.pkt_id=tsb.pkt_id WHERE tsa.pkt_id=ta.pkt_id AND lls_status=1)");
        //$query->whereRaw("ta.pkt_id NOT IN (SELECT pkt_id FROM lelang_seleksi WHERE pkt_id=ta.pkt_id AND lls_status=1)");
        //$query->whereRaw("ta.pkt_id NOT IN (SELECT tsa.pkt_id FROM lelang_seleksi tsa JOIN paket tsb ON tsa.pkt_id=tsb.pkt_id WHERE tsa.pkt_id=ta.pkt_id AND lls_status=1 OR (tsb.pkt_nama=ta.pkt_nama AND tsb.stk_id=ta.stk_id))");
        if ($this->request->has('tahun')) {
            $query->where('ang_tahun', $this->request->tahun);
        }
        if ($this->request->input('filter_satker') > 0) {
            $query->where('tc.stk_id', $this->request->input('filter_satker'));
        }
        if ($this->request->has('tayang')) {
            //$query->where('lls_status', $this->request->tayang);
        }
        if ($this->request->has('kgr_id')) {
            $query->where('kgr_id', $this->request->kgr_id);
        }
        if ($this->request->has('filter')) {
            $query->where(function ($q) {
                $q->orWhereRaw("LOWER(pkt_nama) like '%" . strtolower($this->request->input('filter')) . "%'");
                $q->orWhere('rup_id', 'like', '%' . $this->request->input('filter') . '%');
                $q->orwhere('ta.pkt_id', 'like', '%' . $this->request->input('filter') . '%');
                $q->orWhere('tb.lls_id', 'like', '%' . $this->request->input('filter') . '%');
                $q->orWhereRaw("LOWER(stk_nama) like '%" . strtolower($this->request->input('filter')) . "%'");
            });
        }
        $totalData = $query->get()->count();
        $totalFiltered = $totalData;
        $result = $query->offset($start)->limit($limit)->orderBy('pkt_tgl_buat', 'desc')->get();
        foreach ($result as $key => $res) {
            $result[$key]->metode_pemilihan = MetodePemilihan::find($res->mtd_pemilihan);
            $result[$key]->kategori_lelang = KategoriLelang::find($res->kgr_id);
            $result[$key]->rp_pagu = number_format($res->pkt_pagu, 2, ',', '.');
            $result[$key]->rp_hps = number_format($res->pkt_hps, 2, ',', '.');
            $result[$key]->status_lelang = StatusLelang::find($res->lls_status);
            $ulang_label = ($res->kgr_id == 1) ? "Seleksi Ulang" : "Tender Ulang";
            $result[$key]->ulang = ($res->lls_versi_lelang > 1) ? $ulang_label : false;
            $result[$key]->spse_versi = "";
            if ($res->pkt_flag == 3) {
                $result[$key]->spse_versi = "SPSE 4.3";
            } else if ($res->pkt_flag == 2) {
                $result[$key]->spse_versi = "SPSE 4.2";
            } else if ($res->pkt_flag < 2) {
                $result[$key]->spse_versi = "SPSE 3";
            }
            $result[$key]->on_sibaja = false;
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $result
        );
        return $json_data;
    }
    public function datatable_paket_rup(Request $request)
    {
        $filename = 'json-rup-' . date('Ym') . '.json';
        $url = 'https://sirup.lkpp.go.id/sirup/datatablectr/dataruppenyediakldi?idKldi=D462&tahun=2019&iDisplayStart=0&iDisplayLength=1000000&_=1566352631490';
        if (Storage::exists('json/' . $filename)) {
            $response = Storage::get('json/' . $filename);
        } else {
            $get = file_get_contents($url);
            $json = json_decode($get);
            $array = [];
            foreach ($json->aaData as $item) {
                $obj = new stdClass;
                $obj->rup_id = $item[0];
                $obj->stk_nama = $item[1];
                $obj->pkt_nama = $item[2];
                $obj->pkt_pagu = $item[3];
                $obj->mtd_pemilihan = $item[4];
                $obj->sbd_id = $item[5];
                $obj->waktu_pemilihan = $item[7];
                array_push($array, $obj);
            }
            $response = json_encode($array);
            Storage::put('json/' . $filename, $response);
        }
        $collection = collect(json_decode($response));
        $mtd = [
            "Pengadaan Langsung",
            "Tender",
            // "e-Purchasing",
            "Seleksi",
            "Penunjukan Langsung",
            "Tender Cepat"
        ];
        $this->request = $request;
        $limit = ($this->request->has('length')) ? $this->request->input('length') : 10;
        $start = ($this->request->has('start')) ? $this->request->input('start') : 0;

        $totalData = count($collection);
        $result = $collection->slice($start)->take($limit);
        // $result = $collection->whereIn('mtd_pemilihan', $mtd)->slice($start)->take($limit);
        $totalFiltered = $totalData;
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $result
        );
        return $json_data;
    }
    public function get(Request $request, $url)
    {
        if (method_exists($this, $url) and $request->isMethod('get')) {
            $this->request = $request;
            $this->$url();
            return $this->result;
        } else {
            abort(404);
        }
    }
    public function post(Request $request, $url)
    {
        if (method_exists($this, $url) and $request->isMethod('post')) {
            $this->request = $request;
            $this->$url();
            return $this->result;
        } else {
            abort(404);
        }
    }
    public function put(Request $request, $url)
    {
        if (method_exists($this, $url) and $request->isMethod('put')) {
            $this->request = $request;
            $this->$url();
            return $this->result;
        } else {
            abort(404);
        }
    }
    public function delete(Request $request, $url)
    {
        if (method_exists($this, $url) and $request->isMethod('delete')) {
            $this->request = $request;
            $this->$url();
            return $this->result;
        } else {
            abort(404);
        }
    }

    public function verify_document()
    {
        $paket_doc = PaketDokumen::get_key($this->request->kgr_id);
        foreach ($paket_doc as $doc) {
            $ada = isset(($this->request->is_ada[$doc])) ? true : false;
            $this->result[] = PaketDokumenTable::updateOrCreate(['pkt_id' => $this->request->pkt_id, 'id_dokumen' => $doc], ['is_ada' => $ada, 'tgl_buat' => date('Y-m-d H:i:s')]);
        }
    }

    public function paket_masuk()
    {
        $this->result = Paket::where('auditupdate', '>', Carbon::now()->subDay(15))->get();
    }

    public function dokumen()
    {
        $paket_doc = PaketDokumen::get_key($this->request->kgr_id);
        $this->result = Dokumen::whereIn('id', $paket_doc)->get();
    }

    public function delete_surat()
    {
        $query = Surat::find($this->request->id);
        $this->result = $query->delete();
    }
}
