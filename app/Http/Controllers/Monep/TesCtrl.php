<?php

namespace App\Http\Controllers\Monep;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Epns\KategoriLelang;
use App\Models\Epns\Metode;
use App\Models\Epns\MetodePemilihan;
use App\Models\Epns\StatusLelang;
use App\Models\Epns\Tahap;
use App\Models\Monep\LelangSeleksi;
use App\Models\Monep\Paket;
use App\Models\Monep\PaketPersiapan;
use App\Models\Monep\PaketSpt;
use App\Models\Monep\Rekanan;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use stdClass;

class TesCtrl extends Controller
{
    function __construct(Request $req)
    {
        $this->lastUpdate = Carbon::now()->subDay(20);
        $this->request = $req;
    }
    public function run($url)
    {
        $this->$url();
        //var_dump($this->response);
        return $this->response;
    }
    public function tes()
    {
        $query = DB::connection('epns')->table('paket as p')
            ->selectRaw("p.kgr_id,p.pkt_id,pkt_nama,pkt_pagu,pkt_hps,pkt_status,pkt_flag")
            ->leftJoin('lelang_seleksi as lls', 'lls.pkt_id', '=', 'p.pkt_id')
            ->leftJoin('paket_ppk as pk', 'pk.pkt_id', '=', 'p.pkt_id');
        $query->addSelect('lls_id', 'mtd_pemilihan', 'mtd_id', 'lls_status', 'lls_versi_lelang', 'lls_tgl_setuju');
        //$query->addSelect(DB::raw('tahap_now(lls_id, localtimestamp) AS tahaps'));
        //$query->addSelect(DB::raw('(SELECT dtj_tglakhir FROM jadwal WHERE lls_id=lls.lls_id ORDER BY dtj_tglakhir DESC LIMIT 1)'));
        //$query->addSelect(DB::raw("(SELECT CONCAT(peg_nama, ' / ', peg_nip) FROM pegawai JOIN ppk ON pegawai.peg_id=ppk.peg_id WHERE ppk_id = pk.ppk_id) as ppk"));
        $if_cepat = "(mtd_pemilihan=9 OR mtd_pemilihan=10) AND (SELECT COUNT(*) FROM peserta WHERE lls_id=lls.lls_id AND is_pemenang_verif=1) > 0";
        $harga_cepat = "(SELECT DISTINCT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1 LIMIT 1)";
        $harga_ = "(SELECT DISTINCT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang=1 AND nev_urutan=1 LIMIT 1)";
        $query->addSelect(DB::raw("CASE WHEN {$if_cepat} THEN {$harga_cepat} ELSE {$harga_} END as pemenang_harga"));
        $rekanan_cepat = "(SELECT DISTINCT (SELECT rkn_nama FROM rekanan WHERE rkn_id=psr.rkn_id) FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1 LIMIT 1)";
        $rekanan_ = "(SELECT (SELECT DISTINCT rkn_nama FROM rekanan WHERE rkn_id=psr.rkn_id) FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=lls.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=lls.lls_id) AND eva_jenis=4) AND psr.is_pemenang=1 AND nev_urutan=1 LIMIT 1)";
        $query->addSelect(DB::raw("CASE WHEN {$if_cepat} THEN {$rekanan_cepat} ELSE {$rekanan_} END as pemenang_rekanan"));
        $query->addSelect(DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama"));
        $query->whereRaw('lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=p.pkt_id)');
        $query->where('lls_status', 1);
        $query->whereRaw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct ang.ang_tahun), ', ') FROM paket_anggaran pa JOIN anggaran ang ON ang.ang_id=pa.ang_id WHERE pa.pkt_id=p.pkt_id)=:tahun", ['tahun' => 2019]);
        $this->response = $query->toSql();
    }
    public function lelang()
    {
        $get = DB::connection('epns')->table('lelang_seleksi as ta')->select('lls_id', 'pkt_id', 'mtd_id', 'lls_versi_lelang', 'lls_keterangan', 'lls_dibuat_tanggal', 'lls_diulang_karena', 'lls_ditutup_karena', 'lls_terverifikasi', 'lls_tgl_setuju', 'lls_status', 'mtd_pemilihan', 'mtd_evaluasi', 'auditupdate', 'lls_kontrak_pembayaran', 'lls_kontrak_pekerjaan', 'lls_kontrak_tahun', 'lls_kontrak_sbd', 'lls_penetapan_pemenang');
        $get->addSelect(DB::raw("(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=ta.lls_id) as eva_versi"));
        $get->addSelect(DB::raw("CASE WHEN (SELECT COUNT(*) FROM sppbj WHERE lls_id=ta.lls_id) > 0 THEN true ELSE false END AS is_sppbj"));
        $get->addSelect(DB::raw('CASE WHEN (SELECT COUNT(*) FROM kontrak WHERE lls_id=ta.lls_id) > 0 THEN true ELSE false END AS is_kontrak'));
        $get->addSelect(DB::raw('(SELECT kontrak_nilai FROM kontrak WHERE lls_id=ta.lls_id ORDER BY auditupdate DESC LIMIT 1) AS nilai_kontrak'));
        $get->addSelect(DB::raw('tahap_now(lls_id, localtimestamp) as tahap'));
        // $if_cepat = "(mtd_pemilihan=9 OR mtd_pemilihan=10) AND (SELECT COUNT(*) FROM peserta WHERE lls_id=ta.lls_id AND is_pemenang_verif=1) > 0";
        // $harga_cepat = "(SELECT DISTINCT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=ta.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=ta.lls_id) AND eva_jenis=4) AND psr.is_pemenang_verif=1 LIMIT 1)";
        // $harga_ = "(SELECT DISTINCT CASE WHEN (nev_harga_negosiasi>0 AND nev_harga_negosiasi NOTNULL) THEN nev_harga_negosiasi WHEN (nev_harga_terkoreksi>0 AND nev_harga_terkoreksi NOTNULL) THEN nev_harga_terkoreksi WHEN (psr_harga_terkoreksi>0 AND psr_harga_terkoreksi NOTNULL) THEN psr_harga_terkoreksi ELSE psr_harga END FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=ta.lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=ta.lls_id) AND eva_jenis=4) AND psr.is_pemenang=1 AND nev_urutan=1 LIMIT 1)";
        // $get->addSelect(DB::raw("CASE WHEN {$if_cepat} THEN {$harga_cepat} ELSE {$harga_} END as pemenang_harga"));
        if ($this->request->has('tahun')) {
            $get->whereRaw("date_part('year', ta.auditupdate) = " . $this->request->input('tahun'));
        }
        $get->where('auditupdate', '>', $this->lastUpdate);
        $get = $get->orderBy('lls_id', 'asc')->get()->toArray();
        $data = [];
        foreach ($get as $arr) {
            $pemenang = $this->pemenang($arr);
            $exculde = [1236016, 1235016]; // pemenang paket ini susah ngambil harga penawaran
            if (!in_array($arr->lls_id, $exculde)) {
                $arr->pemenang = ($pemenang) ? $pemenang : null;
            }
            $data[] = (array) $arr;
        }
        $this->response = $data;
    }
    private function pemenang($lelang)
    {
        $select = "nev_lulus,nev_urutan,nev_harga,nev_harga_terkoreksi,nev_harga_negosiasi,psr.psr_id,rkn_id,psr_harga,psr_harga_terkoreksi,is_pemenang,is_pemenang_verif";
        $pemenang = null;
        if ($lelang) {
            $mtd_pemilihan = $lelang->mtd_pemilihan;
            if ($mtd_pemilihan == 9 || $mtd_pemilihan == 10) {
                $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang_verif =:is_pemenang", ["lls_id" => $lelang->lls_id, "is_pemenang" => 1]);
            }
            if ($pemenang == null) {
                $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang =:is_pemenang", ["lls_id" => $lelang->lls_id, "is_pemenang" => 1]);
            }
            if (sizeof($pemenang) > 1) {
                // klo perlu tambah and nev_urutan = 1
                if ($mtd_pemilihan == 9 || $mtd_pemilihan == 10) {
                    $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang_verif =:is_pemenang AND nev_urutan = 1", ["lls_id" => $lelang->lls_id, "is_pemenang" => 1]);
                } else {
                    $pemenang = DB::connection('epns')->select("SELECT {$select} FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=:lls_id AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=:lls_id) AND eva_jenis = 4) AND psr.is_pemenang =:is_pemenang AND nev_urutan = 1", ["lls_id" => $lelang->lls_id, "is_pemenang" => 1]);
                }
            }
        }
        return ($pemenang) ? $pemenang : $pemenang;
    }
    public function get_rup()
    {
        $filename = 'json-rup-' . date('Ym') . '.json';
        $url = 'https://sirup.lkpp.go.id/sirup/datatablectr/dataruppenyediakldi?idKldi=D462&tahun=2019&iDisplayStart=0&iDisplayLength=1000000&_=1566352631490';

        if (Storage::exists('json/' . $filename)) {
            $response = Storage::get('json/' . $filename);
        } else {
            $get = file_get_contents($url);
            $json = json_decode($get);
            $array = [];
            foreach ($json->aaData as $item) {
                $obj = new stdClass;
                $obj->rup_id = $item[0];
                $obj->stk_nama = $item[1];
                $obj->pkt_nama = $item[2];
                $obj->pkt_pagu = $item[3];
                $obj->mtd_pemilihan = $item[4];
                $obj->sbd_id = $item[5];
                $obj->waktu_pemilihan = $item[7];
                array_push($array, $obj);
            }
            $response = json_encode($array);
            Storage::put('json/' . $filename, $response);
        }
        $collection = collect(json_decode($response));
        $this->response = array_keys($collection->groupBy('mtd_pemilihan')->toArray());
    }
    public function lelangs()
    {
        $q = Paket::with('lelangs')->whereHas('lelangs', function ($q2) {
            $q2->gagal();
        })->where('ang_tahun', 2019)->get();
        $this->response = $q;
    }
    public function lelang2()
    {
        $limit = ($this->request->has('length')) ? $this->request->input('length') : 10;
        $start = ($this->request->has('start')) ? $this->request->input('start') : 0;

        $query = DB::connection('monep')->table('paket as ta');
        $query->select('ta.pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_flag', 'kgr_id', 'ta.rup_id', 'sbd_id', 'ang_tahun', 'pkt_status', 'pkt_tgl_buat', 'ta.stk_id', 'ta.pnt_id', 'ppk_nip',);
        $query->addSelect('peg_nip', 'peg_nama', 'stk_nama', 'pnt_nama', 'lls_id', 'mtd_id', 'mtd_pemilihan', 'lls_status', 'tahap', 'pemenang', 'lls_versi_lelang');
        $query->leftJoin('lelang_seleksi as tb', 'ta.pkt_id', '=', 'tb.pkt_id')
            ->leftJoin('pegawai as tc', 'ta.ppk_nip', '=', 'tc.peg_nip')
            ->leftJoin('panitia as td', 'ta.pnt_id', '=', 'td.pnt_id')
            ->leftJoin('surat as te', 'ta.pkt_id', '=', 'te.pkt_id')
            ->leftJoin('satuan_kerja as tf', 'ta.stk_id', '=', 'tf.stk_id');
        $query->whereRaw('lls_versi_lelang=(SELECT max(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=ta.pkt_id)');
        if ($this->request->has('tahun')) {
            $query->where('ang_tahun', $this->request->input('tahun'));
        }
        // untuk menampilkan paket yang sudah masuk ke ULP
        if ($this->request->has('in_ulp') && $this->request->input('in_ulp') === 'true') {
            $query->whereNotNull('te.pkt_id');
        }
        if ($this->request->filter_pnt == 3) {
            $query->whereNull('td.pnt_id');
        } else if ($this->request->filter_pnt == 2) {
            $query->whereNotNull('td.pnt_id');
        }
        // filter menampilkan paket per pokja
        if ($this->request->has('panitia')) {
            $query->where('ta.pnt_id', $this->request->panitia);
        };

        if ($this->request->has('filter')) {
            $query->where(function ($q) {
                $filter = strtolower($this->request->input('filter'));
                $q->orWhereRaw("LOWER(pkt_nama) like '%" . $filter . "%'");
                $q->orWhere('rup_id', 'like', '%' . $filter . '%');
                $q->orWhere('ta.pkt_id', 'like', '%' . $filter . '%');
                $q->orWhere('lls_id', 'like', '%' . $this->request->input('filter') . '%');
                $q->orWhereRaw("LOWER(stk_nama) like '%" . $filter . "%'");
            });
        }

        if ($this->request->input('filter_progres') == 2) {
            $query->where('lls_status', 1);
        } else if ($this->request->input('filter_progres') == 3) {
            $query->whereRaw("lls_status = 1 AND pemenang NOTNULL AND tahap = 'SUDAH_SELESAI'");
        } else if ($this->request->input('filter_progres') == 4) {
            $query->where('lls_status', 1)->where('pemenang', '=', NULL)->where('tahap', '=', 'SUDAH_SELESAI');
        }

        if ($this->request->input('filter_satker') > 0) {
            $query->where('ta.stk_id', $this->request->input('filter_satker'));
        }

        //return $query->toSql();
        $totalData = $query->get()->count();
        $totalFiltered = $totalData;
        $result = $query->offset($start)->limit($limit)->orderBy('pkt_tgl_buat', 'desc')->get();
        foreach ($result as $key => $res) {
            $result[$key]->metode_pemilihan = MetodePemilihan::find($res->mtd_pemilihan);
            $result[$key]->metode = Metode::find($res->mtd_id);
            $result[$key]->kategori_lelang = KategoriLelang::find($res->kgr_id);
            $result[$key]->rp_pagu = number_format($res->pkt_pagu, 2, ',', '.');
            $result[$key]->rp_hps = number_format($res->pkt_hps, 2, ',', '.');
            $result[$key]->status_lelang = StatusLelang::find($res->lls_status);
            $ulang_label = ($res->kgr_id == 1) ? "Seleksi Ulang" : "Tender Ulang";
            $result[$key]->ulang = ($res->lls_versi_lelang > 1) ? $ulang_label : false;
            $result[$key]->spse_versi = "";
            if ($res->pkt_flag == 3) {
                $result[$key]->spse_versi = "SPSE 4.3";
            } else if ($res->pkt_flag == 2) {
                $result[$key]->spse_versi = "SPSE 4.2";
            } else if ($res->pkt_flag < 2) {
                $result[$key]->spse_versi = "SPSE 3";
            }
            $result[$key]->on_sibaja = false;
            //$result[$key]->tahap_now = Tahap::findByLlsId($res->lelang->lls_id);
            $result[$key]->tahap_now = Tahap::find($res->tahap);
            $result[$key]->pemenang = ($res->pemenang) ? json_decode($res->pemenang) : null;
            if ($result[$key]->pemenang) {
                $result[$key]->pemenang->rekanan = Rekanan::find($res->rkn_id);
            }
            $result[$key]->spt = PaketSpt::where('pkt_id', $res->pkt_id)->get();
            $result[$key]->persiapan = PaketPersiapan::where('pkt_id', $res->pkt_id)->get();
            $result[$key]->lelang = LelangSeleksi::find($res->lls_id);
        }
        $json_data = array(
            "draw"            => intval($this->request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $result
        );
        $this->response = $json_data;
    }
}
