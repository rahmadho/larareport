<?php

namespace App\Http\Controllers\Monep;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Epns\Fungsi;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\Monep\Paket;
use App\Models\Epns\KategoriLelang;
use App\Models\Epns\PaketDokumen;
use App\Models\Monep\PaketDokumen as PaketDokumenTable;
use App\Models\Monep\Dokumen;
use App\Models\Monep\PaketPersiapan;
use App\Models\Epns\Metode;
use App\Models\Epns\MetodePemilihan;
use App\Models\Epns\StatusLelang;
use App\Models\Epns\Tahap;
use App\Models\Monep\LelangSeleksi;
use App\Models\Monep\Panitia;
use App\Models\Monep\Rekanan;
use Illuminate\Support\Facades\DB;

class PrintCtrl extends Controller
{
    private $request;
    public function __construct(Request $request)
    {
        $this->tahun_now = 2019;
        $this->stk_ukpbj = 1805016;
        $this->request = $request;
    }
    public function info(Request $request, $id)
    {
        $lls_id = ($this->request->input('lls_id')) ? $this->request->input('lls_id') : $id;
        $query = LelangSeleksi::with([
            'paket' => function ($q) {
                $q->select(['pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_status', 'pkt_flag', 'kgr_id', 'rup_id', 'sbd_id', 'ang_tahun', 'ppk_nip', 'pnt_id', 'stk_id', 'pkt_tgl_buat']);
            }, 'paket.ppk' => function ($q) {
                $q->select(['peg_nip', 'peg_nama']);
            }, 'paket.panitia' => function ($q) {
                $q->select(['pnt_id', 'pnt_nama']);
            }, 'paket.satker' => function ($q) {
                $q->select(['stk_id', 'stk_nama', 'instansi_id']);
            }, 'jadwal', 'aktivitas', 'tgl_penetapan_pemenang', 'paket.panitia.pegawai', 'paket.satker.instansi', 'paket.surat', 'paket.spt.panitia', 'paket.persiapan'
        ]);
        $query->select(DB::raw("*,tahap_now(lls_id, localtimestamp) as tahap_now"));
        $query->where('lls_id', $lls_id);
        $result = $query->first();
        if (!$result) {
            abort(404);
        }
        $result->metode_pemilihan = MetodePemilihan::find($result->mtd_pemilihan);
        $result->kategori_lelang = KategoriLelang::find($result->paket->kgr_id);
        $result->rp_pagu = number_format($result->paket->pkt_pagu, 2, ',', '.');
        $result->rp_hps = number_format($result->paket->pkt_hps, 2, ',', '.');
        $result->status_lelang = StatusLelang::find($result->lls_status);
        $ulang_label = ($result->paket->kgr_id == 1) ? "Seleksi Ulang" : "Tender Ulang";
        $result->ulang = ($result->lls_versi_lelang > 1) ? $ulang_label : false;
        $result->spse_versi = "";
        if ($result->paket->pkt_flag == 3) {
            $result->spse_versi = "SPSE 4.3";
        } else if ($result->paket->pkt_flag == 2) {
            $result->spse_versi = "SPSE 4.2";
        } else if ($result->paket->pkt_flag < 2) {
            $result->spse_versi = "SPSE 3";
        }
        $result->on_sibaja = false;
        $paket_doc = PaketDokumen::get_key($result->paket->kgr_id);
        $result->dokumen = Dokumen::whereIn('id', $paket_doc)->get();
        $result->metode = Metode::find($result->mtd_id);
        $result->selesai = $result->jadwal->where('thp_id', 18817)->first();
        foreach ($result->dokumen as $key => $doc) {
            $adaTidak = PaketDokumenTable::where('pkt_id', $result->paket->pkt_id)->where('id_dokumen', $doc['id'])->first();
            $result->dokumen[$key]->ada = ($adaTidak && $adaTidak->is_ada) ? true : false;
        }
        foreach ($result->aktivitas as $key2 => $tahap) {
            $result->aktivitas[$key2]->keterangan = Tahap::find($tahap->akt_jenis);
        }
        $result->tahap_now = Tahap::find($result->tahap_now);
        $result->pemenang = ($result->pemenang) ? json_decode($result->pemenang) : null;
        if ($result->pemenang) {
            $result->pemenang->rekanan = Rekanan::with(['bentuk_usaha', 'kabupaten'])->select('rkn_nama', 'rkn_alamat', 'rkn_npwp', 'btu_id', 'kbp_id')->where('rkn_id', $result->pemenang->rkn_id)->first();
        }
        $panitia = Panitia::with('pegawai')->where('is_active', -1)->where('stk_id', $this->stk_ukpbj)->where('pnt_tahun', $this->tahun_now)->orderBy('pnt_id', 'asc')->get();
        return view('monep.pdf.paket', compact('result', 'panitia')); // $result; //
    }
    public function spt(Request $request, $id)
    {
        $paket = Paket::with(['lelang', 'panitia', 'satker', 'panitia.pegawai', 'spt'])
            ->where('pkt_id', $id)
            ->has('panitia')->first();
        //return view('monep.pdf.spt', compact('paket'));
        // PDF::setOptions(['defaultPaperSize' => 'a4']);
        $pdf = PDF::loadView('monep.pdf.spt', compact('paket'))->setPaper('legal', 'potrait');
        // return $pdf->download('spt.pdf'); // untuk direct download 
        return $pdf->stream(); // preview in browser
    }
    public function dokumen(Request $request, $id)
    {
        $paket = Paket::with(['panitia', 'satker', 'surat', 'ppk' => function ($q) {
            $q->select(['peg_nip', 'peg_nama']);
        }])->where('pkt_id', $id)->has('panitia')->first();
        $paket->kategori = KategoriLelang::find($paket->kgr_id);
        $paket_doc = PaketDokumen::get_key($paket->kgr_id);
        $paket->dokumen = Dokumen::whereIn('id', $paket_doc)->get();
        foreach ($paket->dokumen as $key => $doc) {
            $adaTidak = PaketDokumenTable::where('pkt_id', $paket->pkt_id)->where('id_dokumen', $doc['id'])->first();
            $paket->dokumen[$key]->ada = ($adaTidak && $adaTidak->is_ada) ? true : false;
        }
        $pdf = PDF::loadView('monep.pdf.dokumen', compact('paket'))->setPaper('A4', 'potrait');
        return $pdf->stream();
    }
    public function persiapan(Request $request, $id)
    {
        $persiapan = PaketPersiapan::with(['paket', 'paket.satker', 'paket.surat', 'paket.spt', 'paket.ppk' => function ($q) {
            $q->select(['peg_nip', 'peg_nama']);
        }])->where('id', $id)->has('paket')->has('paket.spt')->first();
        $persiapan->paket->kategori = KategoriLelang::find($persiapan->paket->kgr_id);
        $pdf = PDF::loadView('monep.pdf.persiapan', compact('persiapan'))->setPaper('A4', 'potrait');
        return $pdf->stream();
    }
    public function hasil(Request $request, $id)
    {
        $paket = Paket::with(['spt', 'lelang', 'persiapan', 'panitia.pegawai'])->where('pkt_id', $id)->has('lelang')->first();
        //return $paket;
        if (sizeof($paket->spt) == 0) {
            $flash = [
                'error' => true,
                'pesan' => 'Tidak ada SPT yang dibuat!'
            ];
            return redirect()->route('monep.lelang.info', $paket->lelang->lls_id)
                ->with('flash', $flash);
        }
        $paket->kategori = KategoriLelang::find($paket->kgr_id);
        $paket->metode = Metode::find($paket->lelang->mtd_id);
        $paket->metode_pemilihan = MetodePemilihan::find($paket->lelang->mtd_pemilihan);
        $paket->pemenang = ($paket->lelang->pemenang) ? json_decode($paket->lelang->pemenang) : null;
        $paket->pemenang->rekanan = Rekanan::with(['bentuk_usaha', 'kabupaten'])->select('rkn_nama', 'rkn_alamat', 'rkn_npwp', 'btu_id', 'kbp_id')->where('rkn_id', $paket->pemenang->rkn_id)->first();
        $pdf = PDF::loadView('monep.pdf.hasil', compact('paket'))->setPaper('A4', 'potrait');
        return $pdf->stream();
    }
}
