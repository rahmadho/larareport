<?php

namespace App\Http\Controllers\Monep;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Epns\Fungsi;
use App\Models\Epns\KategoriLelang;
use App\Models\Monep\Paket;
use App\Models\Monep\Panitia;
use App\Models\Monep\Pegawai;
use App\Models\Monep\PegawaiUkpbj;
use App\Models\Monep\RouteController;
use App\Models\Monep\SatuanKerja;
use App\Models\Monep\UserLevel;
use App\Models\Monep\Users;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MonepCtrl extends Controller
{
    public function __construct(Request $req)
    {
        $this->tahun_now = 2019;
        $this->stk_ukpbj = 1805016;
        $this->request = $req;
    }
    public function dashboard()
    {
        return view('monep.dashboard');
    }
    public function surat()
    {
        return view('monep.surat.index');
    }
    public function paket_spse()
    {
        $result = Fungsi::stat();
        return view('monep.spse', compact('result'));
    }
    public function paket_rup()
    {
        $tahun = 2019;
        $filename = 'json-rup-' . date('Ym') . '.json';
        $url = "https://sirup.lkpp.go.id/sirup/datatablectr/dataruppenyediakldi?idKldi=D462&tahun={$tahun}&iDisplayStart=0&iDisplayLength=1000000&_=1566352631490";
        if (Storage::exists('json/' . $filename)) {
            $response = Storage::get('json/' . $filename);
        } else {
            $get = file_get_contents($url);
            $json = json_decode($get);
            $array = [];
            foreach ($json->aaData as $item) {
                $obj = new \stdClass;
                $obj->rup_id = $item[0];
                $obj->stk_nama = $item[1];
                $obj->pkt_nama = $item[2];
                $obj->pkt_pagu = $item[3];
                $obj->mtd_pemilihan = $item[4];
                $obj->sbd_id = $item[5];
                $obj->waktu_pemilihan = $item[7];
                array_push($array, $obj);
            }
            $response = json_encode($array);
            Storage::put('json/' . $filename, $response);
        }
        $collection = collect(json_decode($response));
        $mtd = [
            "Pengadaan Langsung",
            "Tender",
            // "e-Purchasing",
            "Seleksi",
            "Penunjukan Langsung",
            "Tender Cepat"
        ];
        $mtd_pemilihan = array_keys($collection->groupBy('mtd_pemilihan')->toArray());
        $stk_nama = array_keys($collection->groupBy('stk_nama')->sortKeys()->toArray());
        $rup = $collection->whereIn('mtd_pemilihan', $mtd);
        return view('monep.paket.rup', compact('rup', 'mtd_pemilihan', 'stk_nama'));
    }
    public function paket()
    {
        return view('monep.paket.index');
    }

    // PANITIA
    public function panitia()
    {
        $panitia = Panitia::with('pegawai')->where('is_active', -1)->where('stk_id', $this->stk_ukpbj)->where('pnt_tahun', $this->tahun_now)->orderBy('pnt_id', 'asc')->get();
        $pegawai = PegawaiUkpbj::with('pegawai')->get();
        return view('monep.panitia.index', compact('panitia', 'pegawai'));
    }

    public function report()
    {
        $result = Fungsi::stat();
        if (Auth::user()->id_level == 4) {
            $paket_pokjas = Pegawai::with('panitia')->whereHas('panitia', function ($q) {
                return $q->where('is_active', '-1')->where('pnt_tahun', date('Y'));
            })->where('peg_nip', Auth::user()->username)->first();
            $paket_pokja = $paket_pokjas->panitia->where('pnt_tahun', 2019)->where('is_active', -1);
            //return view('monep.report.paket', compact('result', 'paket_pokja'));
            return view('monep.report.report', compact('result', 'paket_pokja'));
        } else {
            $paket_pokja = Panitia::sumbar()->active()->tahun(date('Y'))->get();
            //return view('monep.report.paket', compact('result'));
            return view('monep.report.report', compact('result', 'paket_pokja'));
        }
    }
    public function report_progres_paket()
    {
        $satker = Fungsi::get_satker($this->tahun_now);
        if (Auth::user()->id_level == 4) {
            $paket_pokjas = Pegawai::with('panitia')->whereHas('panitia', function ($q) {
                return $q->where('is_active', '-1')->where('pnt_tahun', date('Y'));
            })->where('peg_nip', Auth::user()->username)->first();
            $paket_pokja = $paket_pokjas->panitia->where('pnt_tahun', 2019)->where('is_active', -1);
            return view('monep.report.progres', compact('paket_pokja', 'satker'));
        } else {
            return view('monep.report.progres', compact('satker'));
        }
    }
    public function users()
    {
        $users = Users::with('level')->get();
        $levels = UserLevel::all();
        return view('monep.user.index', compact('users', 'levels'));
    }
    public function profile(Request $request)
    {
        $user = Auth::user();
        $levels = UserLevel::all();
        return view('monep.user.edit', compact('user', 'levels'));
    }
}
