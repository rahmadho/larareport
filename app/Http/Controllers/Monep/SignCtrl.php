<?php

namespace App\Http\Controllers\Monep;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Monep\Pegawai;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SignCtrl extends Controller
{
    public function __construct()
    {
        $this->middleware('sign', ['except' => ['login', 'logout', 'info', 'signin', 'signout']]);
    }
    public function dashboard()
    {
        return view('monep.dashboard');
    }
    public function login()
    {
        return view('monep.login');
    }
    public function signin(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        $auth = Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password')]);
        if ($auth) {
            session(['username' => $auth]);
        }
        //return [Auth::user()];
        return redirect()->route('monep.dashboard');
    }
    public function signout()
    {
        Session::flush();
        // Auth::logout();
        return redirect('login');
    }
    public function info(Request $request)
    {
        //return Session::all();
        return Auth::user();
        //return $request->session()->get('username');
        if ($request->session()->exists('username')) return $request->session()->all();
    }
    public function reset()
    {
        $pegs = Pegawai::whereRaw('peg_id IN (SELECT peg_id FROM pegawai_ukpbj)')->get();
        foreach ($pegs as $peg) {
            User::updateOrCreate(
                ['username' => $peg->peg_nip],
                [
                    'password' => Hash::make('hapus'),
                    'nama' => $peg->peg_nama,
                    'id_level' => 4
                ]
            );
        }
    }
}
