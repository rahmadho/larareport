<?php

namespace App\Http\Controllers\Monep;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Models\Monep\BentukUsaha;
use App\Models\Monep\Ukpbj;
use App\Models\Monep\PegawaiUkpbj;
use App\Models\Monep\Paket;
use App\Models\Monep\LelangSeleksi;
use App\Models\Epns\MetodePemilihan;
use App\Models\Epns\KategoriLelang;
use App\Models\Epns\StatusLelang;
use App\Models\Epns\Tahap;
use App\Models\Epns\PaketDokumen;
use App\Models\Monep\PaketDokumen as PaketDokumenTable;
use App\Models\Monep\Dokumen;
use App\Models\Epns\Metode;
use App\Models\Monep\Panitia;
use App\Models\Monep\Pegawai;
use App\Models\Monep\Rekanan;
use App\Models\Monep\SatuanKerja;
use App\Models\Monep\Surat;

class PaketCtrl extends Controller
{
    private $request;
    private $params;
    private $result;

    public function run(Request $request, $run, $param = null)
    {
        if (method_exists($this, $run)) {
            $this->request = $request;
            $this->params = $param;
            $this->$run();
            return $this->result;
        } else {
            abort(404);
        }
    }

    public function dashboard()
    {
        $this->result = view('sibaja.dashboard');
    }

    // paket yang harusnya masuk ke rekap
    public function paket_ulp()
    {
        $query = Paket::select('pkt_id', 'pkt_nama', 'pkt_pagu', 'pkt_hps', 'pkt_flag', 'kgr_id', 'rup_id', 'sbd_id', 'ang_tahun', 'pkt_tgl_buat', 'stk_id', 'pnt_id', 'ppk_nip')
            ->with([
                'ppk' => function ($q) {
                    $q->select(['peg_nip', 'peg_nama']);
                },
                'lelang_ulp',
                'satker' => function ($q) {
                    $q->select(['stk_id', 'stk_nama']);
                },
                'panitia' => function ($q) {
                    $q->select(['pnt_id', 'pnt_nama']);
                }
            ]);
        if ($this->request->has('tahun')) {
            $query->where('ang_tahun', $this->request->input('tahun'));
        }
        if ($this->request->has('has_pnt')) {
            $query->has('panitia');
        }
        if ($this->request->has('filter')) {
            $query->where(function ($q) {
                $q->orWhereHas('lelang_ulp', function ($q) {
                    $q->where('lls_id', 'like', '%' . $this->request->input('filter') . '%');
                });
                $q->orWhereHas('satker', function ($q) {
                    $q->where('stk_nama', 'like', '%' . $this->request->input('filter') . '%');
                });
                $q->where('pkt_id', 'like', '%' . $this->request->input('filter') . '%');
                $q->orWhere('pkt_nama', 'like', '%' . $this->request->input('filter') . '%');
            });
        }
        $query->whereHas('lelang', function ($q) {
            return $q->where('lls_status', 0);
        });
        $result = $query->orderBy('pkt_tgl_buat', 'desc')->limit(5)->get();
        $this->result = view('sibaja.paket.index', compact('result'));
    }

    public function lelang()
    {
        $result = Paket::find(13641016);
        //$get = Ukpbj::with('pegawai')->where('ukpbj_id', 1016)->get();
        return $this->result = $result->lelang;
    }

    public function surat_add(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'kgr_id' => 'required|numeric'
            ]);
            $kgr_id = $request->kgr_id;
        } else {
            $kgr_id = old('kgr_id', '999');
        }
        //return $kgr_id;
        $dokumen = PaketDokumen::find($kgr_id);
        $kategori_lelang = KategoriLelang::find($kgr_id);
        $satker = SatuanKerja::select('stk_nama', 'stk_id')->whereRaw("instansi_id='D462' AND string_to_array(rup_stk_tahun, ',')  && array['2019']")->orderBy('stk_nama', 'asc')->get();
        return view('monep.surat.create', compact('dokumen', 'kategori_lelang', 'request', 'satker'));
    }

    public function surat_info(Request $request, $id)
    {
        $query = Surat::with('paket.ppk')->where('id', $id)->first();
        if (!$query) {
            abort(404);
        }
        $result = $query;
        return view('monep.surat.edit', compact('result'));
    }

    public function edit_pegawai(Request $request, $id)
    {
        $get = Pegawai::findOrFail($id);
        $pegawai = $get;
        return view('monep.pegawai.edit', compact('pegawai'));
    }
}
