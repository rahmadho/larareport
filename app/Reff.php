<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reff extends Model
{
    public static function sumber_dana()
    {
        $query = DB::select("SELECT DISTINCT(sbd_id)::numeric as id, sbd_ket as nama FROM sumber_dana ORDER BY id;");
        $ret = array_map(function ($item) {
            return ["id" => intval($item->id), "nama" => $item->nama];
        }, $query);
        usort($ret, function ($a, $b) {
            return $a['id'] - $b['id'];
        });
        return $ret;
    }

    public static function jenis_kontrak()
    {
        $data = array(
            array("id" => 1, "nama" => "Lumsum"),
            array("id" => 2, "nama" => "Harga Satuan"),
            array("id" => 3, "nama" => "Gabungan Lumsum dan Harga Satuan"),
            array("id" => 4, "nama" => "Terima Jadi (Turnkey)"),
            array("id" => 5, "nama" => "Persentase"),
            array("id" => 6, "nama" => "Tahun Tunggal"),
            array("id" => 7, "nama" => "Tahun Jamak"),
            array("id" => 8, "nama" => "Pengadaan Tunggal"),
            array("id" => 9, "nama" => "Pengadaan Bersama"),
            array("id" => 10, "nama" => "Kontrak Payung"),
            array("id" => 11, "nama" => "Pengadaan Pekerjaan Tunggal"),
            array("id" => 12, "nama" => "Pengadaan Pekerjaan Terintegrasi"),
            array("id" => 13, "nama" => "Waktu Penugasan")
        );
        return $data;
    }
    public static function kategori_lelang($id = null)
    {

        $data = array(
            array('id' => 0, 'nama' => 'Pengadaan Barang'),
            array('id' => 1, 'nama' => 'Jasa Konsultansi Badan Usaha'),
            array('id' => 2, 'nama' => 'Pekerjaan Konstruksi'),
            array('id' => 3, 'nama' => 'Jasa Lainnya'),
            array('id' => 4, 'nama' => 'Jasa Konsultansi Perorangan')
        );
        if ($id != null) {
            $filter = array_filter($data, function ($item) use ($id) {
                return $item['id'] == $id;
            });
            $ret = array_column($filter, 'nama')[0];
        } else {
            $ret = $data;
        }
        return $ret;
    }
    public static function metode_pemilihan($id = null)
    {
        $data = array(
            array("id" => 0,  "nama" => "Lelang Umum", "label" => "Pelelangan Umum"),
            array("id" => 1,  "nama" => "Lelang Sederhana", "label" => "Pelelangan Sederhana"),
            array("id" => 2,  "nama" => "Lelang Pemilihan Langsung", "label" => "Pemilihan Langsung"),
            array("id" => 3,  "nama" => "Seleksi Umum", "label" => "Seleksi Umum"),
            array("id" => 4,  "nama" => "Seleksi Sederhana", "label" => "Seleksi Sederhana"),
            array("id" => 5,  "nama" => "Seleksi Langsung", "label" => "Seleksi Langsung"),
            array("id" => 6,  "nama" => "Penunjukan Langsung", "label" => "Penunjukan Langsung"),
            array("id" => 7,  "nama" => "Swakelola", "label" => "Swakelola"),
            array("id" => 8,  "nama" => "Lelang Terbatas", "label" => "Pelelangan Terbatas"),
            array("id" => 9,  "nama" => "Tender Cepat", "label" => "Tender Cepat"),
            array("id" => 10, "nama" => "Seleksi Cepat", "label" => "E-Seleksi Cepat"),
            array("id" => 11, "nama" => "Pengadaan Langsung", "label" => "Pengadaan Langsung"),
            array("id" => 12, "nama" => "Penunjukan Langsung", "label" => "Penunjukan Langsung"),
            array("id" => 13, "nama" => "Kontes", "label" => "Kontes"),
            array("id" => 14, "nama" => "Sayembara", "label" => "Sayembara"),
            array("id" => 15, "nama" => "Tender", "label" => "Tender"),
            array("id" => 16, "nama" => "Seleksi", "label" => "Seleksi"),
            array("id" => 17, "nama" => "Pengecualian", "label" => "Pengecualian"),
            array("id" => 18, "nama" => "Darurat", "label" => "Darurat"),
            array("id" => 19, "nama" => "Tender Internasional", "label" => "Tender Internasional")
        );
        if ($id != null) {
            $filter = array_filter($data, function ($item) use ($id) {
                return $item['id'] == $id;
            });
            $ret = array_column($filter, 'nama')[0];
        } else {
            $ret = $data;
        }
        return $ret;
    }
    public static function metode_evaluasi($id = null)
    {
        $data = array(
            array('id' => 0, 'nama' => "Harga Terendah Sistem Gugur"),
            array('id' => 1, 'nama' => "Sistem Nilai"),
            array('id' => 2, 'nama' => "Sistem Umur Ekonomis"),
            array('id' => 3, 'nama' => "Kualitas"),
            array('id' => 4, 'nama' => "Kualitas dan Biaya"),
            array('id' => 5, 'nama' => "Pagu Anggaran"),
            array('id' => 6, 'nama' => "Biaya Terendah"),
            array('id' => 7, 'nama' => "Penunjukan Langsung"),
            array('id' => 8, 'nama' => "Harga Terendah Ambang Batas")
        );
        if ($id != null) {
            $filter = array_filter($data, function ($item) use ($id) {
                return $item['id'] == $id;
            });
            $ret = array_column($filter, 'nama')[0];
        } else {
            $ret = $data;
        }
        return $ret;
    }
    public static function jenis_evaluasi($id = null)
    {
        $data = array(
            array('id' => 0, 'nama' => "Evaluasi Kualifikasi"),
            array('id' => 1, 'nama' => "Evaluasi Administrasi"),
            array('id' => 2, 'nama' => "Evaluasi Teknis"),
            array('id' => 3, 'nama' => "Evaluasi Harga"),
            array('id' => 4, 'nama' => "Evaluasi Akhir"),
            array('id' => 5, 'nama' => "Pembuktian Kualifikasi")
        );
        if ($id != null) {
            $filter = array_filter($data, function ($item) use ($id) {
                return $item['id'] == $id;
            });
            $ret = array_column($filter, 'nama')[0];
        } else {
            $ret = $data;
        }
        return $ret;
    }
    public static function metode_dokumen($id = null)
    {
        $data = array(
            array('id' => 0, 'nama' => "Satu File"),
            array('id' => 1, 'nama' => "Dua File"),
            array('id' => 2, 'nama' => "Dua Tahap")
        );
        if ($id != null) {
            $filter = array_filter($data, function ($item) use ($id) {
                return $item['id'] == $id;
            });
            $ret = array_column($filter, 'nama')[0];
        } else {
            $ret = $data;
        }
        return $ret;
    }
    public static function metode_kualifikasi($id = null)
    {
        $data = array(
            array('id' => 0, 'nama' => "Prakualifikasi"),
            array('id' => 1, 'nama' => "Pascakualifikasi")
        );
        if ($id != null) {
            $filter = array_filter($data, function ($item) use ($id) {
                return $item['id'] == $id;
            });
            $ret = array_column($filter, 'nama')[0];
        } else {
            $ret = $data;
        }
        return $ret;
    }
    public static function status_paket($id = null)
    {
        $data = array(
            array('id' => 0, 'nama' => "Draft"),
            array('id' => 1, 'nama' => "Tender Sedang Berjalan"),
            array('id' => 2, 'nama' => "Tender Sudah Selesai"),
            array('id' => 3, 'nama' => "Tender Diulang"),
            array('id' => 4, 'nama' => "Tender Ditolak")
        );
        if ($id != null) {
            $filter = array_filter($data, function ($item) use ($id) {
                return $item['id'] == $id;
            });
            $ret = array_column($filter, 'nama')[0];
        } else {
            $ret = $data;
        }
        return $ret;
    }
    public static function status_lelang($id = null)
    {
        $data = array(
            array('id' => 0, 'nama' => "Draft"),
            array('id' => 1, 'nama' => "Tender Sedang Berjalan"),
            array('id' => 2, 'nama' => "Tender Ditutup"),
            array('id' => 3, 'nama' => "Tender Sudah Selesai"),
            array('id' => 4, 'nama' => "Tender DiTolak"),
            array('id' => 5, 'nama' => "Draft UKPBJ"),
            array('id' => 6, 'nama' => "Draft Pokja")
        );
        if ($id != null) {
            $filter = array_filter($data, function ($item) use ($id) {
                return $item['id'] == $id;
            });
            $ret = array_column($filter, 'nama')[0];
        } else {
            $ret = $data;
        }
        return $ret;
    }
    public static function kualifikasi($id = null)
    {
        $data = array(
            array('id' => 21, 'nama' => "Perusahaan Kecil"),
            array('id' => 22, 'nama' => "Perusahaan Non Kecil"),
            array('id' => 23, 'nama' => "Perusahaan Kecil atau Non Kecil")
        );
        if ($id != null) {
            $filter = array_filter($data, function ($item) use ($id) {
                return $item['id'] == $id;
            });
            $ret = array_column($filter, 'nama')[0];
        } else {
            $ret = $data;
        }
        return $ret;
    }
    // rekanan
    // rkn_status
    // public static int REKANAN_BARU = 0;
    // public static int REKANAN_DISETUJUI = 1; = DEVIRIFIKASI
    // public static int REKANAN_DITOLAK = -2;
    // public static int REKANAN_EXPIRED = -3;
    // jumlah paket yang diikuti: SELECT count(psr_id) FROM peserta WHERE rkn_id=?

}
/*

PERSETUJUAN -> cek kapan lelang tayang (pst_jenis = 0) pst_status = 1

-----------
pst_jenis
    PENGUMUMAN_LELANG(0, "pengumuman"),
    PEMENANG_LELANG(1, "pemenang"),
    PEMENANG_PRAKUALIFIKASI(2, "pemenang_pra"),
    PEMENANG_TEKNIS(3, "pemenang_teknis"),
    BATAL_LELANG(4, "tutup"),
    ULANG_LELANG(5, "ulang")
pst_status
    BELUM_SETUJU, 0
    SETUJU, 1
    TIDAK_SETUJU; 2


    AMBIL_DOKUMEN_PEMILIHAN(18776, "Download Dokumen Pemilihan"),
    AMBIL_DOKUMEN(18777, "Download Dokumen Pemilihan"),
    AMBIL_DOK_PRA(18778, "Download Dokumen Kualifikasi"),
    EVALUASI_DOK_PRA(18779, "Evaluasi Dokumen Kualifikasi"),
    EVALUASI_PENAWARAN(18781, "Evaluasi Penawaran"),
    EVALUASI_PENAWARAN_ADM_TEKNIS(18782, "Evaluasi penawaran administrasi dan teknis"),
    EVALUASI_PENAWARAN_BIAYA(18783, "Evaluasi Penawaran Harga"),
    KLARIFIKASI_TEKNIS_BIAYA(18784, "Klarifikasi dan Negosiasi Teknis dan Biaya"),
    KLARIFIKASI_KEWAJARAN_HARGA(18785, "Klarifikasi Kewajaran harga"),
    KOREKSI_ARITMATIK(18786, "Koreksi Aritmatik dan Klarifikasi Kewajaran Harga"),
    SANGGAH_ADM_TEKNIS(18788, "Masa Sanggah atas Hasil Kelulusan Administrasi dan Teknis"),
    SANGGAH(18789, "Masa Sanggah Hasil Tender"),
    SANGGAH_PRA(18791, "Masa Sanggah Prakualifikasi"),
    PEMASUKAN_DAN_EVALUASI_DOK_PRA(18792, "Pemasukan dan Evaluasi Dokumen Kualifikasi"),
    PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS(18795, "Pembukaan dan Evaluasi Penawaran File I: Administrasi dan Teknis "),
    PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA(18796, "Pembukaan dan Evaluasi Penawaran File II: Harga"),
    PEMBUKAAN_PENAWARAN_BIAYA(18797, "Pembukaan dokumen penawaran file II : Penawaran Harga"),
    PEMBUKAAN_PENAWARAN(18798, "Pembukaan Dokumen Penawaran"),
    VERIFIKASI_KUALIFIKASI(18802, "Pembuktian Kualifikasi"),
    TANDATANGAN_KONTRAK(18803, "Penandatanganan Kontrak"),
    PENETAPAN_PEMENANG_AKHIR(18804, "Penetapan Pemenang"),
    PENETAPAN_PEMENANG_ADM_TEKNIS(18805, "Penetapan Peringkat Teknis"),
    PENGUMUMAN_LELANG(18807, "Pengumuman Pascakualifikasi"),
    UMUM_PRAKUALIFIKASI(18808, "Pengumuman Prakualifikasi"),
    PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK(18809, "Pengumuman Hasil Prakualifikasi dan Download Dokumen Pemilihan"),
    PENGUMUMAN_PEMENANG_ADM_TEKNIS(18793, "Pengumuman Hasil Evaluasi Administrasi dan Teknis"),
    PENGUMUMAN_PEMENANG_AKHIR(18810, "Pengumuman Pemenang"),
    PENGUMUMAN_HASIL_PRA(18811, "Pengumuman Hasil Prakualifikasi"),
    PENJELASAN(18812, "Pemberian Penjelasan"),
    PENJELASAN_PRA(18832, "Penjelasan Dokumen Prakualifikasi"),
    PENYETARAAN_TEKNIS(18814, "Penyetaraan Penawaran Teknis (apabila diperlukan)"),
    PENUNJUKAN_PEMENANG(18817, "Surat Penunjukan Penyedia Barang/Jasa"),
    UPLOAD_BA_EVALUASI_PENAWARAN(18819, "Upload Berita Acara Evaluasi Penawaran"),
    UPLOAD_BA_EVALUASI_HARGA(18820, "Upload Berita Acara Hasil Evaluasi Harga"),
    UPLOAD_BA_EVALUASI_ADM_TEKNIS(18821, "Upload Berita Acara Hasil Evaluasi Administrasi dan Teknis"),
    UPLOAD_BA_HASIL_LELANG(18822, "Upload Berita Acara Hasil Pemilihan"),
    PEMASUKAN_PENAWARAN_ADM_TEKNIS(18824, "Upload dokumen penawaran File I: Penawaran Administrasi dan Teknis"),
    PEMASUKAN_PENAWARAN_BIAYA(18826, "Upload Dokumen Penawaran File II: Penawaran Harga"),
    PEMASUKAN_DOK_PRA(18827, "Kirim Persyaratan Kualifikasi"),
    PENETAPAN_PEMENANG_PENAWARAN(18828, "Usulan Calon Pemenang"),
    PENETAPAN_HASIL_PRA(18830, "Penetapan Hasil Kualifikasi"),
    PEMASUKAN_PENAWARAN(18833, "Upload Dokumen Penawaran"),
    UPLOAD_BA_TAMBAHAN(18839, "Upload Berita Acara Tambahan"),
    KLARIFIKASI_NEGOSIASI(18840, "Klarifikasi Teknis dan Negosiasi"),
    TANDATANGAN_SPK(18841, "Penandatanganan Kontrak"),
    TANDATANGAN_SMPK(18842, "Penandatanganan SMPK"),
    EVALUASI_PENAWARAN_KUALIFIKASI(18843, "Evaluasi Administrasi, Kualifikasi, Teknis, dan Harga"),
    PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI(18844, "Pembukaan dan Evaluasi Penawaran File I: Administrasi, Dokumen Kualifikasi, dan Teknis"),
    PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR(18845, "Penetapan dan Pengumuman Pemenang"),
    BELUM_DILAKSANAKAN(0, "Tender Belum Dilaksanakan"),
    TIDAK_ADA_JADWAL(0, "Tidak Ada Jadwal"),
    SUDAH_SELESAI(0, "Tender Sudah Selesai"),
    PAKET_SUDAH_SELESAI(0, "Paket Sudah Selesai"),
    PAKET_BELUM_DILAKSANAKAN(0, "Paket Belum Dilaksanakan");


    PASCA_SATU_FILE_80(1, MetodeKualifikasi.PASCA, MetodeDokumen.SATU_FILE),
    PASCA_DUA_FILE_80(19, MetodeKualifikasi.PASCA, MetodeDokumen.DUA_FILE),
    PRA_DUA_FILE_80(2, MetodeKualifikasi.PRA, MetodeDokumen.DUA_FILE),
    PRA_DUA_FILE_80_KONSULTANSI(4, MetodeKualifikasi.PRA, MetodeDokumen.DUA_FILE),
    PRA_DUA_TAHAP_80(3, MetodeKualifikasi.PRA, MetodeDokumen.DUA_TAHAP),
    PASCA_SATU_FILE_54(41, MetodeKualifikasi.PASCA, MetodeDokumen.SATU_FILE),
    PASCA_DUA_FILE_54(51, MetodeKualifikasi.PASCA, MetodeDokumen.DUA_FILE),
    PRA_DUA_FILE(42, MetodeKualifikasi.PRA, MetodeDokumen.DUA_FILE),
    PRA_DUA_FILE_KUALITAS(46, MetodeKualifikasi.PRA, MetodeDokumen.DUA_FILE),
    PRA_DUA_FILE_KUALITAS_BIAYA(47, MetodeKualifikasi.PRA, MetodeDokumen.DUA_FILE),
    PRA_DUA_TAHAP(44, MetodeKualifikasi.PRA, MetodeDokumen.DUA_TAHAP),
    PRA_SATU_FILE_KONSULTANSI(48, MetodeKualifikasi.PRA, MetodeDokumen.SATU_FILE),
    PRA_SATU_FILE(50, MetodeKualifikasi.PRA, MetodeDokumen.SATU_FILE),
    PASCA_SATU_FILE_KUALITAS(52, MetodeKualifikasi.PASCA, MetodeDokumen.SATU_FILE),
    PASCA_DUA_FILE_KUALITAS(53, MetodeKualifikasi.PASCA, MetodeDokumen.DUA_FILE),
    PASCA_SATU_FILE(54, MetodeKualifikasi.PASCA, MetodeDokumen.SATU_FILE),
    PASCA_DUA_FILE(55, MetodeKualifikasi.PASCA, MetodeDokumen.DUA_FILE);

// Kualifikasi
0 => prokualifikasi
1 => pascakualifikasi
// Dokumen
0, 'Satu File';
1, 'Dua File';
2, 'Dua Tahap';

0 -> Barang
1 -> Konsul Badan Usaha
2 -> Konstruksi
3 -> Jasa Lain
4 -> Konsul Perorangan


INSERT INTO metode (id, id_metode_kualifikasi, id_metode_dokumen) VALUES 
(1, 1, 0),
(19, 1, 1),
(2, 0, 1),
(4, 0, 1),
(3, 0, 2),
(41, 1, 0),
(51, 1, 1),
(42, 0, 1),
(46, 0, 1),
(47, 0, 1),
(44, 0, 2),
(48, 0, 0),
(50, 0, 0),
(52, 1, 0),
(53, 1, 1),
(54, 1, 0),
(55, 1, 1);
*/
